package baseTest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import managers.FileReaderManager;
import managers.PageObjectManager;
import managers.WebDriverManager;
import pageRepository.CarrierConfirmationPage;
import pageRepository.CarrierOfferPage;
import pageRepository.CarrierRatingPage;
import pageRepository.ControlTowerPage;
import pageRepository.CreateOrderPage;
import pageRepository.CustomerRatingPage;
import pageRepository.DeliveryLogPage;
import pageRepository.ItegratedRatingPage;
import pageRepository.LoginPage;
import pageRepository.OpsClosedPage;
import pageRepository.PickUpLogPage;
import pageRepository.commonPageUtils.CarrierContactSearchHandler;
import pageRepository.commonPageUtils.CarrierOfferAcceptedHandler;
import pageRepository.commonPageUtils.CarrierSearchHandler;
import pageRepository.commonPageUtils.CarrierUnassignmentReasonHandler;
import pageRepository.commonPageUtils.CommonPopupHandler;
import pageRepository.commonPageUtils.EmailTemplateHandler;
import pageRepository.commonPageUtils.MoreActionHandler;
import pageRepository.commonPageUtils.OrderDetailsPanelHandler;
import pageRepository.commonPageUtils.OrderExecutionHandler;
import pageRepository.commonPageUtils.SideNavigationMenuHandler;
import pageRepository.commonPageUtils.UndoPageHandler;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public abstract class BaseTestClass {
	Logger logger = LogManager.getLogger(BaseTestClass.class);
	protected Scenario scenario;
	private WebDriver driver;
	protected WebDriverManager webDriverManager = new WebDriverManager();
	protected LoginPage loginPage;
	protected Waits waits;
	protected ControlTowerPage landingPage;
	protected CreateOrderPage createOrderPage;
	protected SideNavigationMenuHandler sideNav;
	protected UserActionsHandler userActions;
	protected WebElementsUtils webElementsUtils;
	protected CustomerRatingPage customerRatingPage;
	protected CarrierRatingPage carrierRatingPage;
	protected CarrierConfirmationPage carrierConfirmationPage;
	protected PickUpLogPage pickUpLogPage;
	protected DeliveryLogPage deliveryLogPage;
	protected OpsClosedPage opsClosedPage;
	protected OrderDetailsPanelHandler orderDetailsHeaderHandler;
	protected ItegratedRatingPage itegratedRatingPage;
	protected OrderExecutionHandler orderExecutionHandler;
	protected CommonPopupHandler commonPopupHandler;
	protected CarrierOfferPage carrierOfferPage;
	protected CarrierSearchHandler carrierSearchHandler;
	protected CarrierContactSearchHandler carrierContactSearchHandler;
	protected CarrierOfferAcceptedHandler carrierOfferAcceptedHandler;
	protected EmailTemplateHandler emailTemplateHandler;
	protected MoreActionHandler moreActionHandler;
	protected CarrierUnassignmentReasonHandler carrierUnassignmentReasonHandler;
	protected UndoPageHandler undoPageHandler;
	
	protected PrintStream ps;
	
	public BaseTestClass() {
		PageObjectManager pageObjectManager = new PageObjectManager(driver);
		loginPage = pageObjectManager.getLoginPage();
		landingPage = pageObjectManager.getLandingPage();
		createOrderPage = pageObjectManager.getCreateOrderPage();
		customerRatingPage = pageObjectManager.getCustomerRatingPage();
		sideNav = pageObjectManager.getSideNavigationMenu();
		carrierRatingPage = pageObjectManager.getCarrierRatingPage();
		carrierConfirmationPage = pageObjectManager.getCarrierConfirmationPage();
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
		webElementsUtils = new WebElementsUtils(driver);
		pickUpLogPage = pageObjectManager.pickUpLogPage();
		deliveryLogPage = pageObjectManager.deliveryLogPage();
		opsClosedPage = pageObjectManager.opsClosedPage();
		orderDetailsHeaderHandler = new OrderDetailsPanelHandler(driver);
		itegratedRatingPage = pageObjectManager.itegratedRatingPage();
		orderExecutionHandler = new OrderExecutionHandler(driver);
		commonPopupHandler = new CommonPopupHandler(driver);
		carrierOfferPage = pageObjectManager.carrierOfferPage();
		carrierSearchHandler = new CarrierSearchHandler(driver);
		carrierContactSearchHandler = new CarrierContactSearchHandler(driver);
		carrierOfferAcceptedHandler = new CarrierOfferAcceptedHandler(driver);
		emailTemplateHandler = new EmailTemplateHandler(driver);
		moreActionHandler = new MoreActionHandler(driver);
		carrierUnassignmentReasonHandler = new CarrierUnassignmentReasonHandler(driver);
		undoPageHandler = new UndoPageHandler(driver);
	}
	public void initializeBrowser() {
		driver = webDriverManager.getDriver();
		String url = System.getProperty("url");		
		if (url == null) {
			url = FileReaderManager.getInstance().getConfigReader().getApplicationURL();
			logger.info("Opening URL " + url);
		}else {
			logger.info("Opening URL " + url);
		}
		driver.get(url);
		driver.manage().window().maximize();
	}
	public void startRecordingLog(String scenarioName) {
		String reportFileName = scenarioName + ".txt";
		String fileSeperator = System.getProperty("file.separator");
		String reportFilepath = System.getProperty("user.dir") + fileSeperator + "TestReport";
		String reportFileLocation = reportFilepath + fileSeperator + reportFileName;
		File testDirectory = new File(reportFilepath);
		if (!testDirectory.exists()) {
			if (testDirectory.mkdir()) {
				System.out.println("Directory: " + testDirectory.getAbsolutePath() + " is created!");
			} else {
				System.out.println("Failed to create directory: " + testDirectory.getAbsolutePath());
			}
		} else {
			System.out.println("Directory already exists: " + testDirectory.getAbsolutePath());
		}
		File logFile = new File(reportFileLocation);
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(logFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ps = new PrintStream(fos);
		//System.setOut(ps);
	}

	public void stopRecordingLog() {
		System.setOut(ps);
	}
}
