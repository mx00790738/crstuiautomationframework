Feature: End2End Test 

Background:
Given user navigates to the CRST Agent portal login page
When user login as agent user

@End2End
Scenario: FTL List, Edit and execute
And user creates a FTL Order
And user goes to control tower and click next action for the order
And user customer rate the order
And user goes to control tower and click next action for the order
And user carrier rate the order
And user goes to control tower and click next action for the order
And user send carrier confirmation mail
And user goes to control tower and click next action for the order
And user enters pickup logs
And user goes to control tower and click next action for the order
And user enters delivery logs
And user goes to control tower and click next action for the order
Then verify closure of order

@End2End
Scenario: Create FTL Multistop Order and execute
And user creates a FTL with multistop Order
And user customer rate the order
And user carrier rate the order
And user send carrier confirmation mail
And user add pickup log for multiple pickup location
And user add drop log for multiple drop location
Then verify closure of order

@End2End
Scenario: Create FTL Order and execute
And user creates a FTL Order
And user customer rate the order
And user carrier rate the order
And user send carrier confirmation mail
And user enters pickup logs
And user enters delivery logs
Then verify closure of order 

@End2End
Scenario: Create LTL V Order and execute
And user create a LTLV Order
And user customer rate the order
And user carrier rate the order
And user enters pickup logs
And user enters delivery logs
Then verify closure of order

@End2End
Scenario: Create LTL A Order and execute
And user create a LTLA Order
And user enters customer and carrier rating details in integrated rating page
And user enters pickup logs
And user enters delivery logs
Then verify closure of order

@End2End
Scenario: FTL Create-Edit-Execute
And user creates a FTL Order
And user edits same order for consignment
And user customer rate the order
And user edits customer rating
And user carrier rate the order
And user edits carrier rating
And user send carrier confirmation mail
And user enters pickup logs
And user enter callin in tracking page
And user edits pickup logs
And user enters delivery logs
And user closes the order
And user reopen the order
And user edits delivery logs 
And user again closes the order
Then verify closure of order

@End2End
Scenario: FTL-List-Execute-CarrierOffer-01
And user creates a FTL Order
And user customer rate the order
And user do carrier rating from carrier offer page
And user enters pickup logs
And user enters delivery logs
Then verify closure of order