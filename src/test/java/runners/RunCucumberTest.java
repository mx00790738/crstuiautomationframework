package runners;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;


import io.cucumber.testng.CucumberOptions;
import io.cucumber.testng.FeatureWrapper;

import io.cucumber.testng.PickleWrapper;
import io.cucumber.testng.TestNGCucumberRunner;
import testNgUtils.RetryAnalyzer;
import testNgUtils.TestListener;

@CucumberOptions(
features = "src/test/java/featureFiles", 
glue = { "stepDefinitions" },
//dryRun = true,
plugin = { "pretty", "html:target/cucumber-reports", "json:target/cucumber.jason",
				"rerun:target/rerun.txt" }, 
monochrome = true
)

@Listeners(TestListener.class)
public class RunCucumberTest {
	private TestNGCucumberRunner testNGCucumberRunner;
	TestListener testListener = new TestListener();
    @BeforeClass(alwaysRun = true)
    public void setUpClass() {
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
    }
   
   @Test(groups = "cucumber", description = "Runs Cucumber Scenarios", dataProvider = "scenarios",
		   retryAnalyzer=RetryAnalyzer.class)
    public void runScenario(PickleWrapper pickleWrapper, FeatureWrapper featureWrapper) {
        // the 'featureWrapper' parameter solely exists to display the feature
        // file in a test report
	   testListener.setTextContext(pickleWrapper.getPickle().getName(), featureWrapper.toString());
       testNGCucumberRunner.runScenario(pickleWrapper.getPickle());
    }

    @DataProvider
    public Object[][] scenarios() {
        if (testNGCucumberRunner == null) {
            return new Object[0][0];
        }
        return testNGCucumberRunner.provideScenarios();
    }

    @AfterClass(alwaysRun = true)
    public void tearDownClass() {
    	try {
        if (testNGCucumberRunner == null) {
            return;
        }
        testNGCucumberRunner.finish();
    	}catch(Exception e) {
    		
    	}
    }
}
