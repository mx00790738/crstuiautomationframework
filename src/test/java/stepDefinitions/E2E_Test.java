package stepDefinitions;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import baseTest.BaseTestClass;
import enumRepository.CarrierOfferStatus;
import enumRepository.CarrierUnassignmentReason;
import enumRepository.CommentType;
import enumRepository.DelayDescription;
import enumRepository.DelayStatus;
import enumRepository.OrderType;
import enumRepository.PayMethod;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class E2E_Test extends BaseTestClass {
	Logger logger = LogManager.getLogger(E2E_Test.class);
    private String ORDER_ID = null;
    
    @Before
	public void before(Scenario scenario) {
    	initializeBrowser();
	    this.scenario = scenario;	
	    startRecordingLog(scenario.getName().toString());
	}
    
	@Given("^user navigates to the CRST Agent portal login page$")
	public void user_navigates_to_the_CRST_Agent_portal_login_page() {
		loginPage.waitTillLoginPageLoadOut();
	}

	@When("^user login as agent user$")
	public void user_loggingIn() {
		loginPage.loginAsAgent();
	}

	@And("user creates a FTL Order")
	public void user_creates_an_FTL_Order() {
		landingPage.waitTillControlTowerPageLoadOut();
		sideNav.clickOnCreateOrder();
		createOrderPage.verifyUserIsInCreateOrderPageMainMenu();
		createOrderPage.createFTLOrder("1000023", OrderType.SPOT, 
				"0002CEIA", "15 April 2021 Thursday",
				CommentType.HOT, "test", 
				"0004FOMO", "16 April 2021 Friday", 
				CommentType.HOT, "test",
				"100", "10", 
				"123", "123");
		ORDER_ID = orderDetailsHeaderHandler.getOrderID();
	}
	
	@Given("user creates a FTL with multistop Order")
	public void user_creates_a_FTL_with_multistop_Order() {
		HashMap<String, HashMap<String, String>> pickUpList = new HashMap<String, HashMap<String, String>>();
		HashMap<String, String> pickup1 = new HashMap<String, String>();
		pickup1.put("LocationCode", "0002ELIL");
		pickup1.put("PlannedFrom", "16 April 2021 Friday");
		pickup1.put("PlannedTo", "17 April 2021 Saturday");
		pickUpList.put("pickup_1", pickup1);
		
		HashMap<String, HashMap<String, String>> dropList = new HashMap<String, HashMap<String, String>>();
		HashMap<String, String> drop1 = new HashMap<String, String>();
		drop1.put("LocationCode", "0005FOTX");
		drop1.put("PlannedFrom", "19 April 2021 Monday");
		drop1.put("PlannedTo", "20 April 2021 Tuesday");
		dropList.put("drop_1", drop1);
		landingPage.waitTillControlTowerPageLoadOut();
		sideNav.clickOnCreateOrder();
		createOrderPage.verifyUserIsInCreateOrderPageMainMenu();
		createOrderPage.createFTLOrderWithMultiStop("1000023", OrderType.SPOT, 
				"0002CEIA", "15 April 2021 Thursday",
				CommentType.HOT, "test", 
				"0004FOMO", "21 April 2021 Wednesday", 
				CommentType.HOT, "test",
				"100", "10", 
				pickUpList, dropList, "123", "123");
	}

	@And("user edits same order for consignment")
	public void user_edits_same_order_for_consignment() {
		orderExecutionHandler.clickEditOrder();
		createOrderPage.waitTillControlTowerPageLoadOut();
		createOrderPage.addEnterItemDetails("AIRPARTS", "test disc", "70", "5000", "910");
		createOrderPage.addEnterItemDetails("ALCANS", "test disc", "85", "2000", "50");
		createOrderPage.clickSaveButton();
	}

	@And("user customer rate the order")
	public void user_customer_rate_the_order() {
		customerRatingPage.waitTillCustomerRatingPageLoadOut();
		customerRatingPage.enterCustomerRating("100", "100", "50");
		// customerRatingPage.addAditinalCharges(" 3RD PARTY TARPING--TPT", "1000023",
		// "10");
		customerRatingPage.clickOnSave();
		customerRatingPage.clickOnNextStep();
	}

	@And("user edits customer rating")
	public void user_edits_customer_rating() {
		orderExecutionHandler.clickEditCustomerRating();
		customerRatingPage.waitTillCustomerRatingPageLoadOut();
		customerRatingPage.enterCustomerRating("10", "5", "60");
		customerRatingPage.clickOnSave();
	}

	@And("user carrier rate the order")
	public void user_carrier_rate_the_order() {
		carrierRatingPage.waitTillCarrierRatingPageLoadOut();
		carrierRatingPage.enterCarrierRating("CDTIPEGA", "100", "100", "50");
		carrierRatingPage.clickOnNextStep();
	}

	@And("user edits carrier rating")
	public void user_edits_carrier_rating() {
		orderExecutionHandler.clickEditCarrierRating();
		carrierRatingPage.waitTillCarrierRatingPageLoadOut();
		carrierRatingPage.enterLineHaulCharge("20");
		carrierRatingPage.enterDiscount("10");
		carrierRatingPage.clickOnSave();
	}

	@And("user send carrier confirmation mail")
	public void user_send_carrier_confirmation_mail() {
		carrierConfirmationPage.waitTillCarrierConfirmationPageLoadOut();
		carrierConfirmationPage.enterCarrierConfirmationInfo("CDTIPEGA", "test instruction", "WENDY SHELNUTT");
	}

	@And("user enters pickup logs")
	public void user_enters_pickup_logs() {
		pickUpLogPage.waitTillPickupLogPageLoadOut();
		pickUpLogPage.seletActualTimeInDate("16 April 2021 Friday");
		pickUpLogPage.seletActualTimeOutDate("16 April 2021 Friday");
		pickUpLogPage.addServiceFailure(DelayDescription.INCORRECT_ADDRESS, DelayStatus.ACTIVE_STATUS, "test123");
		pickUpLogPage.clickOnNextStep();
	}

	@And("user enter callin in tracking page")
	public void user_enter_callin_in_tracking_page() {
		System.out.println("skipping tracking page");
	}

	@And("user edits pickup logs")
	public void user_edits_pickup_logs() {
		orderExecutionHandler.clickPickUpLog();
		pickUpLogPage.waitTillPickupLogPageLoadOut();
		pickUpLogPage.seletActualTimeInDate("17 April 2021 Saturday");
		pickUpLogPage.seletActualTimeOutDate("17 April 2021 Saturday");
		pickUpLogPage.addServiceFailure(DelayDescription.INCORRECT_ADDRESS, DelayStatus.ACTIVE_STATUS, "test123");
		pickUpLogPage.clickOnSave();
	}

	@And("user enters delivery logs")
	public void user_enters_delivery_logs() {
		deliveryLogPage.waitTillDeliveryLogPageLoadOut();
		deliveryLogPage.seletActualTimeInDate("17 April 2021 Saturday");
		deliveryLogPage.seletActualTimeOutDate("17 April 2021 Saturday");
		deliveryLogPage.addServiceFailure(DelayDescription.INCORRECT_ADDRESS, DelayStatus.ACTIVE_STATUS, "test123");
		deliveryLogPage.clickOnNextStep();
	}

	@And("user edits delivery logs")
	public void user_edits_delivery_logs() {
		orderExecutionHandler.clickEditDeliveryLog();
		deliveryLogPage.waitTillDeliveryLogPageLoadOut();
		deliveryLogPage.seletActualTimeInDate("18 April 2021 Sunday");
		deliveryLogPage.seletActualTimeOutDate("18 April 2021 Sunday");
		deliveryLogPage.addServiceFailure(DelayDescription.INCORRECT_ADDRESS, DelayStatus.ACTIVE_STATUS, "test123");
		deliveryLogPage.clickOnSave();
	}

	@And("user closes the order")
	public void user_closes_the_order() {
		opsClosedPage.waitTillOpsClosedPageLoadOut();
		opsClosedPage.closeCurrentOrder();
	}

	@And("user reopen the order")
	public void user_reopen_the_order() {
		opsClosedPage.waitTillOpsClosedPageLoadOut();
		opsClosedPage.reopenCurrentOrder();
	}

	@And("user again closes the order")
	public void user_again_closes_the_order() {
		opsClosedPage.waitTillOpsClosedPageLoadOut();
		opsClosedPage.closeCurrentOrder();
	}

	@Then("verify closure of order")
	public void verify_closure_of_order() {
		String brokerageStatus = orderDetailsHeaderHandler.getBrokerageStatus();
		Assert.assertEquals("Document Completed", brokerageStatus);
	}

	@Given("user create a LTLV Order")
	public void user_create_a_LTLV_Order() {
		landingPage.waitTillControlTowerPageLoadOut();
		sideNav.clickOnCreateOrder();
		createOrderPage.verifyUserIsInCreateOrderPageMainMenu();
		createOrderPage.createLTLVOrder("1000023", OrderType.SPOT, "0002CEIA", "15 April 2021 Thursday",
				CommentType.HOT, "test", "0004FOMO", "16 April 2021 Friday", CommentType.HOT, "test", "LIQUORNOI",
				"test disc", "100", "1000", "10", "123", "123");
	}

	@Given("user create a LTLA Order")
	public void user_create_a_LTLA_Order() {
		landingPage.waitTillControlTowerPageLoadOut();
		sideNav.clickOnCreateOrder();
		createOrderPage.verifyUserIsInCreateOrderPageMainMenu();
		createOrderPage.createLTLVOrder("1000023", OrderType.CONTRACT, "0002CEIA", "15 April 2021 Thursday",
				CommentType.HOT, "test", "0004FOMO", "16 April 2021 Friday", CommentType.HOT, "test", "LIQUORNOI",
				"test disc", "100", "1000", "10", "123", "123");
	}

	@And("user enters customer and carrier rating details in integrated rating page")
	public void user_enters_customer_and_carrier_rating_details_in_integrated_rating_page() {
		itegratedRatingPage.waitTillItegratedRatingPageLoadOut();
		itegratedRatingPage.applyRating();
	}
	
	@And("user goes to control tower and click next action for the order")
	public void user_goes_to_control_tower_and_click_next_action_for_the_order() {
		sideNav.clickOnControlTower();
		landingPage.waitTillControlTowerPageLoadOut();
		landingPage.clickEditOrderForOrder(ORDER_ID);
	}
	
	@And("user add pickup log for multiple pickup location")
	public void user_add_pickup_log_for_multiple_pickup_location() {
		pickUpLogPage.waitTillPickupLogPageLoadOut();
		pickUpLogPage.seletActualTimeInDate("15 April 2021 Thursday");
		pickUpLogPage.seletActualTimeOutDate("15 April 2021 Thursday");
		pickUpLogPage.addServiceFailure(DelayDescription.INCORRECT_ADDRESS, DelayStatus.ACTIVE_STATUS, "test123");
		pickUpLogPage.seletActualTimeInDate1("15 April 2021 Thursday");
		pickUpLogPage.seletActualTimeOutDate1("16 April 2021 Friday");
		pickUpLogPage.clickOnNextStep();
	}

	@And("user add drop log for multiple drop location")
	public void user_add_drop_log_for_multiple_drop_location() {
		deliveryLogPage.waitTillDeliveryLogPageLoadOut();
		deliveryLogPage.seletActualTimeInDate1("18 April 2021 Sunday");
		deliveryLogPage.seletActualTimeOutDate1("19 April 2021 Monday");
		deliveryLogPage.seletActualTimeInDate2("20 April 2021 Tuesday");
		deliveryLogPage.seletActualTimeOutDate2("20 April 2021 Tuesday");
		deliveryLogPage.clickOnNextStep();
	}
	
	@Given("user do carrier rating from carrier offer page")
	public void user_do_carrier_rating_from_carrier_offer_page() {
		carrierRatingPage.waitTillCarrierRatingPageLoadOut();
		orderExecutionHandler.clickCarrierOfferIcon();
		commonPopupHandler.clickConfirmButton();
		carrierOfferPage.waitTillCarrierOfferPageLoadOut();
		carrierOfferPage.clickOnOfferWorkBookTab();
		carrierOfferPage.clickOnCreateOfferButton();
		carrierOfferPage.clickCarrierSearchBtn();
		carrierSearchHandler.enterCarrierID("CDTIPEGA");
		carrierSearchHandler.clickOnSearch();
		carrierSearchHandler.clickOnFirstQualifiedResult();
		carrierOfferPage.clickContactSearchBtn();
		carrierContactSearchHandler.selectContactEmail("WENDY SHELNUTT");
		carrierOfferPage.clickSaveOffer();
		carrierOfferPage.selectStatus(CarrierOfferStatus.ACCEPTED);
		carrierOfferPage.enterCounterOffer("50");
		carrierOfferPage.clickUpdateOffer();
		carrierOfferAcceptedHandler.selectPayMethod(PayMethod.FLAT);
		carrierOfferAcceptedHandler.clickOnUpdateAndGotoOrderExecutionButton();
		emailTemplateHandler.clickOnSendConfirmation();
	}
	
	@And("user undo carrier rating")
	public void user_undo_carrier_rating() {
		moreActionHandler.clickOnMoreActionBtn();
		moreActionHandler.clickOnUndoOption();
		undoPageHandler.clickOnUndoCarrierAssigned();
		carrierUnassignmentReasonHandler.selectCarrierUnassignmentReason(CarrierUnassignmentReason.AHH_CARRIER_RELATED);
		carrierUnassignmentReasonHandler.enterCarrierUnassignmentReasonComment("test");
		carrierUnassignmentReasonHandler.clickSaveButton();
		commonPopupHandler.clickConfirmButton();
	}

	@And("user reassign new carrier")
	public void user_reassign_new_carrier() {
		carrierRatingPage.waitTillCarrierRatingPageLoadOut();
		carrierRatingPage.enterCarrierRating("ADVASTMO", "200", "300", "20");
		carrierRatingPage.clickOnNextStep();
	}
	
	@After
    public void tearDown(){
		webDriverManager.closeBrowser();
		stopRecordingLog();
    }
}
