package enumRepository;

public enum DelayDescription {
	 MISSED_DELIVERY("A1"),
	 INCORRECT_ADDRESS("A2");
	
	private String value; 
	
	public String getValue() 
    { 
        return this.value; 
    } 
	
	private DelayDescription(String value) {
		this.value=value;
	}
}
