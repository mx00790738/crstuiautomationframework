package enumRepository;

public enum DelayStatus {

	ACTIVE_STATUS("A"),
	 VOID_STATUS("V");
	
	private String value; 
	
	public String getValue() 
   { 
       return this.value; 
   } 
	
	private DelayStatus(String value) {
		this.value=value;
	}

}
