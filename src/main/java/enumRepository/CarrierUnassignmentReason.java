package enumRepository;

public enum CarrierUnassignmentReason {
	AHH_CARRIER_RELATED,
	AJ_CARRIER_RELATED;
}
