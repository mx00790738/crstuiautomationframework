package testNgUtils;

import java.util.Iterator;
import java.util.Set;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;

import com.aventstack.extentreports.Status;

import reportUtils.ExtentManager;
import reportUtils.ExtentTestManager;

public class TestListener implements ITestListener {
	private static String testScenario;
	private static String testFeature;

	public void onStart(ITestContext context) {

	}

	public void onFinish(ITestContext context) {
		Iterator<ITestResult> skippedTestCases = context.getSkippedTests().getAllResults().iterator();
		while (skippedTestCases.hasNext()) {
			ITestResult skippedTestCase = skippedTestCases.next();
			ITestNGMethod method = skippedTestCase.getMethod();
			if (context.getSkippedTests().getResults(method).size() > 0) {
				skippedTestCases.remove();
			}
		}
		ExtentManager.getInstance().flush();
	}

	public void onTestStart(ITestResult result) {
		// System.out.println(("*** Running test method " +
		// result.getMethod().getMethodName() + "..."));
	}

	public void onTestSuccess(ITestResult result) {
		// System.out.println("*** Executed " + result.getMethod().getMethodName() + "
		// test successfully...");
		ExtentTestManager.logTest(testScenario);
		ExtentTestManager.getTest().log(Status.PASS, "PASSED");
	}

	public void onTestFailure(ITestResult result) {
		// System.out.println("*** Test execution " + result.getMethod().getMethodName()
		// + " failed...");
		ExtentTestManager.logTest(testScenario);
		ExtentTestManager.getTest().log(Status.FAIL, "FAILED");
	}

	public void onTestSkipped(ITestResult result) {
		// System.out.println("*** Test " + result.getMethod().getMethodName() + "
		// skipped...");
		// ExtentTestManager.getTest().log(Status.SKIP, "Test Skipped");
		// ExtentTestManager.reomveTest();
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		//System.out.println("*** Test failed but within percentage % " + result.getMethod().getMethodName());
	}

	public void setTextContext(String scenario, String feature) {
		testScenario = scenario;
		testFeature = feature;
	}

}
