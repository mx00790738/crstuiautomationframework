package testNgUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

import pageRepository.CarrierConfirmationPage;

public class RetryAnalyzer implements IRetryAnalyzer {
	private static final Logger logger = LogManager.getLogger(RetryAnalyzer.class);

	int count = 0;
    int maxRetryCount=2;

	@Override
	public boolean retry(ITestResult result) {
		if(count<maxRetryCount){
			logger.info("Retrying scenario again and the count is " + (count+1));
            count++;
            return true;
        }else {
        count = 0;
        return false;
        }
	}

}
