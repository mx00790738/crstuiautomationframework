package seleniumUtils;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

public class Waits {
	private static final Logger logger = LogManager.getLogger(Waits.class);

	private WebDriver driver;
	public final int SHORT_TIME_OUT_MILLISECONDS = 5000;
	public final int MEDIUM_TIME_OUT_MILLISECONDS = 10 * 1000;
	public final int LONG_TIME_OUT_MILLISECONDS = 15 * 1000;
	public final int SHORT_TIME_OUT_IN_SECONDS = 5;
	public final int MEDIUM_TIME_OUT_IN_SECONDS = 10;
	public final int LONG_TIME_OUT_IN_SECONDS = 15;
	public final int MAX_TIME_OUT_IN_SECONDS = 60;
	public final int POLLING_INTERVAL_IN_SECONDS = 2;

	public Waits(WebDriver driver) {
		this.driver = driver;
	}

	public boolean waitTillVisibilityOfElement(By locator) {
		try {
			logger.info("Waiting for visibilty of element with locator [" + locator + "] .....");
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
			wait.withTimeout(MAX_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS);
			wait.pollingEvery(POLLING_INTERVAL_IN_SECONDS, TimeUnit.SECONDS);
			wait.ignoring(NoSuchElementException.class);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			return true;
		} catch (TimeoutException e) {
			logger.error("Visiblity of element with locator [" + locator + "] is not found even after waiting for "
					+ MAX_TIME_OUT_IN_SECONDS + "secconds");
			return false;
		}
	}

	public boolean waitTillInVisibilityOfElement(By locator) {
		try {
			logger.info("Waiting for invisibilty of element with locator [" + locator + "] .....");
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
			wait.withTimeout(MAX_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS);
			wait.pollingEvery(POLLING_INTERVAL_IN_SECONDS, TimeUnit.SECONDS);
			wait.ignoring(NoSuchElementException.class);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
			return true;
		} catch (TimeoutException e) {
			logger.error("Invisiblity of element with locator [" + locator + "] is not achieved even after waiting for "
					+ MAX_TIME_OUT_IN_SECONDS + "seconds");
			return false;
		}
	}

	public boolean waitTillPresenceOfElement(By locator) {
		try {
			logger.info("Waiting for presence of element with locator [" + locator + "] .....");
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
			wait.withTimeout(MAX_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS);
			wait.pollingEvery(POLLING_INTERVAL_IN_SECONDS, TimeUnit.SECONDS);
			wait.ignoring(NoSuchElementException.class);
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			return true;
		} catch (TimeoutException e) {
			logger.error("Presence of element with locator [" + locator + "] is not found even after waiting for "
					+ MAX_TIME_OUT_IN_SECONDS + "seconds");
			return false;
		}
	}

	public boolean waitTillElementIsClickable(By locator) {
		try {
			logger.info("Waiting for clickability of element with locator [" + locator + "] .....");
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
			wait.withTimeout(MAX_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS);
			wait.pollingEvery(POLLING_INTERVAL_IN_SECONDS, TimeUnit.SECONDS);
			wait.ignoring(ElementNotInteractableException.class);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			return true;
		} catch (TimeoutException e) {
			logger.error("Element with locator [" + locator + "] is not clickable even after waiting for "
					+ MAX_TIME_OUT_IN_SECONDS + "seconds");
			return false;
		}
	}

	public void waitImplicitly() {
		logger.info("Waiting implicitly for " + MEDIUM_TIME_OUT_MILLISECONDS + "seconds");
		driver.manage().timeouts().implicitlyWait(MEDIUM_TIME_OUT_MILLISECONDS, TimeUnit.MILLISECONDS);
	}

	public void sleep() {
		try {
			logger.info("Thread sleep for " + SHORT_TIME_OUT_IN_SECONDS + "seconds");
			Thread.sleep(SHORT_TIME_OUT_IN_SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void sleep(long milli) {
		try {
			logger.info("Thread sleep for " + milli + "seconds");
			Thread.sleep(milli);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
