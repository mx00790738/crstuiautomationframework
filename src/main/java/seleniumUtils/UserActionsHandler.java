package seleniumUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class UserActionsHandler {
	private static final Logger logger = LogManager.getLogger(UserActionsHandler.class);

	private WebDriver driver;
	Waits wait;
	WebElementsUtils webElementsUtils;

	public UserActionsHandler(WebDriver driver) {
		this.driver = driver;
		wait = new Waits(driver);
		webElementsUtils = new WebElementsUtils(driver);
	}

	public void scrollToTopOfPage() {
		wait.sleep(1000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,0)");
		wait.sleep(1000);
	}

	public void scrollToElement(WebElement element) {
		wait.sleep(1000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", element);
		wait.sleep(1000);
	}

	public Actions moveToElement(WebElement element) {
		wait.sleep(1000);
		Actions actions = new Actions(driver);
		return actions.moveToElement(element);
	}

	public void performPageUp() {
		wait.sleep(1000);
		logger.debug("Performing page up");
		Actions action = new Actions(driver);
		action.sendKeys(Keys.PAGE_UP).build().perform();
		wait.sleep(1000);
	}

	public void performPageDown() {
		wait.sleep(1000);
		logger.debug("Performing page down");
		Actions action = new Actions(driver);
		action.sendKeys(Keys.PAGE_DOWN).build().perform();
		wait.sleep(1000);
	}

	public void performClick() {
		wait.sleep(1000);
		Actions action = new Actions(driver);
		action.click();
		wait.sleep(1000);
	}

	public boolean getElementInViewport(By locator) {
		logger.debug("Trying to get element with locator [" + locator.toString() + "] in view port area");
		boolean isInViewPort = webElementsUtils.isElementVisibleInViewport(locator);
		int counter = 0;
		while (!isInViewPort && counter < 2) {
			logger.debug("Element with locator [" + locator.toString() + "] is not in view port area");
			if (counter < 2) {
				performPageUp();
				isInViewPort = webElementsUtils.isElementVisibleInViewport(locator);
				counter++;
				if (isInViewPort) {
					break;
				}
			}
		}
		counter = 0;
		while (!isInViewPort && counter < 2) {
			logger.debug("Element with locator [" + locator.toString() + "] is not in view port area");
			if (counter < 2) {
				performPageDown();
				isInViewPort = webElementsUtils.isElementVisibleInViewport(locator);
				counter++;
				if (isInViewPort) {
					break;
				}
			}
		}
		if (isInViewPort) {
			logger.debug("Element with locator [" + locator.toString() + "] is in view port area");
			return true;
		} else {
			logger.debug("Not able to get the Element with locator [" + locator.toString() + "] in view port area");
			return false;
		}
	}

	public boolean clickOnElement(By locator) {
		boolean isClicked = false;
		if (webElementsUtils.checkElementIsDisplayed(locator)) {
			try {
				logger.debug("Clicking on element with locator [" + locator.toString() + "]");
				webElementsUtils.getElement(locator).click();
				logger.debug("Clicked on element with locator [" + locator.toString() + "]");
				isClicked = true;
			} catch (StaleElementReferenceException e) {
				logger.warn("Stale element exception for element with locator [" + locator.toString() + "]");
				logger.debug("Finding element again with locator [" + locator.toString() + "]");
				webElementsUtils.getElement(locator).click();
				isClicked = true;
			} catch (ElementClickInterceptedException e) {
				logger.warn("ElementClickInterceptedException when clicking element with locator [" + locator.toString()
						+ "]");
				logger.debug("Using JavaScript executor to click element with locator [" + locator.toString() + "]");
				wait.sleep();
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", webElementsUtils.getElement(locator));
				isClicked = true;
			}
		} else {
			logger.error("Element with locator [" + locator.toString() + "] not found so no click performed");
		}
		return isClicked;
	}

	public boolean enterTextInTextBox(By locator, String str) {
		boolean isEntered;
		WebElement element = webElementsUtils.getElement(locator);
		try {
		logger.debug("Trying to enter \"" + str + "\" in text box with locator [" + locator.toString() + "]");
		element.clear();
		element.sendKeys(str);
		isEntered = true;
		logger.info("Enter \"" + str + "\" in text box with locator [" + locator.toString() + "]");
		} catch (Exception e) {
			logger.error("Exception when entering text in text box: "+e.getMessage());
			isEntered = false;
		}
		return isEntered;
	}
}
