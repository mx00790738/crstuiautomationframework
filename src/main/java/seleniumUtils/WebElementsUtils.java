package seleniumUtils;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.FluentWait;

import com.google.common.base.Function;

public class WebElementsUtils {
	private static final Logger logger = LogManager.getLogger(WebElementsUtils.class);
	private WebDriver driver;
	private final int MAX_TIME_OUT_IN_SECONDS = 60;
	private final int POLLING_INTERVAL_IN_SECONDS = 2;
	
	public WebElementsUtils(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getElement(By locator) {
		try {
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
			wait.withTimeout(MAX_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS);
			wait.pollingEvery(POLLING_INTERVAL_IN_SECONDS, TimeUnit.SECONDS);
			wait.ignoring(NoSuchElementException.class,StaleElementReferenceException.class);
			WebElement element = wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return driver.findElement(locator);
				}
			});
			if (element != null) {
				logger.debug("getElement() -- Element with locator [" + locator.toString()
						+ "] is found in page. Returning WebElement");
			}
			return element;
		} catch (TimeoutException e) {
			logger.error("getElement() -- Element with locator [" + locator.toString()
					+ "] is not found in page. Returning NULL");
			return null;
		}
	}

	public String getTextFromElement(By locator) {
		try {
			logger.debug("Trying to get text from WebElement with locator [" + locator.toString() + "]");
			String text = getElement(locator).getText();
			logger.debug("Returning text["+text+"] from WebElement with locator [" + locator.toString() + "]");
			return text;
		} catch (Exception e) {
			logger.error("Exception [" + e.getMessage() + "] when getting text from WebElement with locator ["
					+ locator.toString() + "]");
			return null;
		}
	}

	public boolean checkElementIsDisplayed(By locator) {
		try {
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
			wait.withTimeout(MAX_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS);
			wait.pollingEvery(POLLING_INTERVAL_IN_SECONDS, TimeUnit.SECONDS);
			wait.ignoring(NoSuchElementException.class,StaleElementReferenceException.class);
			wait.until(new Function<WebDriver, Boolean>() {
				public Boolean apply(WebDriver driver) {
					logger.debug(
							"Checking Element with locator [" + locator.toString() + "] is displayed or not......");
					return driver.findElement(locator).isDisplayed();
				}
			});
			logger.info("Element with locator [" + locator.toString() + "] is displayed");
			return true;
		} catch (TimeoutException e) {
			logger.error("Element with locator [" + locator.toString() + "] not displayed");
			return false;
		}
	}

	public boolean checkElementIsEnabled(By locator) {
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
		wait.withTimeout(MAX_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS);
		wait.pollingEvery(POLLING_INTERVAL_IN_SECONDS, TimeUnit.SECONDS);
		wait.ignoring(NoSuchElementException.class,StaleElementReferenceException.class);
		boolean status = wait.until(new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver driver) {
				logger.debug("Checking Element with locator [" + locator.toString() + "] is enabled or not......");
				return driver.findElement(locator).isEnabled();
			}
		});
		if (status) {
			logger.info("Element with locator [" + locator.toString() + "] is enabled");
			return true;
		} else {
			logger.error("Element with locator [" + locator.toString() + "] not enabled");
			return false;
		}
	}

	public boolean checkElementIsDisplayedAndEnabled(By locator) {
		if (checkElementIsDisplayed(locator) && checkElementIsEnabled(locator)) {
			return true;
		}
		return false;
	}

	/*
	 * public boolean isElementVisibleInViewport(By locator) { WebElement element =
	 * getElement(locator); JavascriptExecutor js = (JavascriptExecutor) driver;
	 * boolean isInViewPort = (boolean)js.
	 * executeScript(" var rect = arguments[0].getBoundingClientRect(),\r\n" +
	 * "        vWidth   = window.innerWidth || document.documentElement.clientWidth,\r\n"
	 * +
	 * "        vHeight  = window.innerHeight || document.documentElement.clientHeight,\r\n"
	 * +
	 * "        efp      = function (x, y) { return document.elementFromPoint(x, y) };     \r\n"
	 * + "\r\n" + "    // Return false if it's not in the viewport\r\n" +
	 * "    if (rect.right < 0 || rect.bottom < 0 \r\n" +
	 * "            || rect.left > vWidth || rect.top > vHeight)\r\n" +
	 * "        return false;\r\n" + "\r\n" +
	 * "    // Return true if any of its four corners are visible\r\n" +
	 * "    return (\r\n" +
	 * "          arguments[0].contains(efp(rect.left,  rect.top))\r\n" +
	 * "      ||  arguments[0].contains(efp(rect.right, rect.top))\r\n" +
	 * "      ||  arguments[0].contains(efp(rect.right, rect.bottom))\r\n" +
	 * "      ||  arguments[0].contains(efp(rect.left,  rect.bottom))\r\n" +
	 * "    );",element); return isInViewPort; }
	 */
	public boolean isElementVisibleInViewport(By locator) {
		WebElement element = getElement(locator);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		boolean isInViewPort = (boolean) js.executeScript(
				"var elem = arguments[0],                 " + "  box = elem.getBoundingClientRect(),    "
						+ "  cx = box.left + box.width / 2,         " + "  cy = box.top + box.height / 2,         "
						+ "  e = document.elementFromPoint(cx, cy); " + "for (; e; e = e.parentElement) {         "
						+ "  if (e === elem)                        " + "    return true;                         "
						+ "}                                        " + "return false;                            ",
				element);
		return isInViewPort;
	}
}
