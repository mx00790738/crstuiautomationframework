package utilities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConfigFileReader {
	private static final Logger logger = LogManager.getLogger(ConfigFileReader.class);
	
	private Properties properties;
	private final String propertyFilePath= "configs//Configuration.properties";

	
	public ConfigFileReader(){
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		}		
	}

	public String getApplicationURL(){
			logger.info("Reading url value from property file");
			String url = properties.getProperty("url");
			if(url!= null) {
				return url;
			}
			else throw new RuntimeException("url not specified in the Configuration.properties file.");		
		}

	public String getBrowser() {
		logger.info("Reading browser value from property file");
		String browser = properties.getProperty("browser");
		if (browser != null) {
			return browser;
		}
		else
			throw new RuntimeException("browser not specified in the Configuration.properties file.");
	}
	
	public String getAgentUserName() {
		logger.info("Reading agent user name value from property file");
		String agentUser = properties.getProperty("agentuser");
		if (agentUser != null) {
			return agentUser;
		}
		else
			throw new RuntimeException("agentuser not specified in the Configuration.properties file.");
	}
	public String getAgentPassWord() {
		logger.info("Reading agent password name value from property file");
		String agentPwd = properties.getProperty("agentpwd");
		if (agentPwd != null) {
			return agentPwd;
		}
		else
			throw new RuntimeException("agentpwd not specified in the Configuration.properties file.");
	}
	public String getCustomerUserName() {
		logger.info("Reading customer user name value from property file");
		String customerUser = properties.getProperty("customeruser");
		if (customerUser != null) {
			return customerUser;
		}
		else
			throw new RuntimeException("customeruser not specified in the Configuration.properties file.");
	}
	public String getCustomerPassWord() {
		logger.info("Reading customer user password value from property file");
		String customerPwd = properties.getProperty("customerpwd");
		if (customerPwd != null) {
			return customerPwd;
		}
		else
			throw new RuntimeException("customerpwd not specified in the Configuration.properties file.");
	}
	public String getPowerUserName() {
		logger.info("Reading power user name value from property file");
		String powerUser = properties.getProperty("poweruser");
		if (powerUser != null) {
			return powerUser;
		}
		else
			throw new RuntimeException("poweruser not specified in the Configuration.properties file.");
	}
	public String getPowerPassWord() {
		logger.info("Reading power user password name value from property file");
		String powerPwd = properties.getProperty("powerpwd");
		if (powerPwd != null) {
			return powerPwd;
		}
		else
			throw new RuntimeException("powerpwd not specified in the Configuration.properties file.");
	}	
	public String getReportConfigPath(){
		String reportConfigPath = properties.getProperty("reportConfigPath");
		if(reportConfigPath!= null) return reportConfigPath;
		else throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:reportConfigPath");		
	}
}
