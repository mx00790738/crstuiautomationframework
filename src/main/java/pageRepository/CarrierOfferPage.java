package pageRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import basePage.BasePage;
import enumRepository.CarrierOfferStatus;

public class CarrierOfferPage extends BasePage{
	private static final Logger logger = LogManager.getLogger(CarrierOfferPage.class);

	public CarrierOfferPage(WebDriver driver) {
		super(driver);
	}

	public static final By OFFER_WORKBOOK_TAB_LOCATOR = By.xpath("//*[@id=\"tabs6\"]");
	public static final By CREATE_NEW_BTN_OFFER_WORKBOOK_TAB_LOCATOR = By.id("createNewButton");
	public static final By CARRIER_SEARCH_BUTTON = By.id("carrierIdBtnofferWorkbook");
	public static final By CONTACT_SEARCH_BUTTON = By.xpath("//*[@id=\"offerAccoridian2\"]/div/div[1]/div[5]/div[1]/div/button");
	public static final By SAVE_OFFER_BUTTON = By.id("saveOfferButton");
	public static final By UPDATE_OFFER_BUTTON = By.id("updateOfferButton");
	public static final By CARRIER_OFFER_STATUS= By.xpath("//select[@id=\"status\"]");
	public static final By COUNTER_OFFER_ENTRY_TEXT_BOX= By.xpath("//*[@id=\"counterOffer\"]");
	public static final By CARRIER_OFFER_PAGE_LOCATOR= By.xpath("//label[contains(text(),'Carrier Offer')]");

	public void clickOnOfferWorkBookTab() {
		boolean isClicked;
		logger.debug("Clicking on offer workbook tab.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(OFFER_WORKBOOK_TAB_LOCATOR)) {
			userActions.getElementInViewport(OFFER_WORKBOOK_TAB_LOCATOR);
			isClicked = userActions.clickOnElement(OFFER_WORKBOOK_TAB_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on offer workbook tab");
			}
			logger.info("Clicked offer workbook tab");
		} else {
			logger.error("offer workbook tab is not present or diabled");
			throw new RuntimeException("offer workbook tab is not present or diabled");
		}
	}
	
	public void clickOnCreateOfferButton() {
		boolean isClicked;
		logger.debug("Clicking on create new btn in offer workbook tab.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CREATE_NEW_BTN_OFFER_WORKBOOK_TAB_LOCATOR)) {
			userActions.getElementInViewport(CREATE_NEW_BTN_OFFER_WORKBOOK_TAB_LOCATOR);
			isClicked = userActions.clickOnElement(CREATE_NEW_BTN_OFFER_WORKBOOK_TAB_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on create new btn in offer workbook tab");
			}
			logger.info("Clicked create new btn in offer workbook tab");
		} else {
			logger.error("create new btn in offer workbook tab is not present or diabled");
			throw new RuntimeException("create new btn in offer workbook tab is not present or diabled");
		}
	}
	
	public void clickCarrierSearchBtn() {
		boolean isClicked;
		logger.debug("Clicking on carrier serach button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CARRIER_SEARCH_BUTTON)) {
			userActions.getElementInViewport(CARRIER_SEARCH_BUTTON);
			isClicked = userActions.clickOnElement(CARRIER_SEARCH_BUTTON);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on carrier serach button");
			}
			logger.info("Clicked carrier serach button");
		} else {
			logger.error("carrier serach button is not present or diabled");
			throw new RuntimeException("carrier serach butto is not present or diabled");
		}
	}
	
	public void clickContactSearchBtn() {
		boolean isClicked;
		logger.debug("Clicking on contact serach button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CARRIER_SEARCH_BUTTON)) {
			userActions.getElementInViewport(CARRIER_SEARCH_BUTTON);
			isClicked = userActions.clickOnElement(CARRIER_SEARCH_BUTTON);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on contact serach button");
			}
			logger.info("Clicked contact serach button");
		} else {
			logger.error("contact serach button is not present or diabled");
			throw new RuntimeException("contact serach button is not present or diabled");
		}
	}
	
	public void clickSaveOffer() {
		boolean isClicked;
		logger.debug("Clicking on save offer button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(SAVE_OFFER_BUTTON)) {
			userActions.getElementInViewport(SAVE_OFFER_BUTTON);
			isClicked = userActions.clickOnElement(SAVE_OFFER_BUTTON);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on save offer button");
			}
			logger.info("Clicked save offer button");
		} else {
			logger.error("save offer button is not present or diabled");
			throw new RuntimeException("save offer button is not present or diabled");
		}
	}
	
	public void clickUpdateOffer() {
		boolean isClicked;
		logger.debug("Clicking on update offer button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(UPDATE_OFFER_BUTTON)) {
			userActions.getElementInViewport(UPDATE_OFFER_BUTTON);
			isClicked = userActions.clickOnElement(UPDATE_OFFER_BUTTON);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on update offer button");
			}
			logger.info("Clicked update offer button");
		} else {
			logger.error("update offer button is not present or diabled");
			throw new RuntimeException("update offer button is not present or diabled");
		}
	}
	
	public void selectStatus(CarrierOfferStatus offerstatus) {
		logger.info("Selecting carrier offer status: " + offerstatus.toString() + " .....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CARRIER_OFFER_STATUS)) {
			WebElement orderTypeSelectDropDown = webElementsUtils.getElement(CARRIER_OFFER_STATUS);
			Select se = new Select(orderTypeSelectDropDown);
			switch (offerstatus) {
			case ACCEPTED:
				se.selectByValue("A");
				break;
			case DECLINED:
				se.selectByValue("D");
				break;
			}
			logger.info("Selected carrier offer status: " + offerstatus.toString());
		} else {
			logger.error("carrier offer status drop down is not displayed or disabled");
		}
	}
	
	public void enterCounterOffer(String counterOffer) {
		boolean isTextEntered;
		logger.debug("Entering counter offer.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(COUNTER_OFFER_ENTRY_TEXT_BOX)) {
			userActions.getElementInViewport(COUNTER_OFFER_ENTRY_TEXT_BOX);
			isTextEntered = userActions.enterTextInTextBox(COUNTER_OFFER_ENTRY_TEXT_BOX, counterOffer);
			// not sure why i added below code need to check--START
			userActions.clickOnElement(COUNTER_OFFER_ENTRY_TEXT_BOX);
			// END
			if (!isTextEntered) {
				throw new RuntimeException("Not able enter counter offer:" + counterOffer + " in counterOffer entry text box");
			}
			logger.info("Entered counterOffer: " + counterOffer);
		} else {
			logger.error("counterOffer entry text box is not present or diabled");
			throw new RuntimeException("counterOffer entry text box is not present or diabled");
		}
	}
		public void waitTillCarrierOfferPageLoadOut() {
			Boolean isInCarrierConfirmationPage = waits.waitTillVisibilityOfElement(CARRIER_OFFER_PAGE_LOCATOR);
			if (!isInCarrierConfirmationPage) {
				logger.error("CarrierRatingPage loadout failed");
				throw new RuntimeException("CarrierRatingPage loadout failed");
			}
			logger.info("CarrierRatingPage loadout successful");
		}
}
