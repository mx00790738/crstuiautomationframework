package pageRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import basePage.BasePage;
import managers.FileReaderManager;

public class LoginPage extends BasePage{
	
	private static final Logger logger = LogManager.getLogger(LoginPage.class);
	public static final By LOGIN_BUTTON =  By.id("loginBtn");
	public static final By USER_NAME_TXT_BOX =  By.id("loginEmail");
	public static final By PASSWORD_TXT_BOX =  By.id("loginPswd");
	public static final By LOGIN_BACKGROUND =  By.xpath("//div[contains(@class, 'loginBackground')]");

	public LoginPage(WebDriver driver) {
		 super(driver);
	}
	
	public void testmethod() {
		logger.info("Logging in as Power user");
		System.out.println("this is test method");
	} 
	
	public void loginAsAgent() {
		String userName = FileReaderManager.getInstance().getConfigReader().getAgentUserName();
		String pwd = FileReaderManager.getInstance().getConfigReader().getAgentPassWord();
		logger.info("Logging in as Agent user");
		loginWithCredentials(userName,pwd);	
	}
	
    public void loginAsPowerUser() {
    	String userName = FileReaderManager.getInstance().getConfigReader().getPowerUserName();
		String pwd = FileReaderManager.getInstance().getConfigReader().getPowerPassWord();
    	logger.info("Logging in as Power user");
    	loginWithCredentials(userName,pwd);
	}
    
    public void loginAsCustomerUser() {
    	String userName = FileReaderManager.getInstance().getConfigReader().getCustomerUserName();
		String pwd = FileReaderManager.getInstance().getConfigReader().getCustomerPassWord();
    	logger.info("Logging in as customer user");
    	loginWithCredentials(userName,pwd);
	}
	
    public void loginWithCredentials(String username, String password) {
    	logger.info("Logging with username: "+username+" password: "+password);
    	enterUserName(username);
    	enterPassword(password);
    	waits.waitTillVisibilityOfElement(LOGIN_BUTTON);
    	clickLoginButton();
	}
    
	private void enterUserName(String usrName) {
		WebElement usrNameTxtBox = webElementsUtils.getElement(USER_NAME_TXT_BOX);
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(USER_NAME_TXT_BOX)) {
		   logger.debug("Entering username in usrNameTxtBox");    	
		   usrNameTxtBox.sendKeys(usrName);
		}else {
			logger.error("usrNameTxtBox is not present/enabled");
		}
	}
	
	private void enterPassword(String pwd) {
		WebElement pwdTxtBox = webElementsUtils.getElement(PASSWORD_TXT_BOX);
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(PASSWORD_TXT_BOX)) {
			logger.debug("Entering password in pwdTxtBox");
			pwdTxtBox.sendKeys(pwd);
		}else {
			logger.error("pwdTxtBox is not present/enabled");
		}
	}
	
	private void clickLoginButton() {
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(LOGIN_BUTTON)) {
			userActions.clickOnElement(LOGIN_BUTTON);
		}else {
			logger.error("Login button is not present");
		}
		waits.waitTillInVisibilityOfElement(LOGIN_BUTTON);
	}
	
	public boolean verifyUserIsInLoginPage() {
		String currentURL = getCurrentPageURL();
		if(currentURL.contains("login")) {
			return true;
		}
		return false;
	}
	
	public void waitTillLoginPageLoadOut() {
		Boolean isInLoginPage = waits.waitTillVisibilityOfElement(LOGIN_BACKGROUND);
		if(!isInLoginPage) {
		logger.error("Login page loadout failed");	
		throw new RuntimeException("Login page loadout failed");
		}
		logger.info("Login page loadout successful");
	}
}
