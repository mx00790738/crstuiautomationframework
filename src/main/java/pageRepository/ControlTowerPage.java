package pageRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import basePage.BasePage;

public class ControlTowerPage extends BasePage {
	private static final Logger logger = LogManager.getLogger(ControlTowerPage.class);

	public static final By CONTROL_TOWER_PAGE_LOCATOR = By.xpath("//label[contains(text(),'Control Tower')]");
	public static final By ORDER_LIST_CONTAINER_LOCATOR = By.className("ui-datatable-scrollable-view");
	public static final By ORDER_LIST_EMPTY_CONTAINER_LOCATOR = By
			.xpath("//*[@id=\"activeTabContent\"]/div/div/div[4]");

	public static final By PAGE_TITLE_LOCATOR = By
			.xpath("//*[@id=\"floatingTitle\"]/app-breadcrumb/div/div/div/div/label");

	public ControlTowerPage(WebDriver driver) {
		super(driver);
	}

	/*
	 * This method Verify user is in control tower page
	 * 
	 * @return true if page is control tower page false if page is not control tower
	 * page
	 */
	public boolean verifyUserIsInControlTower() {
		String pageTitle = getPageTitle();
		Boolean IsControlTower = pageTitle.equalsIgnoreCase("Control Tower");
		logger.debug("verifying current page is landing page");
		if (IsControlTower) {
			logger.info("Current page is landing page");
			return true;
		}
		return false;
	}

	public boolean waitTillOrderListIsLoaded() {
		String pageTitle = getPageTitle();
		Boolean IsControlTower = pageTitle.equalsIgnoreCase("Control Tower");
		logger.debug("verifying current page is landing page");
		if (IsControlTower) {
			logger.info("Current page is landing page");
			return true;
		}
		return false;
	}

	public String getNextActionForOrder(String orderID) {
		String xpath = "//*[@id=\"ordId" + orderID
				+ "\"]/ancestor::span/ancestor::td/following-sibling::td[1]/span/span";
		waits.waitImplicitly();
		return webElementsUtils.getElement(By.xpath(xpath)).getText();
	}

	public void clickEditOrderForOrder(String orderID) {
		String xpath = "//*[@id=\"ordId" + orderID
				+ "\"]/ancestor::span/ancestor::td/following-sibling::td[1]/span/span[2]/a/span[contains(@ptooltip,\"Edit\")]";
		waits.waitImplicitly();
		userActions.getElementInViewport(By.xpath(xpath));
		userActions.clickOnElement(By.xpath(xpath));
	}

	private String getPageTitle() {
		logger.debug("Getting current page title");
		waits.waitTillVisibilityOfElement(PAGE_TITLE_LOCATOR);
		WebElement pageTitleText = webElementsUtils.getElement(PAGE_TITLE_LOCATOR);
		return pageTitleText.getText();
	}

	public void waitTillControlTowerPageLoadOut() {
		if (webElementsUtils.checkElementIsDisplayed(CONTROL_TOWER_PAGE_LOCATOR)) {
			logger.info("ControlTowerPage loadout successful");
		}else {
			Boolean isInControlTowerPage = waits.waitTillVisibilityOfElement(CONTROL_TOWER_PAGE_LOCATOR);
			if (!isInControlTowerPage) {
				logger.error("ControlTowerPage loadout failed");
				throw new RuntimeException("ControlTowerPage loadout failed");
			}
			logger.info("ControlTowerPage loadout successful");
		}
	}
}