package pageRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import basePage.BasePage;

public class CarrierConfirmationPage extends BasePage {
	private static final Logger logger = LogManager.getLogger(CarrierConfirmationPage.class);

	public CarrierConfirmationPage(WebDriver driver) {
		super(driver);
	}

	public static final By NEXT_STEP_BTN_LOCATOR = By.id("savenclose");
	public static final By INSTRUCTIONS_TXT_AREA_LOCATOR = By
			.xpath("//*[@id=\"view\"]/div/div[3]/div/div/form/div/div[3]/div[2]/textarea");
	public static final By CONTACT_SEARCH_BTN_LOCATOR = By.xpath("//*[@id=\"contactEmailBtn\"]");
	public static final By PAGE_TITLE_LOCATOR = By
			.xpath("//*[@id=\"floatingHeader\"]/app-breadcrumb/div/div/div/div/label");
	public static final By CARRIER_CONFIRMATION_PAGE_LOCATOR = By
			.xpath("//label[contains(text(),'Carrier Confirmation')]");

	public void enterCarrierConfirmationInfo(String carrierId, String instruction, String contactName) {
		addInstructions(instruction);
		selectContactEmail(contactName);
		clickOnNextStep();
		emailTemplateHandler.clickOnSendConfirmation();
		waits.sleep();
	}

	public void addInstructions(String instruction) {
		boolean isTextEntered;
		logger.debug("Entering additional instruction.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(INSTRUCTIONS_TXT_AREA_LOCATOR)) {
			userActions.getElementInViewport(INSTRUCTIONS_TXT_AREA_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(INSTRUCTIONS_TXT_AREA_LOCATOR, "\n " + instruction);
			if (!isTextEntered) {
				throw new RuntimeException("Not able to enter additional instruction");
			}
			logger.info("Entered additional instruction");
		} else {
			logger.error("Additional instruction entry text box is not present or diabled");
			throw new RuntimeException("Additional instruction entry text box is not present or diabled");
		}
	}

	public void selectContactEmail(String contactName) {
		logger.debug("Selecting contact email.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CONTACT_SEARCH_BTN_LOCATOR)) {
			userActions.getElementInViewport(CONTACT_SEARCH_BTN_LOCATOR);
			userActions.clickOnElement(CONTACT_SEARCH_BTN_LOCATOR);
			carrierContactSearchHandler.selectContactEmail(contactName);
			logger.info("Selected contact email");
		} else {
			logger.error("Contact search button is not present or diabled");
			throw new RuntimeException("Contact search button is not present or diabled");
		}
	}

	public void clickOnNextStep() {
		waits.sleep();
		logger.debug("Clicking on Next Step button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(NEXT_STEP_BTN_LOCATOR)) {
			userActions.getElementInViewport(NEXT_STEP_BTN_LOCATOR);
			userActions.clickOnElement(NEXT_STEP_BTN_LOCATOR);
			logger.info("Clicked  Next Step button");
			waits.waitTillInVisibilityOfElement(LOADING_SPINNER);
		} else {
			logger.error("Next Step button is not present or diabled");
			throw new RuntimeException("Next Step button is not present or diabled");
		}
	}

	/**
	 * method to verify if user is in carrier confirm page
	 */
	public boolean verifyUserIsInCarrierConfirmationPage() {
		String pageTitle = getPageTitle();
		Boolean isOrderCreatePage = pageTitle.equalsIgnoreCase("Carrier Confirmation");
		logger.debug("verifying current page is carrier confirmation page");
		if (isOrderCreatePage) {
			logger.info("Current page is carrier confirmation page");
			return true;
		}
		return false;
	}

	/**
	 * method to verify if user is in edit carrier confirm
	 */
	public boolean verifyUserIsInEditCarrierConfirmationPage() {
		String pageTitle = getPageTitle();
		Boolean isOrderCreatePage = pageTitle.equalsIgnoreCase("Edit Carrier Confirmation");
		logger.debug("verifying current page is edit carrier confirmation page");
		if (isOrderCreatePage) {
			logger.info("Current page is edit carrier confirmation page");
			return true;
		}
		return false;
	}

	/**
	 * method to get page title
	 */
	private String getPageTitle() {
		logger.debug("Getting current page title");
		waits.waitTillVisibilityOfElement(PAGE_TITLE_LOCATOR);
		WebElement pageTitleText = webElementsUtils.getElement(PAGE_TITLE_LOCATOR);
		return pageTitleText.getText();
	}

	public void waitTillCarrierConfirmationPageLoadOut() {
		Boolean isInCarrierConfirmationPage = waits.waitTillVisibilityOfElement(CARRIER_CONFIRMATION_PAGE_LOCATOR);
		if (!isInCarrierConfirmationPage) {
			logger.error("CarrierConfirmationPage loadout failed");
			throw new RuntimeException("CarrierConfirmationPage loadout failed");
		}
		logger.info("CarrierConfirmationPage loadout successful");
	}
}
