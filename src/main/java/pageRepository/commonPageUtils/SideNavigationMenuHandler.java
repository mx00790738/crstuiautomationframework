package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import basePage.BasePage;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class SideNavigationMenuHandler {
	private static final Logger logger = LogManager.getLogger(SideNavigationMenuHandler.class);
	public static final By SIDE_NAV_LOCATOR = By.id("Sidenav");
	public static final By CONTROL_TOWER_LOCATOR = By.id("orderHomeLi");
	public static final By CREATE_ORDER_LOCATOR = By.id("createOrderLi");
	public static final By CARRIER_OFFER_LOCATOR = By.id("carrierOfferLi");
	public static final By DOCUMENTS_LOCATOR = By.id("invoicesLi");
	public static final By REPORT_LOCATOR = By.id("ReportsLi");
	public static final By QUOTES_OFFER_LOCATOR = By.id("quoteDetailsLi");
	public static final By MASTER_FILE_LOCATOR = By.id("customerProfileLi");
	public static final By SETTINGS_LOCATOR = By.id("settingsLi");

	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;

	public SideNavigationMenuHandler(WebDriver driver) {
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}

	public void clickOnControlTower() {
		boolean isClicked;
		logger.debug("Clicking on edit order button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CONTROL_TOWER_LOCATOR)) {
			//userActions.getElementInViewport(CONTROL_TOWER_LOCATOR);
			isClicked = userActions.clickOnElement(CONTROL_TOWER_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on control tower icon in side nav");
			}
			logger.info("Clicked on control tower icon in side nav");
		} else {
			logger.error("Control tower icon in side nav is not present or diabled");
			throw new RuntimeException("Control tower icon in side nav is not present or diabled");
		}
	}

	public void clickOnCreateOrder() {
		boolean isClicked;
		logger.debug("Clicking on create order icon in side nav.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CREATE_ORDER_LOCATOR)) {
			//userActions.getElementInViewport(CREATE_ORDER_LOCATOR);
			isClicked = userActions.clickOnElement(CREATE_ORDER_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on control tower icon in side nav");
			}
			logger.info("Clicked on create order icon in side nav");
		} else {
			logger.error("Create order icon in side nav is not present or diabled");
			throw new RuntimeException("Create order icon in side nav is not present or diabled");
		}
	}

	public void clickOnCarrierOffer() {
		boolean isClicked;
		logger.debug("Clicking on carrier offer icon in side nav.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CARRIER_OFFER_LOCATOR)) {
			//userActions.getElementInViewport(CARRIER_OFFER_LOCATOR);
			isClicked = userActions.clickOnElement(CARRIER_OFFER_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on carrier offer icon in side nav");
			}
			logger.info("Clicked on carrier offer icon in side nav");
		} else {
			logger.error("Carrier offer icon in side nav is not present or diabled");
			throw new RuntimeException("Carrier offer icon in side nav is not present or diabled");
		}
	}

	public void clickOnDocuments() {
		boolean isClicked;
		logger.debug("Clicking on documents icon in side nav.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(DOCUMENTS_LOCATOR)) {
			//userActions.getElementInViewport(DOCUMENTS_LOCATOR);
			isClicked = userActions.clickOnElement(DOCUMENTS_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on documents icon in side nav");
			}
			logger.info("Clicked on documents icon in side nav");
		} else {
			logger.error("Documents icon in side nav is not present or diabled");
			throw new RuntimeException("Documents icon in side nav is not present or diabled");
		}
	}

	public void clickOnReports() {
		boolean isClicked;
		logger.debug("Clicking on reports icon in side nav.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(REPORT_LOCATOR)) {
			//userActions.getElementInViewport(REPORT_LOCATOR);
			isClicked = userActions.clickOnElement(REPORT_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on reports icon in side nav");
			}
			logger.info("Clicked on reports icon in side nav");
		} else {
			logger.error("Reports icon in side nav is not present or diabled");
			throw new RuntimeException("Reports icon in side nav is not present or diabled");
		}
	}

	public void clickOnQuotes() {
		boolean isClicked;
		logger.debug("Clicking on quotes icon in side nav.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(QUOTES_OFFER_LOCATOR)) {
			//userActions.getElementInViewport(QUOTES_OFFER_LOCATOR);
			isClicked = userActions.clickOnElement(QUOTES_OFFER_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on quotes icon in side nav");
			}
			logger.info("Clicked on quotes icon in side nav");
		} else {
			logger.error("Reports icon in side nav is not present or diabled");
			throw new RuntimeException("Reports icon in side nav is not present or diabled");
		}
	}

	public void clickOnMasterFile() {
		boolean isClicked;
		logger.debug("Clicking on master file icon in side nav.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(MASTER_FILE_LOCATOR)) {
			//userActions.getElementInViewport(MASTER_FILE_LOCATOR);
			isClicked = userActions.clickOnElement(MASTER_FILE_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on master file icon in side nav");
			}
			logger.info("Clicked on master file icon in side nav");
		} else {
			logger.error("Master icon in side nav is not present or diabled");
			throw new RuntimeException("Master icon in side nav is not present or diabled");
		}
	}

	public void clickOnSettings() {
		boolean isClicked;
		logger.debug("Clicking on settings icon in side nav.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(SETTINGS_LOCATOR)) {
			//userActions.getElementInViewport(MASTER_FILE_LOCATOR);
			isClicked = userActions.clickOnElement(SETTINGS_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on settings icon in side nav");
			}
			logger.info("Clicked on settings icon in side nav");
		} else {
			logger.error("Settings icon in side nav is not present or diabled");
			throw new RuntimeException("Settings icon in side nav is not present or diabled");
		}
	}
}
