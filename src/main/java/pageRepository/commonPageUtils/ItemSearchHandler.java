package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class ItemSearchHandler {
	private static final Logger logger = LogManager.getLogger(ItemSearchHandler.class);
	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;
	
	public ItemSearchHandler(WebDriver driver) {
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}
	
	public static final By ITEM_ID_ENTRY_TEXT_BOX_LOCATOR = 
			By.xpath("//label[contains(text(),'Item Id')]/following-sibling::input");
	public static final By ITEM_DESCRIPTION_ENTRY_TEXT_BOX_LOCATOR = 
			By.xpath("//label[contains(text(),'Item Description')]/following-sibling::input");
	public static final By ITEM_SEARCH_BTN_LOCATOR = 
			By.xpath("//div/h4[contains(text(),'Item Search')]/ancestor::div/following-sibling::div/div[3]/button");
	public static final By CANCEL_BTN_LOCATOR = 
			By.xpath("//div/h4[contains(text(),'Item Search')]/ancestor::div/following-sibling::div[3]/button");
	public static final By LOADING_SPINNER = 
			By.className("ui-progress-spinner-circle");
	
	
	public void enterItemID(String itemID) {
		boolean isTextEntered;
		logger.debug("Entering item id: " + itemID);
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ITEM_ID_ENTRY_TEXT_BOX_LOCATOR)) {
			userActions.getElementInViewport(ITEM_ID_ENTRY_TEXT_BOX_LOCATOR);
			isTextEntered=userActions.enterTextInTextBox(ITEM_ID_ENTRY_TEXT_BOX_LOCATOR, itemID);
			if (!isTextEntered) {
				throw new RuntimeException("Not able to enter item id: " + itemID);
			}
			logger.info("Entered item id: " + itemID);
		} else {
			logger.error(" item id entry text box is not present or diabled");
			throw new RuntimeException("");
		}
	}
	
	public void enterItemDescription(String itemDescription) {
		boolean isTextEntered;
		logger.debug("Entering item discription: " + itemDescription);
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ITEM_DESCRIPTION_ENTRY_TEXT_BOX_LOCATOR)) {
			userActions.getElementInViewport(ITEM_DESCRIPTION_ENTRY_TEXT_BOX_LOCATOR);
			isTextEntered=userActions.enterTextInTextBox(ITEM_DESCRIPTION_ENTRY_TEXT_BOX_LOCATOR, itemDescription);
			if (!isTextEntered) {
				throw new RuntimeException("Not able to enter item discription: " + itemDescription);
			}
			logger.info("Entered item discription: " + itemDescription);
		} else {
			logger.error("Item discription entry text box is not present or diabled");
			throw new RuntimeException("Item discription entry text box is not present or diabled");
		}
	}
	
	public void clickOnSearch() {
		boolean isClicked;
		logger.debug("Clicking on search button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ITEM_SEARCH_BTN_LOCATOR)) {
			userActions.getElementInViewport(ITEM_SEARCH_BTN_LOCATOR);
			isClicked = userActions.clickOnElement(ITEM_SEARCH_BTN_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on search button");
			}
			logger.info("Clicked on search button");
		} else {
			logger.error("Search button is not present or diabled");
			throw new RuntimeException("Search button is not present or diabled");
		}
		waits.sleep();
	}
	
	//span[contains(text(),'ADHESIVES')]/ancestor::span/ancestor::td/preceding-sibling::td/span/label/input
	public void selectSearchResult(String itemID) {
		boolean isClicked;
		String xpath = "//span[contains(text(),'"+itemID+"')]/ancestor::span/ancestor::td/preceding-sibling::td/span/label/input";
		logger.debug("Selecting item id: "+itemID+".....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(By.xpath(xpath))) {
			userActions.getElementInViewport(By.xpath(xpath));
			isClicked = userActions.clickOnElement(By.xpath(xpath));
			if (!isClicked) {
				throw new RuntimeException("Not able to click on search result radio button");
			}
			logger.info("Selected item id: "+itemID);
		} else {
			logger.error("Item ID: "+itemID+" is not present or diabled");
			throw new RuntimeException("Item ID: "+itemID+" is not present or diabled");
		}
	}
	
	public void clickOnCancel() {
		boolean isClicked;
		logger.debug("Clicking on cancel button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CANCEL_BTN_LOCATOR)) {
			userActions.getElementInViewport(CANCEL_BTN_LOCATOR);
			isClicked = userActions.clickOnElement(CANCEL_BTN_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on cancel button");
			}
			logger.info("Clicked on cancel button");
		} else {
			logger.error(" cancel button is not present or diabled");
			throw new RuntimeException("cancel button is not present or diabled");
		}
	}
}
