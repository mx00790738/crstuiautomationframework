package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import enumRepository.CommentType;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class CommentSectionHandler {
	private static final Logger logger = LogManager.getLogger(CommentSectionHandler.class);

	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;

	public CommentSectionHandler(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}

	public static final By COMMENTS_BTN_LOCATOR = By.id("");
	public static final By COMMENTS_TYPE_DROPDOWN_LOCATOR = By
			.xpath("//div[@class='modal fade in']/div/div[2]/div/div/lib-comment-section/div/select");
	public static final By COMMENTS_DISCRIPTION_ENTRY_TXTAREA_LOCATOR = By
			.xpath("//div[@class='modal fade in']/div/div[2]/div/div/lib-comment-section/div[2]/textarea");
	public static final By COMMENTS_ADD_BTN_LOCATOR = By
			.xpath("//div[@class='modal fade in']/div/div[2]/div/div/lib-comment-section/div[4]/button[1]");
	public static final By COMMENTS_UPDATE_BTN_LOCATOR = By
			.xpath("//div[@class='modal fade in']/div/div[2]/div/div/lib-comment-section/div[4]/button[2]");
	public static final By COMMENTS_CANCEL_BTN_LOCATOR = By
			.xpath("//div[@class='modal fade in']/div/div[2]/div/div/lib-comment-section/div[4]/button[3]");
	public static final By COMMENTS_EDIT_BTN_LOCATOR = By.xpath("");
	public static final By COMMENTS_DELETE_BTN_LOCATOR = By.xpath("");
	public static final By COMMENTS_CLOSE_BTN_LOCATOR = By.xpath("//div[@class='modal fade in']/div/div/button");

	public void selectCommentType(CommentType commentType) {
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(COMMENTS_TYPE_DROPDOWN_LOCATOR)) {
			WebElement commentTypeSelectDropDown = webElementsUtils.getElement(COMMENTS_TYPE_DROPDOWN_LOCATOR);
			Select se = new Select(commentTypeSelectDropDown);
			switch (commentType) {
			case HOT:
				se.selectByValue("HC");
				break;
			case DISPATCH:
				se.selectByValue("DC");
				break;
			case BILLING:
				se.selectByValue("BC");
				break;
			case OTHERS:
				se.selectByValue("OC");
				break;
			}
		}
	}

	public void enterCommentDiscription(String CommentDisc) {
		boolean isTextEntered;
		logger.debug("Entering comment description: " + CommentDisc);
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(COMMENTS_DISCRIPTION_ENTRY_TXTAREA_LOCATOR)) {
			userActions.getElementInViewport(COMMENTS_DISCRIPTION_ENTRY_TXTAREA_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(COMMENTS_DISCRIPTION_ENTRY_TXTAREA_LOCATOR, CommentDisc);
			if (!isTextEntered) {
				throw new RuntimeException("Not able enter comment description: "+CommentDisc);
			}
			logger.info("Entered comment description: " + CommentDisc);
		} else {
			logger.error("comment description text box is not present or diabled");
			throw new RuntimeException("comment description text box is not present or diabled");
		}
	}

	public void clickCommentAdd() {
		boolean isClicked;
		logger.debug("Clicking on add comment button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(COMMENTS_ADD_BTN_LOCATOR)) {
			userActions.getElementInViewport(COMMENTS_ADD_BTN_LOCATOR);
			isClicked = userActions.clickOnElement(COMMENTS_ADD_BTN_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on add comment button");
			}
			logger.info("Clicked on add comment button");
		} else {
			logger.error("add comment button is not present or diabled");
			throw new RuntimeException("add comment button is not present or diabled");
		}
	}

	public void clickCommentUpdate() {
		boolean isClicked;
		logger.debug("Clicking on comment update button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(COMMENTS_UPDATE_BTN_LOCATOR)) {
			userActions.getElementInViewport(COMMENTS_UPDATE_BTN_LOCATOR);
			isClicked = userActions.clickOnElement(COMMENTS_UPDATE_BTN_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on comment update button");
			}
			logger.info("Clicked on comment update button");
		} else {
			logger.error("comment update button is not present or diabled");
			throw new RuntimeException("comment update button is not present or diabled");
		}
	}

	public void clickCancel() {
		boolean isClicked;
		logger.debug("Clicking on cancel button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(COMMENTS_CANCEL_BTN_LOCATOR)) {
			userActions.getElementInViewport(COMMENTS_CANCEL_BTN_LOCATOR);
			isClicked = userActions.clickOnElement(COMMENTS_CANCEL_BTN_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on cancel button");
			}
			logger.info("Clicked on cancel button");
		} else {
			logger.error("Cancel button is not present or diabled");
			throw new RuntimeException("Cancel button is not present or diabled");
		}
	}

	public void closePopUp() {
		boolean isClicked;
		logger.debug("Clicking on close(X) popup button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(COMMENTS_CLOSE_BTN_LOCATOR)) {
			userActions.getElementInViewport(COMMENTS_CLOSE_BTN_LOCATOR);
			isClicked = userActions.clickOnElement(COMMENTS_CLOSE_BTN_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on close(X) popup button");
			}
			logger.info("Clicked on close(X) popup button");
		} else {
			logger.error("Close(X) popup button is not present or diabled");
			throw new RuntimeException("Close(X) popup button is not present or diabled");
		}
	}

	public void editComment(String commentType, String commentDisc) {
	}

	public void getListOfComments() {
	}

	public void deleteComment(String commentType, String commentDisc) {
	}

	public void clickOnEdit() {
	}

	public void clickOnDelete() {
	}
}
