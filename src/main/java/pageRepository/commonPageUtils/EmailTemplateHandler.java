package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageRepository.CarrierRatingPage;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class EmailTemplateHandler {
	private static final Logger logger = LogManager.getLogger(CarrierRatingPage.class);

	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;

	public EmailTemplateHandler(WebDriver driver) {
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}

	public static final By SEND_CONFIRMATION_BTN_LOCATOR = By.id("sendConfirmBtn");

	public void clickOnSendConfirmation() {
		boolean isClicked;
		logger.debug("Clicking on send confirmation.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(SEND_CONFIRMATION_BTN_LOCATOR)) {
			userActions.getElementInViewport(SEND_CONFIRMATION_BTN_LOCATOR);
			isClicked = userActions.clickOnElement(SEND_CONFIRMATION_BTN_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on send confirmation button");
			}
			logger.info("Clicked on send confirmation");
		} else {
			logger.error(" Send confirmation button is not present or diabled");
		}
	}

	public void clickOnGoBack() {

	}

	public void clickOnClose() {

	}

	public void addEmailToField(String address) {

	}

	public void addEmailCCField(String address) {

	}

	public void clickOnAttachements() {

	}

}
