package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageRepository.CreateOrderPage;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class CarrierContactSearchHandler {
	private static final Logger logger = LogManager.getLogger(CarrierContactSearchHandler.class);
	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;

	public CarrierContactSearchHandler(WebDriver driver) {
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}

	public void selectContactEmail(String contactName) {
		boolean isClicked;
		String xpath = "//span[contains(text(), '" + contactName
				+ "')]/ancestor::td/preceding-sibling::td/span/label/span";
		By contactEmail = By.xpath(xpath);
		logger.debug("Selecting contact name: " + contactName + " in carrier contact search popup");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(contactEmail)) {
			userActions.getElementInViewport(contactEmail);
			isClicked = userActions.clickOnElement(contactEmail);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on contact name selection radio button");
			}
			logger.info("Selected contact name: " + contactName + " in carrier contact search popup");
		} else {
			logger.error(" Contact email for conatct name \"" + contactName
					+ "\" not present or diabled in carrier contact search popup");
			throw new RuntimeException("Not able to select contact email for contact name: " + contactName);
		}

	}
}
