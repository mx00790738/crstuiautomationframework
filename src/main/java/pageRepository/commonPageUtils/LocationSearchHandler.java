package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class LocationSearchHandler {
	private static final Logger logger = LogManager.getLogger(LocationSearchHandler.class);

	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;

	public LocationSearchHandler(WebDriver driver) {
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}

	public static final By LOC_CODE_ENTRY_TXT_BOX = By.xpath(
			"//div[@class='modal fade in']/div/div/div[3]/div/div/div/label[contains(text(),'Location Code')]/following-sibling::input");
	public static final By SEARCH_BTN = By.xpath("//div[@class='modal fade in']/div/div/div[3]/div/div/div[6]/button");
	public static final By FIRST_RESULT_SELECTOR_BTN_LOCATOR = By.xpath(
			"/html/body/lib-location-search-pop-up[2]/div/div/div/div[3]/div/p-datatable/div/div[1]/table/tbody/tr/td[1]/span/label/input");

	public void enterLocationCode(String locCode) {
		boolean isTextEntered;
		logger.debug("Entering location code: " + locCode + ".....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(LOC_CODE_ENTRY_TXT_BOX)) {
			userActions.getElementInViewport(LOC_CODE_ENTRY_TXT_BOX);
			isTextEntered = userActions.enterTextInTextBox(LOC_CODE_ENTRY_TXT_BOX, locCode);
			if (!isTextEntered) {
				throw new RuntimeException("Not able to enter location code");
			}
			logger.info("Entered location code: " + locCode);
		} else {
			logger.error("Location code entry text box is not present or diabled");
			throw new RuntimeException("Location code entry text box is not present or diabled");
		}
	}

	public void clickLocationCodeSearchBtn() {
		boolean isClicked;
		logger.debug("Clicking on location code search button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(SEARCH_BTN)) {
			userActions.getElementInViewport(SEARCH_BTN);
			isClicked = userActions.clickOnElement(SEARCH_BTN);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on location code seaarch btn");
			}
			logger.info("Clicked on location code search button");
		} else {
			logger.error("Location code search button is not present or diabled");
			throw new RuntimeException("Location code search button is not present or diabled");
		}
	}

	public void clickOnFirstResult(String locCode) {
		boolean isClicked;
		String xpath = "//span[contains(text(),'" + locCode + "')]/ancestor::td/preceding-sibling::td/span/label/span";
		By resultSelectorRadioBtn = By.xpath(xpath);
		logger.debug("Clicking on first search result.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(resultSelectorRadioBtn)) {
			userActions.getElementInViewport(resultSelectorRadioBtn);
			isClicked = userActions.clickOnElement(resultSelectorRadioBtn);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on first search result");
			}
			logger.info("Clicked on first search result");
		} else {
			logger.error("Search result is not present");
			throw new RuntimeException("Search result is not present");
		}
	}
}
