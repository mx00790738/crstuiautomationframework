package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class CommonPopupHandler {
	private static final Logger logger = LogManager.getLogger(CommonPopupHandler.class);

	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;
	
	public static final By CONFIRM_BUTTON = By.xpath("//button/span[contains(text(),'Confirm')]");

	public CommonPopupHandler(WebDriver driver) {
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}
	
	public void clickConfirmButton() {
		boolean isClicked;
		logger.debug("Clicking on confirm button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CONFIRM_BUTTON)) {
			userActions.getElementInViewport(CONFIRM_BUTTON);
			isClicked = userActions.clickOnElement(CONFIRM_BUTTON);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on confirm button");
			}
			logger.info("Clicked confirm button");
		} else {
			logger.error("confirm button is not present or diabled");
			throw new RuntimeException("confirm button is not present or diabled");
		}
	}
}
