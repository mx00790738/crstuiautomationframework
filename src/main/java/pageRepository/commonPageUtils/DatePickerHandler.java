package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import pageRepository.CarrierRatingPage;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class DatePickerHandler {
	private static final Logger logger = LogManager.getLogger(DatePickerHandler.class);
	
	public static final By CALENDER_CONTAINER_LOCATOR = By.className("dropdown-menu show ng-star-inserted");
	public static final By CALENDER_SELECT_MONTH_LOCATOR = By.xpath("//select[@title='Select month']");
	public static final By CALENDER_SELECT_YEAR_LOCATOR = By.xpath("//select[@title='Select year']");
	
	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;

	public DatePickerHandler(WebDriver driver) {
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}

	public void selectDate(String date) {
		logger.info("Selecting date: "+date);
		String[] temp = date.split(" ");
		String xpath = "//div[@aria-label='" + temp[3] + ", " + temp[1] + " " + temp[0] + ", " + temp[2] + "']";
		WebElement selectMonthDropDown = webElementsUtils.getElement(CALENDER_SELECT_MONTH_LOCATOR);
		Select selectMonth = new Select(selectMonthDropDown);
		selectMonth.selectByVisibleText(temp[1].substring(0, 3));
		WebElement selectYearDropDown = webElementsUtils.getElement(CALENDER_SELECT_YEAR_LOCATOR);
		Select selectYear = new Select(selectYearDropDown);
		selectYear.selectByVisibleText(temp[2]);
		waits.waitImplicitly();		
		userActions.clickOnElement(By.xpath(xpath));
	}

	public void selectMonth(String date) {

	}

	public void selectYear(String date) {

	}
}
