package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class UndoPageHandler {
	private static final Logger logger = LogManager.getLogger(UndoPageHandler.class);

	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;

	public UndoPageHandler(WebDriver driver) {
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}
	
	public static final By UNDO_CARRIER_ASSIGN_LOCATOR = 
			By.xpath("//div[contains(@ptooltip,'Set the Movement back to Available and clear the Carrier.')]/i");
	
	public void clickOnUndoCarrierAssigned() {
		boolean isClicked;
		logger.debug("Clicking on undo carrier assigned.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(UNDO_CARRIER_ASSIGN_LOCATOR)) {
			userActions.getElementInViewport(UNDO_CARRIER_ASSIGN_LOCATOR);
			isClicked = userActions.clickOnElement(UNDO_CARRIER_ASSIGN_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on undo carrier assigned");
			}
			logger.info("Clicked on undo carrier assigned");
		} else {
			logger.error("undo carrier assigned icon is not present or diabled");
			throw new RuntimeException("undo carrier assigned icon is not present or diabled");
		}
	}
}
