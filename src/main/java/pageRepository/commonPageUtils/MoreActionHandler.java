package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class MoreActionHandler {
	private static final Logger logger = LogManager.getLogger(MoreActionHandler.class);

	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;

	public MoreActionHandler(WebDriver driver) {
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}
	
	public static final By MORE_ACTION_BTN_LOCATOR = 
			By.xpath("//button[contains(text(),'More Actions')]/following-sibling::span/img");
	public static final By UNDO_OPTION_LOCATOR = 
			By.xpath("//li/a/span[contains(text(),'Undo')]");
	public static final By SPLIT_OPTION_LOCATOR = 
			By.xpath("//li/a/span[contains(text(),'Split')]");
	
	public void clickOnMoreActionBtn() {
		boolean isClicked;
		logger.debug("Clicking on more action button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(MORE_ACTION_BTN_LOCATOR)) {
			userActions.getElementInViewport(MORE_ACTION_BTN_LOCATOR);
			isClicked = userActions.clickOnElement(MORE_ACTION_BTN_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on more action btn");
			}
			logger.info("Clicked on more action button");
		} else {
			logger.error("more action button is not present or diabled");
			throw new RuntimeException("more action button is not present or diabled");
		}
	}
	
	public void clickOnUndoOption() {
		boolean isClicked;
		logger.debug("Clicking on undo option.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(UNDO_OPTION_LOCATOR)) {
			userActions.getElementInViewport(UNDO_OPTION_LOCATOR);
			isClicked = userActions.clickOnElement(UNDO_OPTION_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on undo option");
			}
			logger.info("Clicked on undo option");
		} else {
			logger.error("undo option is not present or diabled");
			throw new RuntimeException("undo option is not present or diabled");
		}
	}
	
	public void clickOnSplitOption() {
		boolean isClicked;
		logger.debug("Clicking on undo option.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(UNDO_OPTION_LOCATOR)) {
			userActions.getElementInViewport(UNDO_OPTION_LOCATOR);
			isClicked = userActions.clickOnElement(UNDO_OPTION_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on undo option");
			}
			logger.info("Clicked on undo option");
		} else {
			logger.error("undo option is not present or diabled");
			throw new RuntimeException("undo option is not present or diabled");
		}
	}
}
