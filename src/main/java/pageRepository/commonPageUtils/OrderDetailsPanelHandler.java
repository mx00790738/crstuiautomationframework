package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageRepository.CarrierRatingPage;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class OrderDetailsPanelHandler {
	private static final Logger logger = LogManager.getLogger(OrderDetailsPanelHandler.class);
	
	public static final By ORDER_DETAILS_PANEL = 
			By.xpath("//div[contains(@class,'row order-details-panel')]");
	public static final By ORDER_ID_LOCATOR = 
			By.xpath("//*[@id=\"floatingHeader\"]/div/div/div/label[contains(text(),\"Order Id\")]/following::label");
	public static final By CUSTOMER_NAME_LOCATOR = 
			By.xpath("//*[@id=\"floatingHeader\"]/div/div/div/label[contains(text(),\"Customer Name \")]/following::label");
	public static final By ORDER_STATUS_LOCATOR = 
			By.xpath("//*[@id=\"floatingHeader\"]/div/div/div/label[contains(text(),\"Order Status\")]/following::label");
	public static final By MOVE_ID_LOCATOR = 
			By.xpath("//*[@id=\"floatingHeader\"]/div/div/div/label[contains(text(),\"Move Id \")]/following::label");
	public static final By MOVE_STATUS_LOCATOR = 
			By.xpath("//*[@id=\"floatingHeader\"]/div/div/div/label[contains(text(),\"Move Status \")]/following::label");
	public static final By BROKERAGE_STATUS_LOCATOR = 
			By.xpath("//*[@id=\"floatingHeader\"]/div/div/div/label[contains(text(),\"Brokerage Status \")]/following::label");
	public static final By CARRIER_ID_LOCATOR = 
			By.xpath("//*[@id=\"floatingHeader\"]/div/div/div/label[contains(text(),\"Carrier Id \")]/following::label");
	public static final By CARRIER_NAME_LOCATOR = 
			By.xpath("//*[@id=\"floatingHeader\"]/div/div/div/label[contains(text(),\"Carrier Name \")]/following::label");
	
	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;

	public OrderDetailsPanelHandler(WebDriver driver) {
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}
	
	public String getOrderID() {
		waits.sleep();
		String orderID;
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ORDER_DETAILS_PANEL)) {
			userActions.getElementInViewport(ORDER_ID_LOCATOR);
			orderID = webElementsUtils.getTextFromElement(ORDER_ID_LOCATOR);
			if (orderID == null) {
				throw new RuntimeException("Not able to get order ID");
			}
			logger.info("Order ID shown in order details panel: " + orderID);
			return orderID.trim();
		} else {
			logger.error("Order details panel is not present or diabled");
			throw new RuntimeException("Order details panel is not present or diabled");
		}
	}
	
	public String getCustomerName() {
		waits.sleep();
		String customerName;
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ORDER_DETAILS_PANEL)) {
			userActions.getElementInViewport(CUSTOMER_NAME_LOCATOR);
			customerName = webElementsUtils.getTextFromElement(CUSTOMER_NAME_LOCATOR);
			if (customerName == null) {
				throw new RuntimeException("Not able to get Customer Name");
			}
			logger.info("Customer Name shown in order details panel: " + customerName);
			return customerName.trim();
		} else {
			logger.error("Order details panel is not present or diabled");
			throw new RuntimeException("Order details panel is not present or diabled");
		}
	}
	
	public String getOrderStatus() {
		waits.sleep();
		String orderStatus;
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ORDER_STATUS_LOCATOR)) {
			userActions.getElementInViewport(ORDER_STATUS_LOCATOR);
			orderStatus = webElementsUtils.getTextFromElement(ORDER_STATUS_LOCATOR);
			if (orderStatus == null) {
				throw new RuntimeException("Not able to get order status");
			}
			logger.info("Order status shown in order details panel: " + orderStatus);
			return orderStatus.trim();
		} else {
			logger.error("Order details panel is not present or diabled");
			throw new RuntimeException("Order details panel is not present or diabled");
		}
	}
	
	public String getMoveID() {
		waits.sleep();
		String moveID;
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(MOVE_ID_LOCATOR)) {
			userActions.getElementInViewport(MOVE_ID_LOCATOR);
			moveID = webElementsUtils.getTextFromElement(MOVE_ID_LOCATOR);
			if (moveID == null) {
				throw new RuntimeException("Not able to get move ID");
			}
			logger.info("Move ID shown in order details panel: " + moveID);
			return moveID.trim();
		} else {
			logger.error("Order details panel is not present or diabled");
			throw new RuntimeException("Order details panel is not present or diabled");
		}
	}
	
	public String getMoveStatus() {
		waits.sleep();
		String moveStatus;
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(MOVE_STATUS_LOCATOR)) {
			userActions.getElementInViewport(MOVE_STATUS_LOCATOR);
			moveStatus = webElementsUtils.getTextFromElement(MOVE_STATUS_LOCATOR);
			if (moveStatus == null) {
				throw new RuntimeException("Not able to get move status");
			}
			logger.info("Move status shown in order details panel: " + moveStatus);
			return moveStatus.trim();
		} else {
			logger.error("Order details panel is not present or diabled");
			throw new RuntimeException("Order details panel is not present or diabled");
		}
	}
	
	public String getBrokerageStatus() {
		waits.sleep();
		String brokerageStatus;
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(BROKERAGE_STATUS_LOCATOR)) {
			userActions.getElementInViewport(BROKERAGE_STATUS_LOCATOR);
			brokerageStatus = webElementsUtils.getTextFromElement(BROKERAGE_STATUS_LOCATOR);
			if (brokerageStatus == null) {
				throw new RuntimeException("Not able to get brokerage status");
			}
			logger.info("Brokerage status shown in order details panel: " + brokerageStatus);
			return brokerageStatus.trim();
		} else {
			logger.error("Order details panel is not present or diabled");
			throw new RuntimeException("Order details panel is not present or diabled");
		}
	}
	
	public String getCarrierID() {
		waits.sleep();
		String carrierID;
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CARRIER_ID_LOCATOR)) {
			userActions.getElementInViewport(CARRIER_ID_LOCATOR);
			carrierID = webElementsUtils.getTextFromElement(CARRIER_ID_LOCATOR);
			if (carrierID == null) {
				throw new RuntimeException("Not able to get carrier ID");
			}
			logger.info("Carrier ID shown in order details panel: " + carrierID);
			return carrierID.trim();
		} else {
			logger.error("Order details panel is not present or diabled");
			throw new RuntimeException("Order details panel is not present or diabled");
		}
	}
	
	public String getCarrierName() {
		waits.sleep();
		String carrierName;
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CARRIER_ID_LOCATOR)) {
			userActions.getElementInViewport(CARRIER_ID_LOCATOR);
			carrierName = webElementsUtils.getTextFromElement(CARRIER_ID_LOCATOR);
			if (carrierName == null) {
				throw new RuntimeException("Not able to get carrier name");
			}
			logger.info("Carrier name shown in order details panel: " + carrierName);
			return carrierName.trim();
		} else {
			logger.error("Order details panel is not present or diabled");
			throw new RuntimeException("Order details panel is not present or diabled");
		}
	}
}
