package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageRepository.CarrierRatingPage;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class CustomerSelectionHandler {
	private static final Logger logger = LogManager.getLogger(CarrierRatingPage.class);

	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;

	public CustomerSelectionHandler(WebDriver driver) {
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}

	public static final By CUSTOMER_CODE_ENTRY_TXT_BOX_LOCATOR = By.id("custId");
	public static final By CUSTOMER_NAME_ENTRY_TXT_BOX_LOCATOR = By.id("customerName");
	public static final By CUSTOMER_ADDRESS_ENTRY_TXT_BOX_LOCATOR = By.id("locationAddress");
	public static final By CUSTOMER_CODE_FIRST_SEARCH_RESULT_SELECTION_BTN = By
			.xpath("//input[@name=\"customerRadios\"]");
	public static final By SEARCH_BTN_LOCATOR = By.id("buttonSaveComment");

	public void enterCustomerCode(String customerCode) {
		boolean isTextEntered;
		logger.debug("Entering customer code :" + customerCode + ".....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CUSTOMER_CODE_ENTRY_TXT_BOX_LOCATOR)) {
			userActions.getElementInViewport(CUSTOMER_CODE_ENTRY_TXT_BOX_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(CUSTOMER_CODE_ENTRY_TXT_BOX_LOCATOR, customerCode);
			if (!isTextEntered) {
				throw new RuntimeException("Not able to enter customer code :" + customerCode);
			}
			logger.info("Entered customer code :" + customerCode);
		} else {
			logger.error("Customer code entry text box is not present or diabled");
			throw new RuntimeException("Customer code entry text box is not present or diabled");
		}
	}

	public void enterCustomerName(String customerName) {
		boolean isTextEntered;
		logger.debug("Entering customer name :" + customerName + ".....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CUSTOMER_NAME_ENTRY_TXT_BOX_LOCATOR)) {
			userActions.getElementInViewport(CUSTOMER_NAME_ENTRY_TXT_BOX_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(CUSTOMER_NAME_ENTRY_TXT_BOX_LOCATOR, customerName);
			if (!isTextEntered) {
				throw new RuntimeException("Not able to enter customer code :" + customerName);
			}
			logger.info("Entered customer name :" + customerName);
		} else {
			logger.error("Customer name entry text box is not present or diabled");
			throw new RuntimeException("Customer name entry text box is not present or diabled");
		}
	}

	public void enterCustomerAddress(String customerAddress) {
		boolean isTextEntered;
		logger.debug("Entering customer address :" + customerAddress + ".....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CUSTOMER_ADDRESS_ENTRY_TXT_BOX_LOCATOR)) {
			userActions.getElementInViewport(CUSTOMER_ADDRESS_ENTRY_TXT_BOX_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(CUSTOMER_ADDRESS_ENTRY_TXT_BOX_LOCATOR, customerAddress);
			if (!isTextEntered) {
				throw new RuntimeException("");
			}
			logger.info("Not able to enter customer code :" + customerAddress);
		} else {
			logger.error("Customer address entry text box is not present or diabled");
			throw new RuntimeException("Customer address entry text box is not present or diabled");
		}
	}

	public void clickOnFirstResult() {
		boolean isClicked;
		logger.debug("Clicking on first search result.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CUSTOMER_CODE_FIRST_SEARCH_RESULT_SELECTION_BTN)) {
			userActions.getElementInViewport(CUSTOMER_CODE_FIRST_SEARCH_RESULT_SELECTION_BTN);
			isClicked = userActions.clickOnElement(CUSTOMER_CODE_FIRST_SEARCH_RESULT_SELECTION_BTN);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on search result radio button");
			}
			logger.info("Clicked on first search result");
		} else {
			logger.error(" No search result is available for the search criteria");
			throw new RuntimeException("No search result is available for the search criteria");
		}
	}

	public void clickOnSearch() {
		boolean isClicked;
		logger.debug("Clicking on search button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(SEARCH_BTN_LOCATOR)) {
			userActions.getElementInViewport(SEARCH_BTN_LOCATOR);
			isClicked = userActions.clickOnElement(SEARCH_BTN_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on search button");
			}
			logger.info("Clicked on search button");
		} else {
			logger.error(" Search button is not present or diabled");
			throw new RuntimeException("Search button is not present or diabled");
		}
	}

}
