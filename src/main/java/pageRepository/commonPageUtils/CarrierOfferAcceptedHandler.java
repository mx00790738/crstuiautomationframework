package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import enumRepository.CarrierOfferStatus;
import enumRepository.PayMethod;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class CarrierOfferAcceptedHandler {
	private static final Logger logger = LogManager.getLogger(CarrierOfferAcceptedHandler.class);
	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;
	
	public static final By PAY_METHOD_DROP_DOWN_LOCATOR = By.id("payMethod");
	public static final By UPDATE_AND_GOTO_ORDER_EXECUTION_LOCATOR =
			By.xpath("//*[@id=\"updateOfferPopup\"]/app-update-offer-popup/div/div/div[2]/div[3]/div[1]/button");

	public CarrierOfferAcceptedHandler(WebDriver driver) {
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}
	
	public void selectPayMethod(PayMethod payMethod) {
		logger.info("Selecting pay method status: " + payMethod.toString() + " .....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(PAY_METHOD_DROP_DOWN_LOCATOR)) {
			WebElement orderTypeSelectDropDown = webElementsUtils.getElement(PAY_METHOD_DROP_DOWN_LOCATOR);
			Select se = new Select(orderTypeSelectDropDown);
			switch (payMethod) {
			case FLAT:
				se.selectByValue("F");
				break;
			case DISTANCE:
				se.selectByValue("D");
				break;
			}
			logger.info("Selected pay method: " + payMethod.toString());
		} else {
			logger.error("pay method drop down is not displayed or disabled");
		}
	}
	
	public void clickOnUpdateAndGotoOrderExecutionButton() {
		boolean isClicked;
		logger.debug("Clicking on update offer button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(UPDATE_AND_GOTO_ORDER_EXECUTION_LOCATOR)) {
			userActions.getElementInViewport(UPDATE_AND_GOTO_ORDER_EXECUTION_LOCATOR);
			isClicked = userActions.clickOnElement(UPDATE_AND_GOTO_ORDER_EXECUTION_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on update offer button");
			}
			logger.info("Clicked update offer button");
		} else {
			logger.error("update offer button is not present or diabled");
			throw new RuntimeException("update offer button is not present or diabled");
		}
	}
}
