package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import enumRepository.DelayDescription;
import enumRepository.DelayStatus;
import pageRepository.CarrierRatingPage;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class DelayReasonHandler {
	private static final Logger logger = LogManager.getLogger(DelayReasonHandler.class);

	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;
	
	public DelayReasonHandler(WebDriver driver) {
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}
	
	public static final By DISCRIPTION_DROP_DOWN_LOCATOR = 
			By.name("standardCode");
	public static final By STATUS_DROP_DOWN_LOCATOR = 
			By.name("serviceFailureStatus");
	public static final By COMMENT_TXT_AREA_LOCATOR = 
			By.name("comment");
	public static final By SAVE_BTN_LOCATOR = 
			By.xpath("//*[@id=\"lateReasonSearchModal\"]/div/div/div[2]/div[3]/ancestor::div/following-sibling::div/button[contains(text(),'Save')]");
	public static final By CANCEL_BTN_LOCATOR = 
			By.xpath("//*[@id=\"lateReasonSearchModal\"]/div/div/div[2]/div[3]/ancestor::div/following-sibling::div/button[contains(text(),'Cancel')]");
	
	public void enterServiceFailureDetails(DelayDescription disc,DelayStatus status,String comment) {
		selectDiscription(disc);
		selectStatus(status);
		enterComment(comment);
		clickOnSave();
	}
	
	public void selectDiscription(DelayDescription disc) {
		WebElement discriptionDropDown = webElementsUtils.getElement(DISCRIPTION_DROP_DOWN_LOCATOR);
		Select se = new Select(discriptionDropDown);
		se.selectByValue(disc.getValue());
	}
	
	public void selectStatus(DelayStatus status) {
		WebElement statusDropDown = webElementsUtils.getElement(STATUS_DROP_DOWN_LOCATOR);
		Select se = new Select(statusDropDown);
		se.selectByValue(status.getValue());
	}
	
	public void enterComment(String comment) {
		boolean isTextEntered;
		logger.debug("Entering delay comment :\"" + comment + "\"....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(COMMENT_TXT_AREA_LOCATOR)) {
			userActions.getElementInViewport(COMMENT_TXT_AREA_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(COMMENT_TXT_AREA_LOCATOR, comment);
			if (!isTextEntered) {
				throw new RuntimeException("Not able to enter comment: "+comment);
			}
			logger.info("Entered delay comment :" + comment);
		} else {
			logger.error("Comment entry text box is not present or diabled");
			throw new RuntimeException("Comment entry text box is not present or diabled");
		}
	}
	
	public void clickOnSave() {
		boolean isClicked;
		logger.debug("Clicking on save button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(SAVE_BTN_LOCATOR)) {
			userActions.getElementInViewport(SAVE_BTN_LOCATOR);
			isClicked = userActions.clickOnElement(SAVE_BTN_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on save button");
			}
			logger.info("Clicked on save button");
		} else {
			logger.error(" Save button is not present or diabled");
			throw new RuntimeException("");
		}
	}
	
	public void clickOnCancel() {
		boolean isClicked;
		logger.debug("Clicking on cancel button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CANCEL_BTN_LOCATOR)) {
			userActions.getElementInViewport(CANCEL_BTN_LOCATOR);
			isClicked = userActions.clickOnElement(CANCEL_BTN_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on cancel button");
			}
			logger.info("Clicked on cancel button");
		} else {
			logger.error(" Cancel button is not present or diabled");
			throw new RuntimeException("");
		}
	}
}
