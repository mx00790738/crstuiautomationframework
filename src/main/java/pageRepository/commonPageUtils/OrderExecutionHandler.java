package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class OrderExecutionHandler {
	private static final Logger logger = LogManager.getLogger(OrderExecutionHandler.class);

	// public static By CALENDER_SELECT_YEAR_LOCATOR;
	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;

	public static final By EDIT_ORDER_ICON = By.xpath("//li[@id=\"viewLabel1\"]/div/span/img");
	public static final By EDIT_CUSTOMER_RATING_ICON = By.xpath("//li[@id=\"viewLabel2\"]/div/span/img");
	public static final By EDIT_CARRIER_RATING_ICON = By.xpath("//li[@id=\"viewLabel4\"]/div/span/img");
	public static final By EDIT_CARRIER_CONFIRM_ICON = By.xpath("//li[@id=\"viewLabel5\"]/div/span/img");
	public static final By EDIT_PICKUP_LOG_ICON = By.xpath("//li[@id=\"viewLabel7\"]/div/span/img");
	public static final By EDIT_DELIVERY_LOG_ICON = By.xpath("//li[@id=\"viewLabel8\"]/div/span/img");
	public static final By EDIT_TRACKING_ICON = By.xpath("//li[@id=\"viewLabel9\"]/div/span/img");

	public static final By ORDER_ICON = By.xpath("//li[@id=\"viewLabel1\"]/div[2]/div");
	public static final By CUSTOMER_RATING_ICON = By.xpath("//li[@id=\"viewLabel2\"]/div[2]/div");
	public static final By CARRIER_RATING_ICON = By.xpath("//li[@id=\"viewLabel4\"]/div[2]/div");
	public static final By CARRIER_CONFIRM_ICON = By.xpath("//li[@id=\"viewLabel5\"]/div[2]/div");
	public static final By PICKUP_LOG_ICON = By.xpath("//li[@id=\"viewLabel7\"]/div[2]/div");
	public static final By DELIVERY_LOG_ICON = By.xpath("//li[@id=\"viewLabel8\"]/div[2]/div");
	public static final By TRACKING_ICON = By.xpath("//li[@id=\"viewLabel9\"]/div[2]/div");
	
	public static final By CARRIER_OFFER_ICON = By.xpath("//li[@id=\"viewLabel4\"]/div[2]/span/i");

	public OrderExecutionHandler(WebDriver driver) {
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}

	public void clickEditOrder() {
		boolean isClicked;
		logger.debug("Clicking on edit order button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(EDIT_ORDER_ICON)) {
			userActions.getElementInViewport(EDIT_ORDER_ICON);
			isClicked = userActions.clickOnElement(EDIT_ORDER_ICON);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on edit order button");
			}
			logger.info("Clicked edit order button");
		} else {
			logger.error("Edit order button is not present or diabled");
			throw new RuntimeException("Edit order button is not present or diabled");
		}
	}

	public void clickEditCustomerRating() {
		boolean isClicked;
		logger.debug("Clicking on edit customer rating button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(EDIT_CUSTOMER_RATING_ICON)) {
			userActions.getElementInViewport(EDIT_CUSTOMER_RATING_ICON);
			isClicked = userActions.clickOnElement(EDIT_CUSTOMER_RATING_ICON);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on edit customer rating button");
			}
			logger.info("Clicked edit customer rating button");
		} else {
			logger.error("Edit customer rating button is not present or diabled");
			throw new RuntimeException("Edit customer rating button is not present or diabled");
		}
	}

	public void clickEditCarrierRating() {
		boolean isClicked;
		logger.debug("Clicking on edit carrier rating button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(EDIT_CARRIER_RATING_ICON)) {
			userActions.getElementInViewport(EDIT_CARRIER_RATING_ICON);
			isClicked = userActions.clickOnElement(EDIT_CARRIER_RATING_ICON);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on edit carrier rating button");
			}
			logger.info("Clicked edit carrier rating button");
		} else {
			logger.error("Edit carrier rating button is not present or diabled");
			throw new RuntimeException("Edit carrier rating button is not present or diabled");
		}
	}

	public void clickEditPickUpLog() {
		boolean isClicked;
		logger.debug("Clicking on edit pick up log button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(EDIT_PICKUP_LOG_ICON)) {
			userActions.getElementInViewport(EDIT_PICKUP_LOG_ICON);
			isClicked = userActions.clickOnElement(EDIT_PICKUP_LOG_ICON);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on edit pick up log button");
			}
			logger.info("Clicked edit pick up log button");
		} else {
			logger.error("Edit pick up log button is not present or diabled");
			throw new RuntimeException("Edit pick up log button is not present or diabled");
		}
	}

	public void clickEditDeliveryLog() {
		boolean isClicked;
		logger.debug("Clicking on edit delivery log button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(EDIT_DELIVERY_LOG_ICON)) {
			userActions.getElementInViewport(EDIT_DELIVERY_LOG_ICON);
			isClicked = userActions.clickOnElement(EDIT_DELIVERY_LOG_ICON);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on edit delivery log button");
			}
			logger.info("Clicked edit delivery log button");
		} else {
			logger.error("Edit delivery log button is not present or diabled");
			throw new RuntimeException("Edit delivery log button is not present or diabled");
		}
	}

	public void clickEditTracking() {
		boolean isClicked;
		logger.debug("Clicking on edit tracking button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(EDIT_TRACKING_ICON)) {
			userActions.getElementInViewport(EDIT_TRACKING_ICON);
			isClicked = userActions.clickOnElement(EDIT_TRACKING_ICON);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on edit tracking button");
			}
			logger.info("Clicked edit tracking button");
		} else {
			logger.error("Edit tracking button is not present or diabled");
			throw new RuntimeException("Edit tracking button is not present or diabled");
		}
	}

	public void clickPickUpLog() {
		boolean isClicked;
		logger.debug("Clicking on pick up log button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(PICKUP_LOG_ICON)) {
			userActions.getElementInViewport(PICKUP_LOG_ICON);
			isClicked = userActions.clickOnElement(PICKUP_LOG_ICON);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on pick up log button");
			}
			logger.info("Clicked pick up log button");
		} else {
			logger.error("Pick up log button is not present or diabled");
			throw new RuntimeException("Pick up log button is not present or diabled");
		}
	}

	public void clickDeliveryLog() {
		boolean isClicked;
		logger.debug("Clicking on delivery log button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(DELIVERY_LOG_ICON)) {
			userActions.getElementInViewport(DELIVERY_LOG_ICON);
			isClicked = userActions.clickOnElement(DELIVERY_LOG_ICON);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on delivery log button");
			}
			logger.info("Clicked delivery log button");
		} else {
			logger.error("Delivery log button is not present or diabled");
			throw new RuntimeException("Delivery log button is not present or diabled");
		}
	}
	
	public void clickCarrierOfferIcon() {
		boolean isClicked;
		logger.debug("Clicking on carrier offer icon.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CARRIER_OFFER_ICON)) {
			userActions.getElementInViewport(CARRIER_OFFER_ICON);
			isClicked = userActions.clickOnElement(CARRIER_OFFER_ICON);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on carrier offer icon");
			}
			logger.info("Clicked on carrier offer icon");
		} else {
			logger.error("carrier offer icon is not present or diabled");
			throw new RuntimeException("carrier offer icon is not present or diabled");
		}
	}
}
