package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import pageRepository.CreateOrderPage;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class CommoditySelectionHandler {
	private static final Logger logger = LogManager.getLogger(CommoditySelectionHandler.class);

	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;
	ItemSearchHandler itemSearchPopupHandler;

	public CommoditySelectionHandler(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
		itemSearchPopupHandler = new ItemSearchHandler(driver);
	}

	public static final By TOTAL_WEIGHT_ENTRY_LOCATOR = By.id("totalWeight");
	public static final By TOTAL_CASES_PIEACES_ENTRY_LOCATOR = By.id("totalPieces");
	public static final By COMMODITY_ENTER_TOTAL_RADIO_BTN_LOCATOR = By.id("radioT");
	public static final By COMMODITY_ENTER_ITEM_DETAILS_RADIO_BTN_LOCATOR = By.id("radioI");
	public static final By ITEM_ID_SEARCH_BTN_LOCATOR = By.xpath("//button[@id=\"itemSearchButton\"]/img");
	public static final By FREIGHT_CLASS_SELECTION_DROPDOWN = By.id("freightClass");
	public static final By WEIGHT_ENTRY_LOCATOR = By.id("actualWeight");
	public static final By QUANTITY_ENTRY_LOCATOR = By.id("actualQuantity");
	public static final By DESCRIPTION_ENTRY_LOCATOR = By.id("descriptionconsignment0");
	public static final By ADD_CONGIGNMENT_BTN_LOCATOR = By.id("buttonSave");

	public void enterCommodityDetails(String weight, String count) {
		enterTotalWeight(weight);
		enterCasesPieaces(count);
	}

	public void enterTotalWeight(String weight) {
		boolean isTextEntered;
		logger.debug("Entering total weight: " + weight);
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(TOTAL_WEIGHT_ENTRY_LOCATOR)) {
			userActions.getElementInViewport(TOTAL_WEIGHT_ENTRY_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(TOTAL_WEIGHT_ENTRY_LOCATOR, weight);
			if (!isTextEntered) {
				throw new RuntimeException("Total weight:" + weight + " not entered in weight entry text box");
			}
			logger.info("Entered total weight: " + weight);
		} else {
			logger.error("Enter total weight text box is not present or diabled");
			throw new RuntimeException("Enter total weight text box is not present or diabled");
		}

	}

	public void enterCasesPieaces(String count) {
		boolean isTextEntered;
		logger.debug("Entering Cases/Pieaces: " + count);
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(TOTAL_CASES_PIEACES_ENTRY_LOCATOR)) {
			userActions.getElementInViewport(TOTAL_CASES_PIEACES_ENTRY_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(TOTAL_CASES_PIEACES_ENTRY_LOCATOR, count);
			if (!isTextEntered) {
				throw new RuntimeException("Cases/Pieaces count:" + count + " not entered in Cases/Pieaces text box");
			}
			logger.info("Entered Cases/Pieaces: " + count);
		} else {
			logger.error(" Enter Cases/Pieaces text box is not present or diabled");
			throw new RuntimeException("Enter Cases/Pieaces text box is not present or diabled");
		}
	}

	public void enterCommodityDetails(String itemID, String itemDesc, String freightClass, String weight,
			String quantity) {
		clickItemIDSearch();
		waits.waitImplicitly();
		selectItemID(itemID);
		enterDescription(itemDesc);
		selectFreightClass(freightClass);
		enterWeight(weight);
		enterQuantity(quantity);
		clickAddConsignment();
	}

	public void clickItemIDSearch() {
		boolean isClicked;
		logger.debug("Clicking on ItemID Search button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ITEM_ID_SEARCH_BTN_LOCATOR)) {
			userActions.getElementInViewport(ITEM_ID_SEARCH_BTN_LOCATOR);
			isClicked = userActions.clickOnElement(ITEM_ID_SEARCH_BTN_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click item id search button");
			}
			logger.info("Clicked ItemID Search button");
		} else {
			logger.error(" ItemID Search button is not present or diabled");
			throw new RuntimeException("ItemID Search button is not present or diabled");
		}
	}

	public void selectItemID(String itemID) {
		logger.info("Selecting item id: " + itemID);
		itemSearchPopupHandler.enterItemID(itemID);
		itemSearchPopupHandler.clickOnSearch();
		itemSearchPopupHandler.selectSearchResult(itemID);
	}

	public void selectFreightClass(String freightClass) {
		logger.info("Selecting freight class: " + freightClass);
		WebElement freightClassDropDown = webElementsUtils.getElement(FREIGHT_CLASS_SELECTION_DROPDOWN);
		Select se = new Select(freightClassDropDown);
		se.selectByValue(freightClass);
	}

	public void enterWeight(String weight) {
		boolean isTextEntered;
		logger.debug("Entering Weight: " + weight);
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(WEIGHT_ENTRY_LOCATOR)) {
			userActions.getElementInViewport(WEIGHT_ENTRY_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(WEIGHT_ENTRY_LOCATOR, weight);
			if (!isTextEntered) {
				throw new RuntimeException("weight:" + weight + " not entered in weight entry text box");
			}
			logger.info("Entered Weight: " + weight);
		} else {
			logger.error("Weight entry text box is not present or diabled");
			throw new RuntimeException("Weight entry text box is not present or diabled");
		}
	}

	public void enterDescription(String desc) {
		boolean isTextEntered;
		logger.debug("Entering Description: " + desc);
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(DESCRIPTION_ENTRY_LOCATOR)) {
			userActions.getElementInViewport(DESCRIPTION_ENTRY_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(DESCRIPTION_ENTRY_LOCATOR, desc);
			if (!isTextEntered) {
				throw new RuntimeException(
						"Discription:" + desc + " not entered in commodity description entry txt box");
			}
			logger.info("Entered Description: " + desc);
		} else {
			logger.error(" Description entry text box is not present or diabled");
			throw new RuntimeException("Description entry text box is not present or diabled");
		}
	}

	public void enterQuantity(String quantity) {
		boolean isTextEntered;
		logger.debug("Entering Quantity: " + quantity);
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(QUANTITY_ENTRY_LOCATOR)) {
			userActions.getElementInViewport(QUANTITY_ENTRY_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(QUANTITY_ENTRY_LOCATOR, quantity);
			if (!isTextEntered) {
				throw new RuntimeException("Quantity:" + quantity + " not entered in quantity entry txt box");
			}
			logger.info("Entered Quantity: " + quantity);
		} else {
			logger.error("Quantity entry text box is not present or diabled");
			throw new RuntimeException("Quantity entry text box is not present or diabled");
		}
	}

	public void clickAddConsignment() {
		boolean isClicked;
		logger.debug("Clicking on Add Consignment button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ADD_CONGIGNMENT_BTN_LOCATOR)) {
			userActions.getElementInViewport(ADD_CONGIGNMENT_BTN_LOCATOR);
			isClicked = userActions.clickOnElement(ADD_CONGIGNMENT_BTN_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click Add Consignment button");
			}
			logger.info("Clicked Add Consignment button");
		} else {
			logger.error("Add Consignment button is not present or diabled");
			throw new RuntimeException("Add Consignment button is not present or diabled");
		}
	}

	public void clickEnterTotal() {
		boolean isClicked;
		logger.debug("Clicking on EnterTotal radio button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(COMMODITY_ENTER_TOTAL_RADIO_BTN_LOCATOR)) {
			userActions.getElementInViewport(COMMODITY_ENTER_TOTAL_RADIO_BTN_LOCATOR);
			isClicked = userActions.clickOnElement(COMMODITY_ENTER_TOTAL_RADIO_BTN_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click EnterTotal radio button");
			}
			logger.info("Clicked EnterTotal radio button");
		} else {
			logger.error("EnterTotal radio button is not present or diabled");
			throw new RuntimeException("EnterTotal radio button is not present or diabled");
		}
	}

	public void clickEnterItemDetails() {
		boolean isClicked;
		logger.debug("Clicking on EnterItemDetails radio button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(COMMODITY_ENTER_ITEM_DETAILS_RADIO_BTN_LOCATOR)) {
			userActions.getElementInViewport(COMMODITY_ENTER_ITEM_DETAILS_RADIO_BTN_LOCATOR);
			isClicked = userActions.clickOnElement(COMMODITY_ENTER_ITEM_DETAILS_RADIO_BTN_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click EnterItemDetails radio button");
			}
			logger.info("Clicked EnterItemDetails radio button");
		} else {
			logger.error(" EnterItemDetails radio button is not present or diabled");
			throw new RuntimeException("EnterItemDetails radio button is not present or diabled");
		}
	}
}
