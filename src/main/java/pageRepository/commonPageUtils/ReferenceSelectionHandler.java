package pageRepository.commonPageUtils;

import org.openqa.selenium.WebDriver;

import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class ReferenceSelectionHandler {
	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;

	public ReferenceSelectionHandler(WebDriver driver) {
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}
}
