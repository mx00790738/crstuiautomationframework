package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageRepository.CustomerRatingPage;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class CarrierSearchHandler {
	private static final Logger logger = LogManager.getLogger(CarrierSearchHandler.class);

	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;

	public CarrierSearchHandler(WebDriver driver) {
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}

	public static final By CARRIER_ID_ENTRY_TXT_BOX_LOCATOR = By.xpath("//div[@class='modal fade in']/div[1]/div[1]/div[2]/div/div[1]/input");
	public static final By SEARCH_BTN = By.xpath(
			"//div[@class='modal fade in']/div[1]/div[1]/div[2]/div/div[12]/button");
	public static final By FIRST_SEARCH_RESULT_SELECTION_BTN = By.xpath(
			"//span[contains(@class,'label green ng-star-inserted')]/ancestor::span/ancestor::td/preceding-sibling::td/span/span/label/input/following-sibling::span");

	public void enterCarrierID(String carrierID) {
		boolean isTextEntered;
		logger.debug("Entering carrier ID.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CARRIER_ID_ENTRY_TXT_BOX_LOCATOR)) {
			userActions.getElementInViewport(CARRIER_ID_ENTRY_TXT_BOX_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(CARRIER_ID_ENTRY_TXT_BOX_LOCATOR, carrierID);
			// not sure why i added below code need to check--START
			userActions.clickOnElement(CARRIER_ID_ENTRY_TXT_BOX_LOCATOR);
			// END
			if (!isTextEntered) {
				throw new RuntimeException("Not able enter carrier ID:" + carrierID + " in carrier id entry text box");
			}
			logger.info("Entered carrier ID: " + carrierID);
		} else {
			logger.error("Carrier ID entry text box is not present or diabled");
			throw new RuntimeException("Carrier ID entry text box is not present or diabled");
		}
	}

	public void clickOnSearch() {
		boolean isClicked;
		logger.debug("Clicking on carrier search popup search button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(SEARCH_BTN)) {
			userActions.getElementInViewport(SEARCH_BTN);
			isClicked = userActions.clickOnElement(SEARCH_BTN);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on search button");
			}
			logger.info("Clicked on carrier search popup search button");
		} else {
			logger.error("Carrier search popup search button button is not present or diabled");
			throw new RuntimeException("Carrier search popup search button button is not present or diabled");
		}
	}

	public void clickOnFirstQualifiedResult() {
		boolean isClicked;
		logger.debug("Clicking on carrier search popup first qualified result.....");
		// need to change the element in isdiaplayedandenabled
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(FIRST_SEARCH_RESULT_SELECTION_BTN)) {
			userActions.getElementInViewport(FIRST_SEARCH_RESULT_SELECTION_BTN);
			isClicked = userActions.clickOnElement(FIRST_SEARCH_RESULT_SELECTION_BTN);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on search button");
			}
			logger.info("Clicked on carrier search popup first qualified result");
		} else {
			logger.error("No qualified carrier result for carrier search");
			throw new RuntimeException("No qualified carrier result found for carrier search");
		}
	}
}
