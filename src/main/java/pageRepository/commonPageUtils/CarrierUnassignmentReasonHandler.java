package pageRepository.commonPageUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import enumRepository.CarrierUnassignmentReason;
import enumRepository.PayMethod;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class CarrierUnassignmentReasonHandler {
	private static final Logger logger = LogManager.getLogger(CarrierUnassignmentReasonHandler.class);

	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;

	public CarrierUnassignmentReasonHandler(WebDriver driver) {
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}
	
	public static final By REASON_DROPDOWN_LOCATOR = 
			By.id("carrierReasonCode");
	public static final By COMMNET_TXT_BOX_LOCATOR = 
			By.id("carrieUndoComment");
	public static final By SUBMIT_BUTTON_LOCATOR = 
			By.xpath("//*[@id=\"carrierUndoPopup\"]/div/div/div[2]/div/div[4]/button");
	
	public void selectCarrierUnassignmentReason(CarrierUnassignmentReason reason) {
		logger.info("Selecting carrier unassignment reason: " + reason.toString() + " .....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(REASON_DROPDOWN_LOCATOR)) {
			WebElement orderTypeSelectDropDown = webElementsUtils.getElement(REASON_DROPDOWN_LOCATOR);
			Select se = new Select(orderTypeSelectDropDown);
			switch (reason) {
			case AHH_CARRIER_RELATED:
				se.selectByValue("AHH");
				break;
			case AJ_CARRIER_RELATED:
				se.selectByValue("AJ");
				break;
			}
			logger.info("Selected carrier unassignment reason: " + reason.toString());
		} else {
			logger.error("carrier unassignment reason drop down is not displayed or disabled");
		}
	}
	
	public void enterCarrierUnassignmentReasonComment(String comments) {
		boolean isTextEntered;
		logger.debug("Entering counter offer.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(COMMNET_TXT_BOX_LOCATOR)) {
			userActions.getElementInViewport(COMMNET_TXT_BOX_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(COMMNET_TXT_BOX_LOCATOR, comments);
			if (!isTextEntered) {
				throw new RuntimeException("Not able enter cooment:" + comments + "in carrier unassignment reason comment txt box");
			}
			logger.info("Entered carrier unassignment reason comment: " + comments);
		} else {
			logger.error("carrier unassignment reason comment txt box is not present or diabled");
			throw new RuntimeException("carrier unassignment reason comment txt box is not present or diabled");
		}
	}
	
	public void clickSaveButton() {
		boolean isClicked;
		logger.debug("Clicking on save button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(SUBMIT_BUTTON_LOCATOR)) {
			userActions.getElementInViewport(SUBMIT_BUTTON_LOCATOR);
			isClicked = userActions.clickOnElement(SUBMIT_BUTTON_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on save button");
			}
			logger.info("Clicked save button");
		} else {
			logger.error("save button is not present or diabled");
			throw new RuntimeException("save button is not present or diabled");
		}
	}
}
