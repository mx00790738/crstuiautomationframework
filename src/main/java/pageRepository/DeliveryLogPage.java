package pageRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import basePage.BasePage;
import enumRepository.DelayDescription;
import enumRepository.DelayStatus;

public class DeliveryLogPage extends BasePage{
	private static final Logger logger = LogManager.getLogger(DeliveryLogPage.class);
	
	public DeliveryLogPage(WebDriver driver) {
		super(driver);
	}
	
	public static final By DELIVERY_LOG_PAGE_LOCATOR = By
			.xpath("//label[contains(text(),'Delivery Date and Time')]");
	public static final By ACTUAL_TIME_IN_DATE_PICKER_LOCATOR = 
			By.xpath("//input[@id=\"dateactualArrivals1\"]/following-sibling::button");
	public static final By ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR = 
			By.xpath("//*[@id=\"dateactualDepartures1\"]/following-sibling::button");
	
	public static final By ACTUAL_TIME_IN_DATE_PICKER_LOCATOR1 = 
			By.xpath("//input[@id=\"dateactualArrivals2\"]/following-sibling::button");
	public static final By ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR1 = 
			By.xpath("//*[@id=\"dateactualDepartures2\"]/following-sibling::button");
	public static final By ACTUAL_TIME_IN_DATE_PICKER_LOCATOR2 = 
			By.xpath("//input[@id=\"dateactualArrivals3\"]/following-sibling::button");
	public static final By ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR2 = 
			By.xpath("//*[@id=\"dateactualDepartures3\"]/following-sibling::button");
	
	public static final By SERVICE_FAILURE_BTN_LOCATOR = 
			By.id("serviceFailure-1");
	public static final By NEXT_STEP_BTN_LOCATOR = 
			By.id("buttonarrivalDepartureSave");
	public static final By SAVE_BTN_LOCATOR = 
			By.id("arrivalDepartureSave");
	public static final By PAGE_TITLE_LOCATOR = By
			.xpath("//*[@id=\"floatingHeader\"]/app-breadcrumb/div/div/div/div/label");
	
	public void seletActualTimeInDate(String date) {
		logger.debug("Selecting actual time in date.....");
		clickOnActualTimeInDatePicker();
		datePickerHandler.selectDate(date);
		logger.info("Selected actual time in date");
		clickOnActualTimeInDatePicker();
		datePickerHandler.selectDate(date);
	}
	
	public void seletActualTimeOutDate(String date) {
		logger.debug("Selecting actual time out date.....");
		clickOnActualTimeOutDatePicker();
		datePickerHandler.selectDate(date);
		logger.info("Selected actual time out date");
	}
	
	public void seletActualTimeInDate1(String date) {
		logger.debug("Selecting actual time in date.....");
		clickOnActualTimeInDatePicker1();
		datePickerHandler.selectDate(date);
		logger.info("Selected actual time in date");
		clickOnActualTimeInDatePicker1();
		datePickerHandler.selectDate(date);
	}
	
	public void seletActualTimeOutDate1(String date) {
		logger.debug("Selecting actual time out date.....");
		clickOnActualTimeOutDatePicker1();
		datePickerHandler.selectDate(date);
		logger.info("Selected actual time out date");
	}
	
	public void seletActualTimeInDate2(String date) {
		logger.debug("Selecting actual time in date.....");
		clickOnActualTimeInDatePicker2();
		datePickerHandler.selectDate(date);
		logger.info("Selected actual time in date");
		clickOnActualTimeInDatePicker2();
		datePickerHandler.selectDate(date);
	}
	
	public void seletActualTimeOutDate2(String date) {
		logger.debug("Selecting actual time out date.....");
		clickOnActualTimeOutDatePicker2();
		datePickerHandler.selectDate(date);
		logger.info("Selected actual time out date");
	}
	
	public void clickOnActualTimeInDatePicker() {
		logger.debug("Clicking on Actual time in date picker button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ACTUAL_TIME_IN_DATE_PICKER_LOCATOR)) {
			userActions.getElementInViewport(ACTUAL_TIME_IN_DATE_PICKER_LOCATOR);
			userActions.clickOnElement(ACTUAL_TIME_IN_DATE_PICKER_LOCATOR);
			logger.info("Clicked Actual time in date picker");
		} else {
			logger.error("Actual time in date picker button is not present or diabled");
		}
	}
	
	public void clickOnActualTimeInDatePicker1() {
		logger.debug("Clicking on Actual time in date picker button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ACTUAL_TIME_IN_DATE_PICKER_LOCATOR1)) {
			userActions.getElementInViewport(ACTUAL_TIME_IN_DATE_PICKER_LOCATOR1);
			userActions.clickOnElement(ACTUAL_TIME_IN_DATE_PICKER_LOCATOR1);
			logger.info("Clicked Actual time in date picker");
		} else {
			logger.error("Actual time in date picker button is not present or diabled");
		}
	}
	
	public void clickOnActualTimeInDatePicker2() {
		logger.debug("Clicking on Actual time in date picker button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ACTUAL_TIME_IN_DATE_PICKER_LOCATOR2)) {
			userActions.getElementInViewport(ACTUAL_TIME_IN_DATE_PICKER_LOCATOR2);
			userActions.clickOnElement(ACTUAL_TIME_IN_DATE_PICKER_LOCATOR2);
			logger.info("Clicked Actual time in date picker");
		} else {
			logger.error("Actual time in date picker button is not present or diabled");
		}
	}
	
	public void clickOnActualTimeOutDatePicker() {
		logger.debug("Clicking on Actual time out date picker button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR)) {
			userActions.getElementInViewport(ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR);
			userActions.clickOnElement(ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR);
			logger.info("Clicked Actual time out date picker");
		} else {
			logger.error("Actual time out date picker button is not present or diabled");
		}
	}
	
	public void clickOnActualTimeOutDatePicker1() {
		logger.debug("Clicking on Actual time out date picker button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR1)) {
			userActions.getElementInViewport(ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR1);
			userActions.clickOnElement(ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR1);
			logger.info("Clicked Actual time out date picker");
		} else {
			logger.error("Actual time out date picker button is not present or diabled");
		}
	}
	
	public void clickOnActualTimeOutDatePicker2() {
		logger.debug("Clicking on Actual time out date picker button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR2)) {
			userActions.getElementInViewport(ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR2);
			userActions.clickOnElement(ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR2);
			logger.info("Clicked Actual time out date picker");
		} else {
			logger.error("Actual time out date picker button is not present or diabled");
		}
	}
	
	public void addServiceFailure(DelayDescription description,DelayStatus status,String comments) {
		logger.debug("Adding service failure with [DelayDescription: " + description.toString() + ",DelayStatus: "
				+ status.toString() + ",comments: " + comments + "].....");
		clickOnServiceFailureButton();
		delayReasonPopupHandler.enterServiceFailureDetails(description, status, comments);
		logger.info("Service failure added with [DelayDescription: " + description.toString() + ",DelayStatus: "
				+ status.toString() + ",comments: " + comments + "]");
	}
	
	public void clickOnServiceFailureButton() {
		logger.debug("Clicking on service failurer button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(SERVICE_FAILURE_BTN_LOCATOR)) {
			userActions.getElementInViewport(SERVICE_FAILURE_BTN_LOCATOR);
			userActions.clickOnElement(SERVICE_FAILURE_BTN_LOCATOR);
			logger.info("Clicked service failure button");
		} else {
			logger.error("Service failure button is not present or diabled");
		}
	}
	
	public void clickOnNextStep() {
		logger.debug("Clicking on Next Step button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(NEXT_STEP_BTN_LOCATOR)) {
			userActions.getElementInViewport(NEXT_STEP_BTN_LOCATOR);
			userActions.clickOnElement(NEXT_STEP_BTN_LOCATOR);
			logger.info("Clicked  Next Step button");
			waits.waitTillInVisibilityOfElement(LOADING_SPINNER);
		}else {
			logger.error(" Next Step button is not present or diabled");
		}
	}
	
	public void clickOnSave() {
		logger.debug("Clicking on Save button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(SAVE_BTN_LOCATOR)) {
			userActions.getElementInViewport(SAVE_BTN_LOCATOR);
			userActions.clickOnElement(SAVE_BTN_LOCATOR);
			logger.info("Clicked  Save button");
			waits.waitTillInVisibilityOfElement(LOADING_SPINNER);
		}else {
			logger.error(" Save button is not present or diabled");
		}
	}
	
	/**
	 * method to verify if user is in delivery log page
	 */
	public boolean verifyUserIsInPickUpPage() {
		String pageTitle = getPageTitle();
		Boolean isOrderCreatePage = pageTitle.equalsIgnoreCase("Delivery Log");
		logger.debug("verifying current page is Delivery Log page");
		if (isOrderCreatePage) {
			logger.info("Current page is Delivery Log page");
			return true;
		}
		return false;
	}

	/**
	 * method to verify if user is in edit delivery log
	 */
	public boolean verifyUserIsInEditPickUpPage() {
		String pageTitle = getPageTitle();
		Boolean isOrderCreatePage = pageTitle.equalsIgnoreCase("Edit Delivery Log");
		logger.debug("verifying current page is edit Delivery Log page");
		if (isOrderCreatePage) {
			logger.info("Current page is edit Delivery Log page");
			return true;
		}
		return false;
	}

	/**
	 * method to get page title
	 */
	private String getPageTitle() {
		logger.debug("Getting current page title");
		waits.waitTillVisibilityOfElement(PAGE_TITLE_LOCATOR);
		WebElement pageTitleText = webElementsUtils.getElement(PAGE_TITLE_LOCATOR);
		return pageTitleText.getText();
	}
	
	public void waitTillDeliveryLogPageLoadOut() {
		Boolean isInCreateOrderPage = waits.waitTillVisibilityOfElement(DELIVERY_LOG_PAGE_LOCATOR);
		if (!isInCreateOrderPage) {
			logger.error("DeliveryLogPage loadout failed");
			throw new RuntimeException("DeliveryLogPage loadout failed");
		}
		logger.info("DeliveryLogPage loadout successful");
	}
}
