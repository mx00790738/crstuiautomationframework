package pageRepository;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import basePage.BasePage;
import enumRepository.DelayDescription;
import enumRepository.DelayStatus;

public class PickUpLogPage extends BasePage {
	private static final Logger logger = LogManager.getLogger(PickUpLogPage.class);

	public PickUpLogPage(WebDriver driver) {
		super(driver);
	}

	public static final By PICKUP_LOG_PAGE_LOCATOR = By
			.xpath("//label[contains(text(),'Pickup Date and Time')]");
	public static final By ACTUAL_TIME_IN_DATE_PICKER_LOCATOR = By
			.xpath("//input[@id=\"dateactualArrivals0\"]/following-sibling::button");
	public static final By ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR = By
			.xpath("//*[@id=\"dateactualDepartures0\"]/following-sibling::button");
	public static final By ACTUAL_TIME_IN_DATE_PICKER_LOCATOR1 = By
			.xpath("//input[@id=\"dateactualArrivals1\"]/following-sibling::button");
	public static final By ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR1 = By
			.xpath("//*[@id=\"dateactualDepartures1\"]/following-sibling::button");
	public static final By SERVICE_FAILURE_BTN_LOCATOR = By.id("serviceFailure-0");
	public static final By NEXT_STEP_BTN_LOCATOR = By.id("buttonarrivalDepartureSave");
	public static final By SAVE_BTN_LOCATOR = By.id("arrivalDepartureSave");
	public static final By PAGE_TITLE_LOCATOR = By
			.xpath("//*[@id=\"floatingHeader\"]/app-breadcrumb/div/div/div/div/label");
	
	public void seletActualTimeInDate(String date) {
		logger.debug("Selecting actual time in date.....");
		clickOnActualTimeInDatePicker();
		datePickerHandler.selectDate(date);
		logger.info("Selected actual time in date");
	}

	public void seletActualTimeOutDate(String date) {
		logger.debug("Selecting actual time out date.....");
		clickOnActualTimeOutDatePicker();
		datePickerHandler.selectDate(date);
		logger.info("Selected actual time out date");
	}
	
	public void seletActualTimeInDate1(String date) {
		logger.debug("Selecting actual time in date.....");
		clickOnActualTimeInDatePicker1();
		datePickerHandler.selectDate(date);
		logger.info("Selected actual time in date");
	}

	public void seletActualTimeOutDate1(String date) {
		logger.debug("Selecting actual time out date.....");
		clickOnActualTimeOutDatePicker1();
		datePickerHandler.selectDate(date);
		logger.info("Selected actual time out date");
	}
	
	public void clickOnActualTimeInDatePicker() {
		logger.debug("Clicking on Actual time in date picker button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ACTUAL_TIME_IN_DATE_PICKER_LOCATOR)) {
			userActions.getElementInViewport(ACTUAL_TIME_IN_DATE_PICKER_LOCATOR);
			userActions.clickOnElement(ACTUAL_TIME_IN_DATE_PICKER_LOCATOR);
			logger.info("Clicked Actual time in date picker");
		} else {
			logger.error("Actual time in date picker button is not present or diabled");
		}
	}

	public void clickOnActualTimeOutDatePicker() {
		logger.debug("Clicking on Actual time out date picker button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR)) {
			userActions.getElementInViewport(ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR);
			userActions.clickOnElement(ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR);
			logger.info("Clicked Actual time out date picker");
		} else {
			logger.error("Actual time out date picker button is not present or diabled");
		}
	}
	
	public void clickOnActualTimeInDatePicker1() {
		logger.debug("Clicking on Actual time in date picker button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ACTUAL_TIME_IN_DATE_PICKER_LOCATOR1)) {
			userActions.getElementInViewport(ACTUAL_TIME_IN_DATE_PICKER_LOCATOR1);
			userActions.clickOnElement(ACTUAL_TIME_IN_DATE_PICKER_LOCATOR1);
			logger.info("Clicked Actual time in date picker");
		} else {
			logger.error("Actual time in date picker button is not present or diabled");
		}
	}

	public void clickOnActualTimeOutDatePicker1() {
		logger.debug("Clicking on Actual time out date picker button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR1)) {
			userActions.getElementInViewport(ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR1);
			userActions.clickOnElement(ACTUAL_TIME_OUT_DATE_PICKER_LOCATOR1);
			logger.info("Clicked Actual time out date picker");
		} else {
			logger.error("Actual time out date picker button is not present or diabled");
		}
	}

	public void addServiceFailure(DelayDescription description, DelayStatus status, String comments) {
		logger.debug("Adding service failure with [DelayDescription: " + description.toString() + ",DelayStatus: "
				+ status.toString() + ",comments: " + comments + "].....");
		clickOnServiceFailureButton();
		delayReasonPopupHandler.enterServiceFailureDetails(description, status, comments);
		logger.info("Service failure added with [DelayDescription: " + description.toString() + ",DelayStatus: "
				+ status.toString() + ",comments: " + comments + "]");
	}

	public void clickOnServiceFailureButton() {
		logger.debug("Clicking on service failurer button.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(SERVICE_FAILURE_BTN_LOCATOR)) {
			userActions.getElementInViewport(SERVICE_FAILURE_BTN_LOCATOR);
			userActions.clickOnElement(SERVICE_FAILURE_BTN_LOCATOR);
			logger.info("Clicked service failure button");
		} else {
			logger.error("Service failure button is not present or diabled");
		}
	}

	public void clickOnNextStep() {
		logger.debug("Clicking on Next Step button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(NEXT_STEP_BTN_LOCATOR)) {
			userActions.getElementInViewport(NEXT_STEP_BTN_LOCATOR);
			userActions.clickOnElement(NEXT_STEP_BTN_LOCATOR);
			logger.info("Clicked  Next Step button");
			waits.waitTillInVisibilityOfElement(LOADING_SPINNER);
		}else {
			logger.error(" Next Step button is not present or diabled");
		}
	}

	public void clickOnSave() {
		logger.debug("Clicking on Save button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(SAVE_BTN_LOCATOR)) {
			userActions.getElementInViewport(SAVE_BTN_LOCATOR);
			userActions.clickOnElement(SAVE_BTN_LOCATOR);
			logger.info("Clicked  Save button");
			waits.waitTillInVisibilityOfElement(LOADING_SPINNER);
		}else {
			logger.error(" Save button is not present or diabled");
		}
	}
	
	/**
	 * method to verify if user is in pick up page
	 */
	public boolean verifyUserIsInPickUpPage() {
		String pageTitle = getPageTitle();
		Boolean isOrderCreatePage = pageTitle.equalsIgnoreCase("Pickup Log");
		logger.debug("verifying current page is Pickup Log page");
		if (isOrderCreatePage) {
			logger.info("Current page is Pickup Log page");
			return true;
		}
		return false;
	}

	/**
	 * method to verify if user is in edit pick up
	 */
	public boolean verifyUserIsInEditPickUpPage() {
		String pageTitle = getPageTitle();
		Boolean isOrderCreatePage = pageTitle.equalsIgnoreCase("Edit Pickup Log");
		logger.debug("verifying current page is edit Pickup Log page");
		if (isOrderCreatePage) {
			logger.info("Current page is edit Pickup Log page");
			return true;
		}
		return false;
	}

	/**
	 * method to get page title
	 */
	private String getPageTitle() {
		logger.debug("Getting current page title");
		waits.waitTillVisibilityOfElement(PAGE_TITLE_LOCATOR);
		WebElement pageTitleText = webElementsUtils.getElement(PAGE_TITLE_LOCATOR);
		return pageTitleText.getText();
	}
	
	public void waitTillPickupLogPageLoadOut() {
		Boolean isInCreateOrderPage = waits.waitTillVisibilityOfElement(PICKUP_LOG_PAGE_LOCATOR);
		if (!isInCreateOrderPage) {
			logger.error("PickupLogPage loadout failed");
			throw new RuntimeException("PickupLogPage loadout failed");
		}
		logger.info("PickupLogPage loadout successful");
	}
}
