package pageRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import basePage.BasePage;

public class OpsClosedPage extends BasePage{
	private static final Logger logger = LogManager.getLogger(OpsClosedPage.class);
	
	public OpsClosedPage(WebDriver driver) {
		super(driver);
	}
	
	public static final By OPS_CLOSE_BUTTON_LOCATOR = 
			By.id("opsClosed");
	public static final By SHOW_DETAILS_BUTTON_LOCATOR = 
			By.id("showDetails");
	public static final By CANCEL_LOCATOR = 
			By.id("cancelopsClosed");
	public static final By REOPEN_BUTTON_LOCATOR = 
			By.id("undoOpsClosed");
	public static final By LIST_OF_DOCUMENT_LOCATOR = 
			By.xpath("//*[@id=\"documentsModuleId\"]/a");
	public static final By OPS_CLOSE_POPUP_PROCEED_LOCATOR = 
			By.xpath("//span[text() = 'Proceed']");
	public static final By OPS_CLOSE_POPUP_CANCEL_LOCATOR = 
			By.xpath("//span[text() = 'cancel']");
	public static final By PAGE_TITLE_LOCATOR = By
			.xpath("//*[@id=\"floatingHeader\"]/app-breadcrumb/div/div/div/div/label");
	public static final By OPS_CLOSED_PAGE_LOCATOR = 
			By.xpath("//label[contains(text(),'Ops Closed')]");
	
	public void closeCurrentOrder() {
		logger.debug("Closing current order.....");
		clickOnOpsCloseButton();
		waits.waitTillVisibilityOfElement(OPS_CLOSE_POPUP_PROCEED_LOCATOR);
		logger.debug("Clicking on ops close proceed button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(OPS_CLOSE_POPUP_PROCEED_LOCATOR)) {
			userActions.getElementInViewport(OPS_CLOSE_POPUP_PROCEED_LOCATOR);
			userActions.clickOnElement(OPS_CLOSE_POPUP_PROCEED_LOCATOR);
			logger.info("Clicked ops close proceed button");
		}else {
			logger.error("Ops close proceed button is not present or enabled. Check if Ops close popup is opened");
		}
		logger.info("Closed current order");
	}
	
	public void reopenCurrentOrder() {
		logger.debug("Reopen current order.....");
		clickOnReopenOrder();
		waits.waitTillVisibilityOfElement(OPS_CLOSE_POPUP_PROCEED_LOCATOR);
		logger.debug("Clicking on ops reopen proceed button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(OPS_CLOSE_POPUP_PROCEED_LOCATOR)) {
			userActions.getElementInViewport(OPS_CLOSE_POPUP_PROCEED_LOCATOR);
			userActions.clickOnElement(OPS_CLOSE_POPUP_PROCEED_LOCATOR);
			logger.info("Clicked ops reopen proceed button");
		}else {
			logger.error("Ops reopen proceed button is not present or enabled. Check if Ops reopen popup is opened");
		}
		logger.info("Closed current order");
	}
	
	public void clickOnCancelInOpsCloseConfirmationPopup() {
		waits.waitTillVisibilityOfElement(OPS_CLOSE_POPUP_PROCEED_LOCATOR);
		logger.debug("Clicking on ops close popup cancel button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(OPS_CLOSE_POPUP_CANCEL_LOCATOR)) {
			userActions.getElementInViewport(OPS_CLOSE_POPUP_CANCEL_LOCATOR);
			userActions.clickOnElement(OPS_CLOSE_POPUP_CANCEL_LOCATOR);
			logger.info("Clicked on ops close popup cancel button");
		}else {
			logger.error("Ops close popup cancel button is not present or enabled. Check if Ops reopen popup is opened");
		}
	}
	
	public void clickOnOpsCloseButton() {
		logger.debug("Clicking on ops close button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(OPS_CLOSE_BUTTON_LOCATOR)) {
			userActions.getElementInViewport(OPS_CLOSE_BUTTON_LOCATOR);
			userActions.clickOnElement(OPS_CLOSE_BUTTON_LOCATOR);
			logger.info("Clicked  Ops Close button");
		}else {
			logger.error(" Ops close button is not present or diabled");
		}
	}
	
	public void clickOnShowDetailsButton() {
		logger.debug("Clicking on show detials button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(SHOW_DETAILS_BUTTON_LOCATOR)) {
			userActions.getElementInViewport(SHOW_DETAILS_BUTTON_LOCATOR);
			userActions.clickOnElement(SHOW_DETAILS_BUTTON_LOCATOR);
			logger.info("Clicked  show detials button");
		}else {
			logger.error("Show detials button is not present or diabled");
		}
	}
	
	public void clickOnCancelButton() {
		logger.debug("Clicking on cancel button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(CANCEL_LOCATOR)) {
			userActions.getElementInViewport(CANCEL_LOCATOR);
			userActions.clickOnElement(CANCEL_LOCATOR);
			logger.info("Clicked cancel button");
		}else {
			logger.error("Cancel button is not present or diabled");
		}
	}
	
	public void clickOnListOfDocuments() {
		logger.debug("Clicking on list of documents tab.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(LIST_OF_DOCUMENT_LOCATOR)) {
			userActions.getElementInViewport(LIST_OF_DOCUMENT_LOCATOR);
			userActions.clickOnElement(LIST_OF_DOCUMENT_LOCATOR);
			logger.info("Clicked list of documents tab");
		}else {
			logger.error("List of documents tab is not present or diabled");
		}
	}
	
	public void clickOnReopenOrder() {
		logger.debug("Clicking on reopen button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(REOPEN_BUTTON_LOCATOR)) {
			userActions.getElementInViewport(REOPEN_BUTTON_LOCATOR);
			userActions.clickOnElement(REOPEN_BUTTON_LOCATOR);
			logger.info("Clicked reopen button");
		}else {
			logger.error("Reopen button is not present or diabled");
		}
	}
	
	/**
	 * method to verify if user is in ops close page
	 */
	public boolean verifyUserIsInPickUpPage() {
		String pageTitle = getPageTitle();
		Boolean isOrderCreatePage = pageTitle.equalsIgnoreCase("Ops Closed");
		logger.debug("verifying current page is Ops Closed page");
		if (isOrderCreatePage) {
			logger.info("Current page is Ops Closed page");
			return true;
		}
		return false;
	}

	/**
	 * method to verify if user is in edit ops close log
	 */
	public boolean verifyUserIsInEditPickUpPage() {
		String pageTitle = getPageTitle();
		Boolean isOrderCreatePage = pageTitle.equalsIgnoreCase("Edit Ops Closed");
		logger.debug("verifying current page is edit Ops Closed page");
		if (isOrderCreatePage) {
			logger.info("Current page is edit Ops Closed page");
			return true;
		}
		return false;
	}

	/**
	 * method to get page title
	 */
	private String getPageTitle() {
		logger.debug("Getting current page title");
		waits.waitTillVisibilityOfElement(PAGE_TITLE_LOCATOR);
		WebElement pageTitleText = webElementsUtils.getElement(PAGE_TITLE_LOCATOR);
		return pageTitleText.getText();
	}
	
	public void waitTillOpsClosedPageLoadOut() {
		Boolean isInCreateOrderPage = waits.waitTillVisibilityOfElement(OPS_CLOSED_PAGE_LOCATOR);
		if (!isInCreateOrderPage) {
			logger.error("OpsClosedPage loadout failed");
			throw new RuntimeException("OpsClosedPage loadout failed");
		}
		logger.info("OpsClosedPage loadout successful");
	}
}
