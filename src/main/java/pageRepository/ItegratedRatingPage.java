package pageRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import basePage.BasePage;
import pageRepository.commonPageUtils.CustomerSelectionHandler;
import pageRepository.commonPageUtils.SideNavigationMenuHandler;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class ItegratedRatingPage extends BasePage{

	public ItegratedRatingPage(WebDriver driver) {
		super(driver);
	}
	
	private static final Logger logger = LogManager.getLogger(ItegratedRatingPage.class);
	public static final By INTEGRATED_RATING_LOCATOR = 
			By.xpath("");
	public static final By FIND_CARRIER_RATES_BUTTON = 
			By.id("findRate");
	public static final By FIRST_QUALIFIED_CARRIER = 
			By.xpath("//div[contains(@class, 'label green')]");
	public static final By FIRST_QUALIFIED_CARRIER_SELECT_RADIO_BTN = 
			By.xpath("//div[contains(@class, 'label green')]/parent::div/parent::div/following-sibling::div/div/div[5]/div/div/input");
	public static final By FIRST_QUALIFIED_CARRIER_SAVE_COMPLETE_BTN = 
			By.xpath("//div[contains(@class, 'label green')]/parent::div/parent::div/following-sibling::div/div/div[5]/div[2]/div/button");
	
	public static final By CONFIRMATION_POPUP = 
			By.xpath("/html/body/app-root/app-integrated-rating/app-header/p-confirmdialog[1]/div");
	public static final By CONFIRMATION_POPUP_CONFIRM_BTN = 
			By.xpath("/html/body/app-root/app-integrated-rating/app-header/p-confirmdialog[1]/div/div[3]/button[1]");
	
	public void applyRating() {
		clickFindCarrierRatesBtn();
		waits.waitTillInVisibilityOfElement(LOADING_SPINNER);
		selectFirstQualifiedCarrier();
		waits.waitTillInVisibilityOfElement(LOADING_SPINNER);
	}
	
	public void clickFindCarrierRatesBtn() {
		logger.debug("Clicking on find carrier rates button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(FIND_CARRIER_RATES_BUTTON)) {
			userActions.getElementInViewport(FIND_CARRIER_RATES_BUTTON);
			userActions.clickOnElement(FIND_CARRIER_RATES_BUTTON);
			logger.info("Clicked find carrier rates button");
		}else {
			logger.error("Find carrier rates button is not present or diabled");
		}
	}
	
	public void selectFirstQualifiedCarrier() {
		logger.info("Selecting first qualified carrier");
		WebElement firstQualifiedCarrier = 
				webElementsUtils.getElement(FIRST_QUALIFIED_CARRIER);
		userActions.moveToElement(firstQualifiedCarrier);
		userActions.clickOnElement(FIRST_QUALIFIED_CARRIER);
		userActions.clickOnElement(CONFIRMATION_POPUP);	
		waits.waitTillVisibilityOfElement(FIRST_QUALIFIED_CARRIER_SELECT_RADIO_BTN);
		WebElement firstQualifiedCarrierSelectRadioBtn = 
				webElementsUtils.getElement(FIRST_QUALIFIED_CARRIER_SELECT_RADIO_BTN);
		userActions.moveToElement(firstQualifiedCarrierSelectRadioBtn);
		userActions.clickOnElement(FIRST_QUALIFIED_CARRIER_SELECT_RADIO_BTN);	
		WebElement firstQualifiedCarrierSaveCompleteRadioBtn = 
				webElementsUtils.getElement(FIRST_QUALIFIED_CARRIER_SAVE_COMPLETE_BTN);
		userActions.moveToElement(firstQualifiedCarrierSaveCompleteRadioBtn);
		userActions.clickOnElement(FIRST_QUALIFIED_CARRIER_SAVE_COMPLETE_BTN);
		waits.waitTillVisibilityOfElement(CONFIRMATION_POPUP);
		WebElement confirmationPopupConfirmBtn = 
				webElementsUtils.getElement(CONFIRMATION_POPUP_CONFIRM_BTN);
		userActions.moveToElement(confirmationPopupConfirmBtn);
		userActions.clickOnElement(CONFIRMATION_POPUP_CONFIRM_BTN);		
		logger.info("Selected first qualified carrier");
	}
	
	public void waitTillItegratedRatingPageLoadOut() {
		Boolean isInCreateOrderPage = waits.waitTillVisibilityOfElement(INTEGRATED_RATING_LOCATOR);
		if (!isInCreateOrderPage) {
			logger.error("IntegratedRatingPage loadout failed");
			throw new RuntimeException("IntegratedRatingPage loadout failed");
		}
		logger.info("IntegratedRatingPage loadout successful");
	}
}
