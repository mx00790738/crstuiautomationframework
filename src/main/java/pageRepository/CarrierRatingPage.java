package pageRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import basePage.BasePage;

public class CarrierRatingPage extends BasePage{
	private static final Logger logger = LogManager.getLogger(CarrierRatingPage.class);
	
	public CarrierRatingPage(WebDriver driver) {
		super(driver);
	}
	public static final By CARRIER_RATING_PAGE_LOCATOR = 
			By.xpath("//label[contains(text(),'Carrier Rating')]");
	public static final By ADDITIONAL_CHARGE_SELECTION_DROPDOWN_LOCATOR = 
			By.id("custAddCharge");
	public static final By NEXT_STEP_BTN_LOCATOR = 
			By.id("buttonCarrierRatingSave");
	public static final By CARRIER_SEARCH_BTN_LOCATOR = 
			By.id("carrierIdBtncarrierRate");
	
	public static final By LINE_HAUL_RATE_ENTRY_TXTBOX_LOCATOR = 
			By.id("unit1");
	public static final By FUEL_SURCHARGE_ENTRY_TXTBOX_LOCATOR = 
			By.name("ratingUnitFSC");
	public static final By DISCOUNT_ENTRY_TXTBOX_LOCATOR = 
			By.name("ratingUnitDiscount");
	public static final By SAVE_BTN_LOCATOR = 
			By.id("CarrierRatingSave");
	public static final By PAGE_TITLE_LOCATOR = By
			.xpath("//*[@id=\"floatingHeader\"]/app-breadcrumb/div/div/div/div/label");
	
	
	public void enterCarrierRating(String carrierId,String lineHaulCharge,String fuelSurCharge,String discount) {
		logger.info("Carrier rating order.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(CARRIER_SEARCH_BTN_LOCATOR)) {
			userActions.getElementInViewport(CARRIER_SEARCH_BTN_LOCATOR);
			userActions.clickOnElement(CARRIER_SEARCH_BTN_LOCATOR);
			logger.info("Clicked carrier search button");
		}else {
			logger.error("Carrier search button is not present or diabled");
		}
		selectCarrier(carrierId);
		enterLineHaulCharge(lineHaulCharge);
		enterFuelSurcharge(fuelSurCharge);
		enterDiscount(discount);
		logger.info("Carrier rated order");
	}
	
	public void enterLineHaulCharge(String charge) {
		logger.debug("Entering line haul charge.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(LINE_HAUL_RATE_ENTRY_TXTBOX_LOCATOR)) {
			userActions.getElementInViewport(LINE_HAUL_RATE_ENTRY_TXTBOX_LOCATOR);
			userActions.enterTextInTextBox(LINE_HAUL_RATE_ENTRY_TXTBOX_LOCATOR, charge);
			logger.info("Entered line haul charge");
		}else {
			logger.error("Line haul charge entry text box is not present or diabled");
		}
	}
	
	public void enterFuelSurcharge(String charge) {
		logger.debug("Entering fuel surcharge charge.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(FUEL_SURCHARGE_ENTRY_TXTBOX_LOCATOR)) {
			userActions.getElementInViewport(FUEL_SURCHARGE_ENTRY_TXTBOX_LOCATOR);
			userActions.enterTextInTextBox(FUEL_SURCHARGE_ENTRY_TXTBOX_LOCATOR, charge);
			logger.info("Entered fuel surcharge charge");
		}else {
			logger.error("Fuel surcharge entry text box is not present or diabled");
		}
	}
	
	public void enterDiscount(String discount) {
		logger.debug("Entering discount.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(DISCOUNT_ENTRY_TXTBOX_LOCATOR)) {
			userActions.getElementInViewport(DISCOUNT_ENTRY_TXTBOX_LOCATOR);
			userActions.enterTextInTextBox(DISCOUNT_ENTRY_TXTBOX_LOCATOR, discount);
			logger.info("Entered discount");
		}else {
			logger.error("Discount entry text box is not present or diabled");
		};
	}
	
	public void selectCarrier(String carrierId) {
		carrierSearchPopUpHandler.enterCarrierID(carrierId);
		carrierSearchPopUpHandler.clickOnSearch();
		carrierSearchPopUpHandler.clickOnFirstQualifiedResult();
	}
	public void clickOnNextStep() {
		logger.debug("Clicking on Next Step button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(NEXT_STEP_BTN_LOCATOR)) {
			userActions.getElementInViewport(NEXT_STEP_BTN_LOCATOR);
			userActions.clickOnElement(NEXT_STEP_BTN_LOCATOR);
			logger.info("Clicked  Next Step button");
			waits.waitTillInVisibilityOfElement(LOADING_SPINNER);
		}else {
			logger.error(" Next Step button is not present or diabled");
		}
	}
	
	public void clickOnSave() {
		logger.debug("Clicking on Save button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(SAVE_BTN_LOCATOR)) {
			userActions.getElementInViewport(SAVE_BTN_LOCATOR);
			userActions.clickOnElement(SAVE_BTN_LOCATOR);
			logger.info("Clicked Save button");
			waits.waitTillInVisibilityOfElement(LOADING_SPINNER);
		}else {
			logger.error(" Save button is not present or diabled");
		}
	}
	
	/**
	 * method to verify if user is in customer rating page
	 */
	public boolean verifyUserIsInCarrierRatingPage() {
		String pageTitle = getPageTitle();
		Boolean isOrderCreatePage = pageTitle.equalsIgnoreCase("Carrier Rating");
		logger.debug("verifying current page is CarrierRatingPage");
		if (isOrderCreatePage) {
			logger.info("Current page is CarrierRatingPage");
			return true;
		}
		return false;
	}

	/**
	 * method to verify if user is in edit customer rating
	 */
	public boolean verifyUserIsInEditCarrierRatingPage() {
		String pageTitle = getPageTitle();
		Boolean isOrderCreatePage = pageTitle.equalsIgnoreCase("Edit Carrier Rating");
		logger.debug("verifying current page is edit customer rating page");
		if (isOrderCreatePage) {
			logger.info("Current page is edit customer rating page");
			return true;
		}
		return false;
	}

	/**
	 * method to get page title
	 */
	private String getPageTitle() {
		logger.debug("Getting current page title");
		waits.waitTillVisibilityOfElement(PAGE_TITLE_LOCATOR);
		WebElement pageTitleText = webElementsUtils.getElement(PAGE_TITLE_LOCATOR);
		return pageTitleText.getText();
	}
	
	public void waitTillCarrierRatingPageLoadOut() {
		Boolean isInCarrierConfirmationPage = waits.waitTillVisibilityOfElement(CARRIER_RATING_PAGE_LOCATOR);
		if (!isInCarrierConfirmationPage) {
			logger.error("CarrierRatingPage loadout failed");
			throw new RuntimeException("CarrierRatingPage loadout failed");
		}
		logger.info("CarrierRatingPage loadout successful");
	}

}
