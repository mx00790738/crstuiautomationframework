package pageRepository;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import basePage.BasePage;
import enumRepository.CommentType;
import enumRepository.OrderType;

public class CreateOrderPage extends BasePage {
	private static final Logger logger = LogManager.getLogger(CreateOrderPage.class);

	public static final By CREATE_ORDER_PAGE_LOCATOR = By.xpath("//label[contains(text(),'Create Order')]");
	public static final By PAGE_TITLE_LOCATOR = By
			.xpath("//*[@id=\"floatingHeader\"]/app-breadcrumb/div/div/div/div/label");
	// Create order main menu
	public static final By FTL_ORDER_ICON_LOCATOR = By.id("createFTLOrder");
	public static final By LTL_ORDER_ICON_LOCATOR = By.id("createLTLOrder");
	// Customer selection locators
	public static final By CUSTOMER_ID_ENTRY_BOX_LOCATOR = By.id("customerSearch");
	public static final By CUSTOMER_ID_SEARCH_ICON_LOCATOR = By.id("custBtn");
	public static final By CUSTOMER_SEARCH_POPUP = By.xpath("//*[@id=\"customerSearchModal\"]/div/div");
	// order type locators
	public static final By ORDER_TYPE_SELECT_DROPDOWN_LOCATOR = By.id("orderTypeSelect");

	public static final By ORDER_CREATE_PAGE_START_LOCATOR = By.id("pageStart");

	public static final By LOC_SEARCH_BUTTON_PICKUP = By.xpath("//*[@id=\"locationBtnPickUp1\"]");
	public static final By LOC_SEARCH_BUTTON_DROP = By.id("locationBtnDrop1");

	public static final By EDIT_COMMENTS_BUTTON_LOCATOR = By.id("stopCommentsPickUp1");

	public static final By PICKUP_PLANNED_FROM_DATE = By.id("dateinitialPickupPickUp1");
	public static final By PICKUP_PLANNED_TO_DATE = By.id("dateinitialDropPickUp1");
	public static final By DROP_PLANNED_FROM_DATE = By.id("dateinitialPickupDrop1");
	public static final By DROP_PLANNED_TO_DATE = By.id("dateinitialDropDrop1");
	public static final By PICKUP_PLANNED_FROM_DATE_DATE_PICKER = By
			.xpath("//input[@id='dateinitialPickupPickUp1']/following-sibling::button");
	public static final By DEST_PLANNED_FROM_DATE_DATE_PICKER = By
			.xpath("//input[@id='dateinitialPickupDrop1']/following-sibling::button");
	// Commodity Tab
	public static final By COMMODITY_TAB = By.id("consignment-tab");
	public static final By ENTER_ITEM_DETAILS_RADIO_BTN = By.xpath("//input[@id=\"radioI\"]/following::span");
	// Special reference
	public static final By SPECIAL_REF_TAB = By.id("references-tab");
	// Stops tab
	public static final By ADDITIONAL_STOPS_TAB = By.id("additional-stop-tab");

	public static final By CREATE_ORDER_BTN = By.id("createOrder");
	public static final By SAVE_BTN = By.id("SaveButtonCreateOrder");

	public static final By RATING_MODE_CONFIRM_POPUP = By.xpath("//*[@id=\"ratingModeSelection\"]/div");
	public static final By RATING_MODE_CONFIRM_PROCEED_BTN = By
			.xpath("//*[@id=\"ratingModeSelection\"]/div/div/div[3]/button[1]");
	public static final By RATING_MODE_GOBACK_PROCEED_BTN = By
			.xpath("//*[@id=\"ratingModeSelection\"]/div/div/div[3]/button[2]");

	public CreateOrderPage(WebDriver driver) {
		super(driver);
	}

	/**
	 * method to create FTL Order
	 */
	public void createFTLOrder(String customerId, OrderType orderType, String originlocCode, String originDate,
			CommentType originCommentType, String originCommentDisc, String destlocCode, String destDate,
			CommentType destCommentType, String destCommentDisc, String commodityWeight, String commodityPieace,
			String refNo, String bol) {
		logger.info("Creating FTL Order");
		clickOnFTLIconInOrderCreateMainMenu();
		selectCustomer(customerId);
		selectOrderType(orderType);
		// Origin Selection
		selectOriginLocation(originlocCode);
		selectOriginPlannedFromAndToDates(originDate);
		addOriginComments(originCommentType, originCommentDisc);
		// Destination selection
		selectDestinationLocation(destlocCode);
		selectDestinationPlannedFromAndToDates(destDate);
		addDestinationComments(destCommentType, destCommentDisc);
		// Commodity tab
		addEnterTotalCommodityDetails(commodityWeight, commodityPieace);
		// Special reference
		addSpecialReferenceDetails(refNo, bol);
		// Click create button
		clickCreateButton();
	}

	// TODO: NEED TO IMPLEMENT THIS
	public void createFTLOrderWithMultiStop(String customerId, OrderType orderType, String originlocCode,
			String originDate, CommentType originCommentType, String originCommentDisc, String destlocCode,
			String destDate, CommentType destCommentType, String destCommentDisc, String commodityWeight,
			String commodityPieace, HashMap<String, HashMap<String, String>> additionalPickUp,
			HashMap<String, HashMap<String, String>> additionalDrop, String refNo, String bol) {
		WebElement ftlIcon = webElementsUtils.getElement(FTL_ORDER_ICON_LOCATOR);
		ftlIcon.click();
		// Need to change below
		webElementsUtils.getElement(CUSTOMER_ID_SEARCH_ICON_LOCATOR).sendKeys(Keys.PAGE_UP);
		selectCustomer(customerId);
		selectOrderType(orderType);
		// Origin Selection
		selectOriginLocation(originlocCode);
		selectOriginPlannedFromAndToDates(originDate);
		addOriginComments(originCommentType, originCommentDisc);
		// Destination selection
		selectDestinationLocation(destlocCode);
		selectDestinationPlannedFromAndToDates(destDate);
		addDestinationComments(destCommentType, destCommentDisc);
		// Commodity tab
		addEnterTotalCommodityDetails(commodityWeight, commodityPieace);
		// additional stops
		selectAdditionalStopsTab();
		stopAdditionHandler.addMultiplePickUp(additionalPickUp);
		stopAdditionHandler.addMultipleDrop(additionalDrop);
		// Special reference
		addSpecialReferenceDetails(refNo, bol);
		// Click create button
		clickCreateButton();
	}

	public void createLTLVOrder(String customerId, OrderType orderType, String originlocCode, String originDate,
			CommentType originCommentType, String originCommentDisc, String destlocCode, String destDate,
			CommentType destCommentType, String destCommentDisc, String itemID, String itemDesc, String freightClass,
			String weight, String quantity, String refNo, String bol) {
		logger.info("Creating LTL V Order");
		clickOnLTLIconInOrderCreateMainMenu();
		// Need to change below
		selectCustomer(customerId);
		selectOrderType(orderType);
		// Origin Selection
		selectOriginLocation(originlocCode);
		selectOriginPlannedFromAndToDates(originDate);
		addOriginComments(originCommentType, originCommentDisc);
		// Destination selection
		selectDestinationLocation(destlocCode);
		selectDestinationPlannedFromAndToDates(destDate);
		addDestinationComments(destCommentType, destCommentDisc);
		// Commodity tab
		addEnterItemDetails(itemID, itemDesc, freightClass, weight, quantity);
		// Special reference
		addSpecialReferenceDetails(refNo, bol);
		// Click create buttonclickCreateButton
		clickCreateButton();
		clickOnRatingModeConfirmProceedButton();
	}

	/**
	 * method to select customer
	 */
	public void selectCustomer(String customerId) {
		logger.info("Selecting customer with ID: " + customerId + " .....");
		if (webElementsUtils.checkElementIsDisplayed(CUSTOMER_ID_ENTRY_BOX_LOCATOR)) {
			userActions.getElementInViewport(CUSTOMER_ID_ENTRY_BOX_LOCATOR);
			userActions.enterTextInTextBox(CUSTOMER_ID_ENTRY_BOX_LOCATOR, customerId);
			if (webElementsUtils.checkElementIsDisplayedAndEnabled(CUSTOMER_ID_SEARCH_ICON_LOCATOR)) {
				userActions.clickOnElement(CUSTOMER_ID_SEARCH_ICON_LOCATOR);
			} else {
				logger.error("CustomerID search button is not displayed or disabled");
			}
			waits.waitTillInVisibilityOfElement(CUSTOMER_SEARCH_POPUP);
			logger.info("Selected customer with ID: " + customerId);
		} else {
			logger.error("CustomerID Entry text box is not displayed");
		}
	}

	/**
	 * method to select order type
	 */
	public void selectOrderType(OrderType orderType) {
		logger.info("Selecting order type: " + orderType.toString() + " .....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ORDER_TYPE_SELECT_DROPDOWN_LOCATOR)) {
			WebElement orderTypeSelectDropDown = webElementsUtils.getElement(ORDER_TYPE_SELECT_DROPDOWN_LOCATOR);
			Select se = new Select(orderTypeSelectDropDown);
			switch (orderType) {
			case SPOT:
				se.selectByValue("SPOT");
				break;
			case WABTECP:
				se.selectByValue("WABTECP");
				break;
			case CONTRACT:
				se.selectByValue("CONTRACT");
				break;
			case LTL:
				se.selectByValue("LTL");
				break;
			case PASSTHRU:
				se.selectByValue("PASSTHRU");
				break;
			case SPOTLTL:
				se.selectByValue("SPOT-LTL");
				break;
			}
			logger.info("Selected order type: " + orderType.toString());
		} else {
			logger.error("Order Type drop down is not displayed or disabled");
		}
	}

	/**
	 * method to select origin location
	 */
	public void selectOriginLocation(String locCode) {
		logger.info("Selecting origin location code: " + locCode + " .....");
		userActions.getElementInViewport(LOC_SEARCH_BUTTON_PICKUP);
		userActions.clickOnElement(LOC_SEARCH_BUTTON_PICKUP);
		waits.waitImplicitly();
		locationSearchHandler.enterLocationCode(locCode);
		locationSearchHandler.clickLocationCodeSearchBtn();
		locationSearchHandler.clickOnFirstResult(locCode);
		logger.info("Selected origin location code: " + locCode);
	}

	/**
	 * method to select origin planned from date
	 */
	public void selectOriginPlannedFromAndToDates(String date) {
		logger.info("Selecting Origin PlannedFrom date: [" + date + "] .....");
		userActions.getElementInViewport(PICKUP_PLANNED_FROM_DATE_DATE_PICKER);
		userActions.clickOnElement(PICKUP_PLANNED_FROM_DATE_DATE_PICKER);
		datePickerHandler.selectDate(date);
		logger.info("Selected Origin PlannedFrom date: [" + date + "]");
	}

	/**
	 * method to select destination location
	 */
	public void selectDestinationLocation(String locCode) {
		logger.info("Selecting destination location code: " + locCode + " .....");
		userActions.getElementInViewport(LOC_SEARCH_BUTTON_DROP);
		userActions.clickOnElement(LOC_SEARCH_BUTTON_DROP);
		waits.waitImplicitly();
		locationSearchHandler.enterLocationCode(locCode);
		locationSearchHandler.clickLocationCodeSearchBtn();
		locationSearchHandler.clickOnFirstResult(locCode);
		logger.info("Selected destination location code: " + locCode);
	}

	/**
	 * method to select destination planned from date
	 */
	public void selectDestinationPlannedFromAndToDates(String date) {
		logger.info("Selecting Destination PlannedFrom date: [" + date + "] .....");
		userActions.getElementInViewport(DEST_PLANNED_FROM_DATE_DATE_PICKER);
		userActions.clickOnElement(DEST_PLANNED_FROM_DATE_DATE_PICKER);
		datePickerHandler.selectDate(date);
		logger.info("Selected Destination PlannedFrom date: [" + date + "]");
	}

	/**
	 * method to add origin comments
	 */
	public void addOriginComments(CommentType commentType, String commentDisc) {
		logger.info(
				"Adding origin comments[type: " + commentType.toString() + ", " + "commentDisc: " + commentDisc + "]");
		userActions.getElementInViewport(EDIT_COMMENTS_BUTTON_LOCATOR);
		userActions.clickOnElement(EDIT_COMMENTS_BUTTON_LOCATOR);
		commentSectionHandler.selectCommentType(commentType);
		commentSectionHandler.enterCommentDiscription(commentDisc);
		commentSectionHandler.clickCommentAdd();
		commentSectionHandler.closePopUp();
		logger.info("Added origin comments");
	}

	/**
	 * method to add destination comments
	 */
	public void addDestinationComments(CommentType destCommentType, String destCommentDisc) {

	}

	/**
	 * method to EnterTotalCommodityDetails
	 */
	public void addEnterTotalCommodityDetails(String weight, String pieace) {
		selectCommodityTab();
		logger.info("Entering commodity details [weight:" + weight + ",pieaces:" + pieace + "]");
		commoditySelectionHandler.enterCommodityDetails(weight, pieace);
		logger.info("Entered commodity details");
	}

	/**
	 * method to EnterTotalCommodityDetails
	 */
	public void addEnterItemDetails(String itemID, String itemDesc, String freightClass, String weight,
			String quantity) {
		selectCommodityTab();
		logger.info("Entering commodity details [itemID:" + itemID + ",itemDesc:" + itemDesc + ",freightClass:"
				+ freightClass + ",weight:" + weight + ",quantity:" + quantity + "]");
		selectEnterItemDetails();
		commoditySelectionHandler.enterCommodityDetails(itemID, itemDesc, freightClass, weight, quantity);
		commoditySelectionHandler.clickAddConsignment();
		logger.info("Item details entered");
	}

	/**
	 * method to selectEnterItemDetails radio button
	 */
	public void selectEnterItemDetails() {
		logger.debug("Clicking on Enter Item Details radio button.....");
		userActions.clickOnElement(ENTER_ITEM_DETAILS_RADIO_BTN);
		logger.debug("Clicked on Enter Item Details radio button");
	}

	/**
	 * method to select commodity tab
	 */
	public void selectCommodityTab() {
		logger.debug("Selecting commodity tab .....");
		userActions.getElementInViewport(COMMODITY_TAB);
		userActions.clickOnElement(COMMODITY_TAB);
		logger.info("Selected commodity tab");
	}

	/**
	 * method to add special reference details
	 */
	public void addSpecialReferenceDetails(String refNo, String bol) {
		selectSpecialReferenceTab();
		specialReferenceHandler.enterSpecialReferenceDetails(refNo, bol);
	}

	/**
	 * method to add special reference tab
	 */
	public void selectSpecialReferenceTab() {
		logger.debug("Selecting special reference tab .....");
		userActions.getElementInViewport(SPECIAL_REF_TAB);
		userActions.clickOnElement(SPECIAL_REF_TAB);
		logger.info("Selected special reference tab");
	}

	/**
	 * method to add additional stops tab
	 */
	private void selectAdditionalStopsTab() {
		logger.debug("Selecting additional stops tab .....");
		userActions.getElementInViewport(ADDITIONAL_STOPS_TAB);
		userActions.clickOnElement(ADDITIONAL_STOPS_TAB);
		logger.info("Selected additional stops tab");
	}

	/**
	 * method to click create button
	 */
	private void clickCreateButton() {
		logger.debug("Clicking create button.....");
		userActions.getElementInViewport(CREATE_ORDER_BTN);
		userActions.clickOnElement(CREATE_ORDER_BTN);
		logger.info("Clicked create button");
		waits.waitTillInVisibilityOfElement(LOADING_SPINNER);
	}

	/**
	 * method to click save button
	 */
	public void clickSaveButton() {
		logger.debug("Clicking save button.....");
		userActions.getElementInViewport(SAVE_BTN);
		userActions.clickOnElement(SAVE_BTN);
		logger.info("Clicked save button");
		waits.waitTillInVisibilityOfElement(LOADING_SPINNER);
	}

	/**
	 * method to click on rating mode confirm popup proceed button
	 */
	private void clickOnRatingModeConfirmProceedButton() {
		waits.waitTillVisibilityOfElement(RATING_MODE_CONFIRM_POPUP);
		logger.debug("Clicking on rating mode confirm popup proceed button.....");
		userActions.getElementInViewport(RATING_MODE_CONFIRM_PROCEED_BTN);
		userActions.clickOnElement(RATING_MODE_CONFIRM_PROCEED_BTN);
		logger.info("Clicked on rating mode confirm popup proceed button");
	}

	/**
	 * method to clicked on FTL Icon in order create main menu
	 */
	public void clickOnFTLIconInOrderCreateMainMenu() {
		logger.debug("Clicking on FTL Icon in order create main menu.....");
		userActions.getElementInViewport(FTL_ORDER_ICON_LOCATOR);
		userActions.clickOnElement(FTL_ORDER_ICON_LOCATOR);
		logger.info("Clicked on FTL Icon in order create main menu");
	}

	/**
	 * method to clicked on LTL Icon in order create main menu
	 */
	public void clickOnLTLIconInOrderCreateMainMenu() {
		logger.debug("Clicking on FTL Icon in order create main menu.....");
		userActions.getElementInViewport(LTL_ORDER_ICON_LOCATOR);
		userActions.clickOnElement(LTL_ORDER_ICON_LOCATOR);
		logger.info("Clicked on FTL Icon in order create main menu");
	}

	/**
	 * method to get order ID created
	 */
	public void getOrderIDCreated() {
		logger.debug("Getting order ID.....");
		orderDetailsHeaderHandler.getOrderID();
		logger.info("Order ID Obtained");
	}

	/**
	 * method to verify if user is in create order page main menu
	 */
	public boolean verifyUserIsInCreateOrderPageMainMenu() {
		waits.waitTillVisibilityOfElement(By.xpath("//*[@id=\"view\"]/h3"));
		String pageTitle = webElementsUtils.getElement(By.xpath("//*[@id=\"view\"]/h3")).getText();
		Boolean isOrderCreatePageMainMenu = pageTitle.equalsIgnoreCase("Create Order");
		logger.debug("verifying current page is create order page main menu");
		if (isOrderCreatePageMainMenu) {
			logger.info("Current page is create order page main menu");
			return true;
		}
		return false;
	}

	/**
	 * method to verify if user is in create order page
	 */
	public boolean verifyUserIsInCreateOrderPage() {
		String pageTitle = getPageTitle();
		Boolean isOrderCreatePage = pageTitle.equalsIgnoreCase("Create Order");
		logger.debug("verifying current page is create order page");
		if (isOrderCreatePage) {
			logger.info("Current page is create order page");
			return true;
		}
		return false;
	}

	/**
	 * method to verify if user is in edit order page
	 */
	public boolean verifyUserIsInEditOrderPage() {
		String pageTitle = getPageTitle();
		Boolean isOrderCreatePage = pageTitle.equalsIgnoreCase("Edit Order");
		logger.debug("verifying current page is create order page");
		if (isOrderCreatePage) {
			logger.info("Current page is edit order page");
			return true;
		}
		return false;
	}

	/**
	 * method to get page title
	 */
	private String getPageTitle() {
		logger.debug("Getting current page title");
		waits.waitTillVisibilityOfElement(PAGE_TITLE_LOCATOR);
		WebElement pageTitleText = webElementsUtils.getElement(PAGE_TITLE_LOCATOR);
		return pageTitleText.getText();
	}

	public void waitTillControlTowerPageLoadOut() {
		Boolean isInCreateOrderPage = waits.waitTillVisibilityOfElement(CREATE_ORDER_PAGE_LOCATOR);
		if (!isInCreateOrderPage) {
			logger.error("CreateOrderPage loadout failed");
			throw new RuntimeException("CreateOrderPage loadout failed");
		}
		logger.info("CreateOrderPage loadout successful");
	}
}
