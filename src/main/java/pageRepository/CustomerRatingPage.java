package pageRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import basePage.BasePage;

public class CustomerRatingPage extends BasePage {
	private static final Logger logger = LogManager.getLogger(CustomerRatingPage.class);
	
	public CustomerRatingPage(WebDriver driver) {
		super(driver);
	}
	
	public static final By CUSTOMER_RATING_PAGE_LOCATOR = By
			.xpath("//label[contains(text(),'Customer Rating')]");
	public static final By LINE_HAUL_RATE_ENTRY_TXTBOX_LOCATOR = 
			By.name("rate1");
	public static final By FUEL_SURCHARGE_ENTRY_TXTBOX_LOCATOR = 
			By.name("rate3");
	public static final By DISCOUNT_ENTRY_TXTBOX_LOCATOR = 
			By.name("rate2");
	public static final By ADDITIONAL_CHARGE_SELECTION_DROPDOWN_LOCATOR = 
			By.id("custAddCharge");
	public static final By RATE_ENTRY_TXTBOX_LOCATOR = 
			By.id("rateAdditionalCharge");
	public static final By ADD_CHARGE_BTN_LOCATOR = 
			By.id("buttonSavecustomerRating");
	public static final By CUSTOMER_SEARCH_BTN_LOCATOR = 
			By.id("customerDataBtn");
	public static final By NEXT_STEP_BTN_LOCATOR = 
			By.id("buttonCustomerRatingSave");
	public static final By SAVE_BTN_LOCATOR = 
			By.id("CustomerRatingSave");
	public static final By PAGE_TITLE_LOCATOR = By
			.xpath("//*[@id=\"floatingHeader\"]/app-breadcrumb/div/div/div/div/label");
	
	
	public void enterCustomerRating(String charge,String fuelSurCharge,String discount) {
		logger.info("Customer rating order.....");
		enterLineHaulCharge(charge);
		enterFuelSurcharge(fuelSurCharge);
		enterDiscount(discount);
		logger.info("Customer rated order");
	}
	
	public void enterLineHaulCharge(String charge) {
		logger.debug("Entering line haul charge.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(LINE_HAUL_RATE_ENTRY_TXTBOX_LOCATOR)) {
			userActions.getElementInViewport(LINE_HAUL_RATE_ENTRY_TXTBOX_LOCATOR);
			userActions.enterTextInTextBox(LINE_HAUL_RATE_ENTRY_TXTBOX_LOCATOR, charge);
			logger.info("Entered line haul charge");
		}else {
			logger.error("Line haul charge entry text box is not present or diabled");
		}
	}
	
	public void enterFuelSurcharge(String charge) {
		logger.debug("Entering fuel surcharge charge.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(FUEL_SURCHARGE_ENTRY_TXTBOX_LOCATOR)) {
			userActions.getElementInViewport(FUEL_SURCHARGE_ENTRY_TXTBOX_LOCATOR);
			userActions.enterTextInTextBox(FUEL_SURCHARGE_ENTRY_TXTBOX_LOCATOR, charge);
			logger.info("Entered fuel surcharge charge");
		}else {
			logger.error("Fuel surcharge entry text box is not present or diabled");
		}
	}
	
	public void enterDiscount(String discount) {
		logger.debug("Entering discount.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(DISCOUNT_ENTRY_TXTBOX_LOCATOR)) {
			userActions.getElementInViewport(DISCOUNT_ENTRY_TXTBOX_LOCATOR);
			userActions.enterTextInTextBox(DISCOUNT_ENTRY_TXTBOX_LOCATOR, discount);
			logger.info("Entered discount");
		}else {
			logger.error("Discount entry text box is not present or diabled");
		}
	}
	
	public void enterAdditionalRate(String charge) {
		logger.debug("Entering rate.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(RATE_ENTRY_TXTBOX_LOCATOR)) {
			userActions.getElementInViewport(RATE_ENTRY_TXTBOX_LOCATOR);
			userActions.enterTextInTextBox(RATE_ENTRY_TXTBOX_LOCATOR, charge);
			logger.info("Entered rate");
		}else {
			logger.error("Rate entry text box is not present or diabled");
		}
	}
	
	public void clickAddChargeBtn() {
		logger.debug("Clicking on Add Charge button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(ADD_CHARGE_BTN_LOCATOR)) {
			userActions.getElementInViewport(ADD_CHARGE_BTN_LOCATOR);
			userActions.clickOnElement(ADD_CHARGE_BTN_LOCATOR);
			logger.info("Clicked Add Charge button");
		}else {
			logger.error("Add Charge button is not present or diabled");
		}
	}
	
	public void addAditinalCharges(String addCharge,String custID,String rate) {
		logger.debug("Adding additional Charges [AddCarge: "+addCharge+",CustomerID: "+custID+",Rate: "+rate);
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(ADDITIONAL_CHARGE_SELECTION_DROPDOWN_LOCATOR)) {
		logger.debug("Selecting additional Charges from drop down");
		userActions.getElementInViewport(ADDITIONAL_CHARGE_SELECTION_DROPDOWN_LOCATOR);
		WebElement additionalChargeDropDown = webElementsUtils.getElement(ADDITIONAL_CHARGE_SELECTION_DROPDOWN_LOCATOR);
		Select se = new Select(additionalChargeDropDown);
		se.selectByVisibleText(addCharge);
		waits.waitImplicitly();
		} else {
			logger.error("Add Charge drop down is not present or diabled");
		}
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(CUSTOMER_SEARCH_BTN_LOCATOR)) {
			logger.error("Clicking on customer selection button");
			userActions.clickOnElement(CUSTOMER_SEARCH_BTN_LOCATOR);
			waits.waitImplicitly();
			cusSelectionHandler.enterCustomerCode(custID);
			cusSelectionHandler.clickOnSearch();
			cusSelectionHandler.clickOnFirstResult();
			waits.waitImplicitly();
		} else {
			logger.error("Customer selection button is not present or diabled");
		}	
		enterAdditionalRate(rate);
		clickAddChargeBtn();
	}

	public void clickOnNextStep() {
		logger.debug("Clicking on Next Step button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(NEXT_STEP_BTN_LOCATOR)) {
			userActions.getElementInViewport(NEXT_STEP_BTN_LOCATOR);
			userActions.clickOnElement(NEXT_STEP_BTN_LOCATOR);
			logger.info("Clicked  Next Step button");
			waits.waitTillInVisibilityOfElement(LOADING_SPINNER);
		}else {
			logger.error(" Next Step button is not present or diabled");
		}
	}

	public void clickOnSave() {
		logger.debug("Clicking on Save button.....");
		if(webElementsUtils.checkElementIsDisplayedAndEnabled(SAVE_BTN_LOCATOR)) {
			userActions.getElementInViewport(SAVE_BTN_LOCATOR);
			userActions.clickOnElement(SAVE_BTN_LOCATOR);
			logger.info("Clicked  Save button");
			waits.waitTillInVisibilityOfElement(LOADING_SPINNER);
		}else {
			logger.error(" Save button is not present or diabled");
		}
	}
	
	/**
	 * method to verify if user is in customer rating page
	 */
	public boolean verifyUserIsInCustomerRatingPage() {
		String pageTitle = getPageTitle();
		Boolean isOrderCreatePage = pageTitle.equalsIgnoreCase("Customer Rating");
		logger.debug("verifying current page is customer rating page");
		if (isOrderCreatePage) {
			logger.info("Current page is customer rating page");
			return true;
		}
		return false;
	}

	/**
	 * method to verify if user is in edit customer rating
	 */
	public boolean verifyUserIsInEditCustomerRatingPage() {
		String pageTitle = getPageTitle();
		Boolean isOrderCreatePage = pageTitle.equalsIgnoreCase("Edit Customer Rating");
		logger.debug("verifying current page is edit customer rating page");
		if (isOrderCreatePage) {
			logger.info("Current page is edit customer rating page");
			return true;
		}
		return false;
	}

	/**
	 * method to get page title
	 */
	private String getPageTitle() {
		logger.debug("Getting current page title");
		waits.waitTillVisibilityOfElement(PAGE_TITLE_LOCATOR);
		WebElement pageTitleText = webElementsUtils.getElement(PAGE_TITLE_LOCATOR);
		return pageTitleText.getText();
	}
	
	public void waitTillCustomerRatingPageLoadOut() {
		Boolean isInCreateOrderPage = waits.waitTillVisibilityOfElement(CUSTOMER_RATING_PAGE_LOCATOR);
		if (!isInCreateOrderPage) {
			logger.error("CustomerRatingPage loadout failed");
			throw new RuntimeException("CustomerRatingPage loadout failed");
		}
		logger.info("CustomerRatingPage loadout successful");
	}

}
