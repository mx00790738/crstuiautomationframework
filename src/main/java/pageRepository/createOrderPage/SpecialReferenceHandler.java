package pageRepository.createOrderPage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageRepository.commonPageUtils.ItemSearchHandler;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class SpecialReferenceHandler {
	private static final Logger logger = LogManager.getLogger(SpecialReferenceHandler.class);
	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;
	public SpecialReferenceHandler(WebDriver driver) {
		this.driver=driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}
	public static final By CONSIGNEE_REF_NO_LOCATOR = By.id("consigneeBtn");
	public static final By BOL_NO_ENTRY_LOCATOR = By.name("bol");
	
	public void enterSpecialReferenceDetails(String refNo,String bol) {
		enterConsigneeRefNo(refNo);
		enterBOL(bol);
	}
	public void enterConsigneeRefNo(String number) {
		boolean isTextEntered;
		logger.debug("Entering consignee ref no: " + number + ".....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(CONSIGNEE_REF_NO_LOCATOR)) {
			userActions.getElementInViewport(CONSIGNEE_REF_NO_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(CONSIGNEE_REF_NO_LOCATOR, number);
			if (!isTextEntered) {
				throw new RuntimeException("Not able to enter location code");
			}
			logger.info("Entered consignee ref no: " + number);
		} else {
			logger.error("consignee ref no entry text box is not present or diabled");
			throw new RuntimeException("consignee ref no entry text box is not present or diabled");
		}
	}
	public void enterBOL(String number) {
		boolean isTextEntered;
		logger.debug("Entering BOL number: " + number + ".....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(BOL_NO_ENTRY_LOCATOR)) {
			userActions.getElementInViewport(BOL_NO_ENTRY_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(BOL_NO_ENTRY_LOCATOR, number);
			if (!isTextEntered) {
				throw new RuntimeException("Not able to enter BOL numberBOL number");
			}
			logger.info("Entered BOL number: " + number);
		} else {
			logger.error("BOL number entry text box is not present or diabled");
			throw new RuntimeException("BOL number entry text box is not present or diabled");
		}
	}
}
