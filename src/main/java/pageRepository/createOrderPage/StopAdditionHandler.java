package pageRepository.createOrderPage;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import pageRepository.CreateOrderPage;
import pageRepository.commonPageUtils.DatePickerHandler;
import pageRepository.commonPageUtils.LocationSearchHandler;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class StopAdditionHandler {
	private static final Logger logger = LogManager.getLogger(StopAdditionHandler.class);
	
	public static final By PICKUP_RADIO_BUTTON_LOCATOR = By
			.xpath("//input[contains(@id,'rmradiopickUp')]/following-sibling::span");
	public static final By DROP_RADIO_BUTTON_LOCATOR = By
			.xpath("//input[contains(@id,'rmradiodrop')]/following-sibling::span");
	public static final By LOCATION_CODE_SEARCH_ICON_LOCATOR = By
			.xpath("//button[contains(@id,'locationBtnAdditionalStop')]");
	public static final By PLANNED_FROM_DATE_PICKER_LOCATOR = By
			.xpath("//input[contains(@id,'dateinterMediatePickup')]/following-sibling::button");
	public static final By PLANNED_TO_DATE_PICKER_LOCATOR = By
			.xpath("//input[contains(@id,'dateinterMediateDrop')]/following-sibling::button");
	public static final By ADD_STOP_BUTTON_LOCATOR = By
			.id("buttonSaveStopPopUp");
	
	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;
	LocationSearchHandler locationSearchHandler;
	DatePickerHandler datePickerHandler;

	public StopAdditionHandler(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
		locationSearchHandler = new LocationSearchHandler(driver);
		datePickerHandler = new DatePickerHandler(driver);
	}

	public void addMultiplePickUp(HashMap<String, HashMap<String, String>> pickupIp) {
		clickPickUpRadioButton();
		clickOnLocationCodeSearchIcon();
		for (HashMap<String, String> stopDetails : pickupIp.values()) {
			locationSearchHandler.enterLocationCode(stopDetails.get("LocationCode"));
			locationSearchHandler.clickLocationCodeSearchBtn();
			locationSearchHandler.clickOnFirstResult(stopDetails.get("LocationCode"));
			clickOnPlannedFromDatePicker();
			datePickerHandler.selectDate(stopDetails.get("PlannedFrom"));
			clickOnPlannedToDatePicker();
			datePickerHandler.selectDate(stopDetails.get("PlannedTo"));
			clickOnAddStopButton();
		}
	}

	public void addMultipleDrop(HashMap<String, HashMap<String, String>> dropIp) {
		clickDropRadioButton();
		clickOnLocationCodeSearchIcon();
		for (HashMap<String, String> stopDetails : dropIp.values()) {
			locationSearchHandler.enterLocationCode(stopDetails.get("LocationCode"));
			locationSearchHandler.clickLocationCodeSearchBtn();
			locationSearchHandler.clickOnFirstResult(stopDetails.get("LocationCode"));
			clickOnPlannedFromDatePicker();
			datePickerHandler.selectDate(stopDetails.get("PlannedFrom"));
			clickOnPlannedToDatePicker();
			datePickerHandler.selectDate(stopDetails.get("PlannedTo"));
			clickOnAddStopButton();
		}
	}

	public void clickPickUpRadioButton() {
		boolean isClicked;
		logger.debug("Clicking on pickUp radio button .....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(PICKUP_RADIO_BUTTON_LOCATOR)) {
			userActions.getElementInViewport(PICKUP_RADIO_BUTTON_LOCATOR);
			isClicked = userActions.clickOnElement(PICKUP_RADIO_BUTTON_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on pickUp radio button");
			}
			logger.info("Clicked on pickUp radio button");
		} else {
			logger.error("pickUp radio button is not present or diabled");
			throw new RuntimeException("pickUp radio button is not present or diabled");
		}
	}

	public void clickDropRadioButton() {
		boolean isClicked;
		logger.debug("Clicking on drop radio button .....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(DROP_RADIO_BUTTON_LOCATOR)) {
			userActions.getElementInViewport(DROP_RADIO_BUTTON_LOCATOR);
			isClicked = userActions.clickOnElement(DROP_RADIO_BUTTON_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on drop radio button");
			}
			logger.info("Clicked on drop radio button");
		} else {
			logger.error("drop radio button is not present or diabled");
			throw new RuntimeException("drop radio button is not present or diabled");
		}
	}
	
	public void clickOnAddStopButton() {
		boolean isClicked;
		logger.debug("Clicking on add stop button .....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ADD_STOP_BUTTON_LOCATOR)) {
			userActions.getElementInViewport(ADD_STOP_BUTTON_LOCATOR);
			isClicked = userActions.clickOnElement(ADD_STOP_BUTTON_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on add stop button");
			}
			logger.info("Clicked on add stop button");
		} else {
			logger.error("add stop button is not present or diabled");
			throw new RuntimeException("add stop button is not present or diabled");
		}
	}
	
	public void clickOnUpdateStopButton() {

	}
	
	public void clickOnCancelUpdateButton() {

	}
	
	public void clickOnLocationCodeSearchIcon() {
		boolean isClicked;
		logger.debug("Clicking on location code search button .....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(LOCATION_CODE_SEARCH_ICON_LOCATOR)) {
			userActions.getElementInViewport(LOCATION_CODE_SEARCH_ICON_LOCATOR);
			isClicked = userActions.clickOnElement(LOCATION_CODE_SEARCH_ICON_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on location code search button");
			}
			logger.info("Clicked on location code search button");
		} else {
			logger.error("location code search button is not present or diabled");
			throw new RuntimeException("location code search button is not present or diabled");
		}
	}
	
	public void clickOnPlannedFromDatePicker() {
		boolean isClicked;
		logger.debug("Clicking on planned from date picker .....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(PLANNED_FROM_DATE_PICKER_LOCATOR)) {
			userActions.getElementInViewport(PLANNED_FROM_DATE_PICKER_LOCATOR);
			isClicked = userActions.clickOnElement(PLANNED_FROM_DATE_PICKER_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on planned from date picker");
			}
			logger.info("Clicked on planned from date picker");
		} else {
			logger.error("planned from date picker is not present or diabled");
			throw new RuntimeException("planned from date picker is not present or diabled");
		}
	}
	
	public void clickOnPlannedToDatePicker() {
		boolean isClicked;
		logger.debug("Clicking on planned to date picker .....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(PLANNED_TO_DATE_PICKER_LOCATOR)) {
			userActions.getElementInViewport(PLANNED_TO_DATE_PICKER_LOCATOR);
			isClicked = userActions.clickOnElement(PLANNED_TO_DATE_PICKER_LOCATOR);
			if (!isClicked) {
				throw new RuntimeException("Not able to click on planned to date picker");
			}
			logger.info("Clicked on planned to date picker");
		} else {
			logger.error("planned to date picker is not present or diabled");
			throw new RuntimeException("planned to date picker is not present or diabled");
		}
	}
}
