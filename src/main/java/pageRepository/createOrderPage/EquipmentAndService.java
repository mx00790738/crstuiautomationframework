package pageRepository.createOrderPage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class EquipmentAndService {
	private static final Logger logger = LogManager.getLogger(EquipmentAndService.class);
	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;
	public EquipmentAndService(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}
	public static final By ENTER_ORDER_VALUE_LOCATOR = 
			By.xpath("//*[@id=\"exTab3\"]/div[4]/lib-agent-portal-services/div/div/div/div[1]/lib-equipment/span[2]/div[2]/input");
	public static final By ENTER_TEMPERATURE_VALUE1_LOCATOR = 
			By.xpath("//*[@id=\"exTab3\"]/div[4]/lib-agent-portal-services/div/div/div/div[1]/lib-equipment/span[3]/div[2]/input[1]");
	public static final By ENTER_TEMPERATURE_VALUE2_LOCATOR = 
			By.xpath("//*[@id=\"exTab3\"]/div[4]/lib-agent-portal-services/div/div/div/div[1]/lib-equipment/span[3]/div[2]/input[2]");
	public static final By FIRST_ADDITIONAL_SERVICE_LOCATOR = 
			By.xpath("//*[@id=\"exTab3\"]/div[4]/lib-agent-portal-services/div/div/div/div[1]/lib-equipment/span[3]/div[2]/input[2]");
	
	public void enterOrderValue(String value) {
		boolean isTextEntered;
		logger.debug("Entering order value: " + value + ".....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ENTER_ORDER_VALUE_LOCATOR)) {
			userActions.getElementInViewport(ENTER_ORDER_VALUE_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(ENTER_ORDER_VALUE_LOCATOR, value);
			if (!isTextEntered) {
				throw new RuntimeException("Not able to enter order value:"+value);
			}
			logger.info("Entered order value: " + value);
		} else {
			logger.error("order value entry text box is not present or diabled");
			throw new RuntimeException("order value entry text box is not present or diabled");
		}
	}
	
	public void enterTemperatureValue1(String temperatureValue1) {
		boolean isTextEntered;
		logger.debug("Entering temperature value1: " + temperatureValue1 + ".....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ENTER_TEMPERATURE_VALUE1_LOCATOR)) {
			userActions.getElementInViewport(ENTER_TEMPERATURE_VALUE1_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(ENTER_TEMPERATURE_VALUE1_LOCATOR, temperatureValue1);
			if (!isTextEntered) {
				throw new RuntimeException("Not able to enter temperature value1:"+temperatureValue1);
			}
			logger.info("Entered temperature value1: " + temperatureValue1);
		} else {
			logger.error("temperature value1 entry text box is not present or diabled");
			throw new RuntimeException("temperature value1 entry text box is not present or diabled");
		}
	}
	
	public void enterTemperatureValue2(String temperatureValue2) {
		boolean isTextEntered;
		logger.debug("Entering temperature value1: " + temperatureValue2 + ".....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(ENTER_TEMPERATURE_VALUE2_LOCATOR)) {
			userActions.getElementInViewport(ENTER_TEMPERATURE_VALUE2_LOCATOR);
			isTextEntered = userActions.enterTextInTextBox(ENTER_TEMPERATURE_VALUE2_LOCATOR, temperatureValue2);
			if (!isTextEntered) {
				throw new RuntimeException("Not able to enter temperature value2:"+temperatureValue2);
			}
			logger.info("Entered temperature value2: " + temperatureValue2);
		} else {
			logger.error("temperature value2 entry text box is not present or diabled");
			throw new RuntimeException("temperature value2 entry text box is not present or diabled");
		}
	}
	
	//recheck may be not correct
	public void selectAdditionalServices() {
		logger.debug("Selecing additional service.....");
		if (webElementsUtils.checkElementIsDisplayedAndEnabled(FIRST_ADDITIONAL_SERVICE_LOCATOR)) {
			userActions.getElementInViewport(FIRST_ADDITIONAL_SERVICE_LOCATOR);
			userActions.clickOnElement(FIRST_ADDITIONAL_SERVICE_LOCATOR);
			logger.info("Selected additional service");
		} else {
			logger.error("Additional service is not present or diabled");
		}
	}
}
