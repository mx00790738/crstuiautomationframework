package pageRepository.createOrderPage;

import org.openqa.selenium.WebDriver;

import pageRepository.CreateOrderPage;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public class OriginReferenceSelectionHandler{
	WebElementsUtils webElementsUtils;
	Waits waits;
	UserActionsHandler userActions;
	WebDriver driver;
	public OriginReferenceSelectionHandler(WebDriver driver) {
		this.driver=driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
	}
}
