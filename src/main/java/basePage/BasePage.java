package basePage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import pageRepository.commonPageUtils.CarrierContactSearchHandler;
import pageRepository.commonPageUtils.CarrierSearchHandler;
import pageRepository.commonPageUtils.CommentSectionHandler;
import pageRepository.commonPageUtils.CommoditySelectionHandler;
import pageRepository.commonPageUtils.CommonPopupHandler;
import pageRepository.commonPageUtils.CustomerSelectionHandler;
import pageRepository.commonPageUtils.DatePickerHandler;
import pageRepository.commonPageUtils.DelayReasonHandler;
import pageRepository.commonPageUtils.EmailTemplateHandler;
import pageRepository.commonPageUtils.LocationSearchHandler;
import pageRepository.commonPageUtils.OrderDetailsPanelHandler;
import pageRepository.commonPageUtils.SideNavigationMenuHandler;
import pageRepository.createOrderPage.SpecialReferenceHandler;
import pageRepository.createOrderPage.StopAdditionHandler;
import seleniumUtils.UserActionsHandler;
import seleniumUtils.Waits;
import seleniumUtils.WebElementsUtils;

public abstract class BasePage {
	protected WebDriver driver;
	protected static final By LOADING_SPINNER = By.className("ui-progress-spinner-circle");
	
	protected WebElementsUtils webElementsUtils;
	protected Waits waits;
	protected UserActionsHandler userActions;
	protected SideNavigationMenuHandler sideNav;
	protected CarrierContactSearchHandler carrierContactSearchHandler;
	protected EmailTemplateHandler emailTemplateHandler;
	protected CarrierSearchHandler carrierSearchPopUpHandler;
	protected CommentSectionHandler commentSectionHandler;
	protected DatePickerHandler datePickerHandler;
	protected LocationSearchHandler locationSearchHandler;
	protected CommoditySelectionHandler commoditySelectionHandler;
	protected SpecialReferenceHandler specialReferenceHandler;
	protected OrderDetailsPanelHandler orderDetailsHeaderHandler;
	protected CustomerSelectionHandler cusSelectionHandler;
	protected DelayReasonHandler delayReasonPopupHandler;
	protected StopAdditionHandler stopAdditionHandler;
	protected CommonPopupHandler commonPopupHandler;
	public BasePage(WebDriver driver){
		this.driver=driver;
		webElementsUtils = new WebElementsUtils(driver);
		waits = new Waits(driver);
		userActions = new UserActionsHandler(driver);
		sideNav = new SideNavigationMenuHandler(driver);
		carrierContactSearchHandler = new CarrierContactSearchHandler(driver);
		emailTemplateHandler = new EmailTemplateHandler(driver);
		carrierSearchPopUpHandler = new CarrierSearchHandler(driver);
		commentSectionHandler = new CommentSectionHandler(driver);
		datePickerHandler = new DatePickerHandler(driver);
		locationSearchHandler = new LocationSearchHandler(driver);
		commoditySelectionHandler = new CommoditySelectionHandler(driver);
		specialReferenceHandler = new SpecialReferenceHandler(driver);
		orderDetailsHeaderHandler = new OrderDetailsPanelHandler(driver);
		cusSelectionHandler = new CustomerSelectionHandler(driver);
		delayReasonPopupHandler = new DelayReasonHandler(driver);
		stopAdditionHandler = new StopAdditionHandler(driver);
		commonPopupHandler = new CommonPopupHandler(driver);
	}
	
	protected String getCurrentPageURL() {
		return driver.getCurrentUrl();
	}
	
	protected String getCurrentPageTitle() {
		return driver.getTitle();
	}
	
}
