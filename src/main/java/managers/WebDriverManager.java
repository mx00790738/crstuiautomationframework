package managers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class WebDriverManager {
	private static final Logger logger = LogManager.getLogger(WebDriverManager.class);
	private WebDriver driver;
	private String browserType;
	private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";
	
	private static final String chromeDriverExecutable = "chromedriver.exe";
    private static final String fileSeperator = System.getProperty("file.separator");
    private static final String driverFolderPath = System.getProperty("user.dir") +fileSeperator+ "drivers";
    private static final String chromeDriverExecutablePath = driverFolderPath+fileSeperator+chromeDriverExecutable;
    
    public WebDriverManager() {
		browserType = FileReaderManager.getInstance().getConfigReader().getBrowser();
	}

	public WebDriver getDriver() {
		if (driver == null) {
			driver = createDriver();
		}
		return driver;
	}

	private WebDriver createDriver() {
		
		switch (browserType) {
		case "firefox":
			logger.info("Creating Firefox Driver");
			driver = new FirefoxDriver();
			break;
		case "chrome":
			logger.info("Creating Chrome Driver");
			System.setProperty(CHROME_DRIVER_PROPERTY,
					chromeDriverExecutablePath);
			driver = new ChromeDriver();
			break;
		case "ie":
			logger.info("Creating InternetBrowser Driver");
			driver = new InternetExplorerDriver();
			break;
		default:
			throw new RuntimeException("Invalid browser option in Configuration.properties");
		}
		return driver;
	}
	
	public void closeBrowser() {
		 driver.close();
   }
	
	public void closeDriver() {
		 driver.quit();
    }
}
