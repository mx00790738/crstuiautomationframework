package managers;

import org.openqa.selenium.WebDriver;

import pageRepository.CarrierConfirmationPage;
import pageRepository.CarrierOfferPage;
import pageRepository.CarrierRatingPage;
import pageRepository.ControlTowerPage;
import pageRepository.CreateOrderPage;
import pageRepository.CustomerRatingPage;
import pageRepository.DeliveryLogPage;
import pageRepository.ItegratedRatingPage;
import pageRepository.LoginPage;
import pageRepository.OpsClosedPage;
import pageRepository.PickUpLogPage;
import pageRepository.commonPageUtils.SideNavigationMenuHandler;

public class PageObjectManager {
	private WebDriver driver;
	private LoginPage loginPage;
	private ControlTowerPage landingPage;
	private CreateOrderPage createOrderPage;
	private SideNavigationMenuHandler sideNav;
	private CustomerRatingPage customerRatingPage;
	private CarrierRatingPage carrierRatingPage;
	private CarrierConfirmationPage carrierConfirmationPage;
	private PickUpLogPage pickUpLogPage;
	private DeliveryLogPage deliveryLogPage;
	private OpsClosedPage opsClosedPage;
	private ItegratedRatingPage itegratedRatingPage;
	private CarrierOfferPage carrierOfferPage;
	public PageObjectManager(WebDriver driver) {
		this.driver = driver;
	}
	
	public SideNavigationMenuHandler getSideNavigationMenu() {
		return (sideNav == null) ? sideNav = new SideNavigationMenuHandler(driver) : sideNav;
	}
	
	public LoginPage getLoginPage() {
		return (loginPage == null) ? loginPage = new LoginPage(driver) : loginPage;
	}
	
	public ControlTowerPage getLandingPage() {
		return (landingPage == null) ? landingPage = new ControlTowerPage(driver) : landingPage;
	}
	
	public CreateOrderPage getCreateOrderPage() {
		return (createOrderPage == null) ? createOrderPage = new CreateOrderPage(driver) : createOrderPage;
	}
	
	public CustomerRatingPage getCustomerRatingPage() {
		return (customerRatingPage == null) ? customerRatingPage = new CustomerRatingPage(driver) : customerRatingPage;
	}
	
	public CarrierRatingPage getCarrierRatingPage() {
		return (carrierRatingPage == null) ? carrierRatingPage = new CarrierRatingPage(driver) : carrierRatingPage;
	}
	
	public CarrierConfirmationPage getCarrierConfirmationPage() {
		return (carrierConfirmationPage == null) ? carrierConfirmationPage = new CarrierConfirmationPage(driver) : carrierConfirmationPage;
	}
	
	public PickUpLogPage pickUpLogPage() {
		return (pickUpLogPage == null) ? pickUpLogPage = new PickUpLogPage(driver) : pickUpLogPage;
	}
	
	public DeliveryLogPage deliveryLogPage() {
		return (deliveryLogPage == null) ? deliveryLogPage = new DeliveryLogPage(driver) : deliveryLogPage;
	}
	
	public OpsClosedPage opsClosedPage() {
		return (opsClosedPage == null) ? opsClosedPage = new OpsClosedPage(driver) : opsClosedPage;
	}
	
	public ItegratedRatingPage itegratedRatingPage() {
		return (itegratedRatingPage == null) ? itegratedRatingPage = new ItegratedRatingPage(driver) : itegratedRatingPage;
	}
	
	public CarrierOfferPage carrierOfferPage() {
		return (carrierOfferPage == null) ? carrierOfferPage = new CarrierOfferPage(driver) : carrierOfferPage;
	}
}
